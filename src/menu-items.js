export default {

  items: [
    {
      id: "chart-maps",
      title: "Chart & Maps",
      type: "group",
      icon: "feather icon-charts",
      children: [


        {
          id: "home",
          title: "Home",
          type: "item",
          icon: "feather icon-map",
          url: "/",
        },
        {
          id: "inventory",
          title: "Inventory",
          icon: "feather icon-box",
          type: "item",
          url: "/inventory",

        },
        {
          id: "connection",
          title: "Devices Connection",
          type: "item",
          icon: "feather icon-wifi",
          url: "/connection",
        },
      ],
    },


    {
      id: "records",
      title: "Records",
      type: "group",
      icon: "icon-historic",
      children: [



        {
          id: "watchRecords",
          title: "Historic Records",
          type: "item",
         
          icon: "feather icon-pie-chart",
          url: "/records",
        },
        {
          id: "analiticLiveIP",
          title: "Analitic Live (IP)",
          type: "item",
          redirect:true,
          url: "https://3.129.97.184:3000/charts",
          icon: "feather icon-bar-chart-2"
        },
        {
          id: "analiticLiveOCH",
          title: "Analitic Live (OCH)",
          type: "item",
          redirect:true,
          url: "https://3.129.97.184:3000/charts2",
          icon: "feather icon-bar-chart-2"
        },
        {
          id: "info",
          title: "About Hubble",
          type: "item",
          redirect:true,
          url: "http://104.131.172.141:4000/index",
          icon: "feather icon-info"
          
        },



      ],
    },

   

  ],
};
