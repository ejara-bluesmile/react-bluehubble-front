import React, { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import Aux from "../hoc/_Aux";
import redButton from "../assets/images/redbutton.png";
import greenButton from '../assets/images/greenbutton.png';
import yellowButton from '../assets/images/yellowbutton.png';
import { socket, socket2 } from "../App/components/map-utils/MarkerCluster";

import SearchMap from './../components/mapMenuComponent/searchMap';

import CardFilterLayer from './../components/mapMenuComponent/cardFilterLayer';
import PyhsicalLayer from './../components/mapMenuComponent/pyhsicalLayer';
import WarningFilter from './../components/mapMenuComponent/warningFIlter';
import Status from './../components/mapMenuComponent/status';
import MetrixCard from './../components/graphComponent/metrixCard';

import  '../assets/scss/map-menu.scss';

export default (props) => {

  const [queries, setQueries] = useState(0);
  const [timeouts, setTimeouts] = useState(0);
  const [packages, setPackages] = useState(0);
  const [status, setStatus] = useState(redButton);
  const [queries2, setQueries2] = useState(0);
  const [timeouts2, setTimeouts2] = useState(0);
  const [packages2, setPackages2] = useState(0);
  const [status2, setStatus2] = useState(redButton);

  useEffect(() => {
    (async () => {
      let channel = socket.subscribe("toQueryCounter");
      for await (let data of channel) {
        setStatus(greenButton)
        setQueries(data);
      }
    })();
    (async () => {
      let channel = socket.subscribe("timeoutCounter");
      for await (let data of channel) {

        if (data >= 1000000) {
          data = (data / 100).toFixed(0) + "K";
        }
        setTimeouts(data.timeouts)
        if (data.timeouts > 1 && data.id !== "") {
          setStatus(yellowButton);

        }


      }
    })();
    (async () => {
      let channel = socket.subscribe("packagesCounter");
      for await (let data of channel) {
        setPackages(data);
      }
    })();

    (async () => {
      let channel = socket2.subscribe("toQueryCounter");
      for await (let data of channel) {
        setStatus2(greenButton);
        setQueries2(data);

      }
    })();
    (async () => {
      let channel = socket2.subscribe("timeoutCounter");
      for await (let data of channel) {

        if (data >= 1000000) {
          data = (data / 100).toFixed(0) + "K";
        }
        setTimeouts2(data.timeouts);
        if (data.timeouts > 1 && data.id !== "") {
          setStatus2(yellowButton);
        }
      }
    })();
    (async () => {
      let channel = socket2.subscribe("packagesCounter");
      for await (let data of channel) {
        setPackages2(data);
      }
    })();

    return () => {
      // if (socket) socket.disconnect();
      // if (socket2) socket2.disconnect();
    }
  }, []);



  return (
    <Aux>
      <Row className="rowmenu">
       
        <SearchMap
        />
        <Col md="2" xl="2" className="p-1">
          <CardFilterLayer
          />
        </Col>
      
        {props.layer === 'IP_INTERFACE' &&
      <>
 <Col  className="p-1  queryCol ">

<MetrixCard
  clase="queryCard"
  name="Queries"
  value={queries}
/>
</Col>      
        </>}
        {props.layer === 'OCH' && <>
        <Col className="p-1 queryCol">

<MetrixCard
 clase="queryCard"
  name="Queries"
  value={queries2}
/>
</Col> 
        </>
        }
      </Row>
      <Row className="rowmenu">
        <WarningFilter
          onLayerChange={props.onLayerChange}
          markers={props.markers}
        />
        {props.layer === 'IP_INTERFACE' &&
          <>
            <Col className="p-1 bigColMap">
              <Status
                status={status}
              />
            </Col>
            <Col md="2" xl="2" className="p-1">
          <PyhsicalLayer
          />
        </Col>
        <Col  className="p-1 bigColMap">
            <div className="metrix">
            <Col>
            <Row>
            <Col className="p-1 bigColMap packeCol">
                <MetrixCard
                name="Timeouts"
                value={timeouts }
                />
</Col>              
            </Row>

            <Row className="packageRow">
          <Col className="p-1 bigColMap packageCol">
                <MetrixCard
                 clase="PackageCard"
                name="Packets"
                value={packages}
                />
              </Col>
            </Row>
           </Col>
            </div>
            
          </Col>
          </>}
      
        {props.layer === 'OCH' && <>
          <Col md={2} xl={2} sm={3} className="p-1">
            <Status
              status={status2}
            />
            
          </Col>
          <Col md="2" xl="2" className="p-1">
          <PyhsicalLayer
          />
        </Col>
          <Col  className="p-1 bigColMap">
            <div className="metrix">
            <Col>
            <Row>
            <Col className="p-1 bigColMap packeCol">
                <MetrixCard
                name="Timeouts"
                value={timeouts2}
                />
</Col>              
            </Row>

            <Row className="packageRow">
          <Col className="p-1 bigColMap packageCol">
                <MetrixCard
                 clase="PackageCard"
                name="Packets"
                value={packages2}
                />
              </Col>
            </Row>
           </Col>
            </div>
            
          </Col>
        </>}
      </Row>
    </Aux >
  );
}
