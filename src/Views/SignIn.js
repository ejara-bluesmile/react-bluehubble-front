
import { NavLink } from "react-router-dom";

import "../assets/scss/style.scss";

import "../assets/scss/logIn.scss";
import Aux from "../hoc/_Aux";
import Breadcrumb from "../App/layout/AdminLayout/Breadcrumb";
import Form from "../components/Form";
import { Link } from "react-router-dom";
import axios from "axios";
import swal from "sweetalert2";
import logo1 from "../assets/images/LOGOS/LOGOS/logo-transparente-fondo-color.png"

import { useHistory } from "react-router-dom";
import React, { useState, useEffect } from 'react'



const SignIn = () => {

  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");
  const [islogin, setIslogin] = useState(false);
  let history = useHistory();

  //const navigate = useHistory();


  const handleSubmit = (e) => {


    //alert(`${user}, ${password}`)
    e.preventDefault();

   if(
   user === "maria2@maria.cl" && password==="maria" ||
   user === "shenao@bluesmile-it.com" && password==="shenao" ||
   user === "ahoyos@bluesmile-it.com" && password==="ahoyos"||
   user === "bcassis@bluesmile-it.com" && password==="bcassis"){
    setIslogin(true);
    localStorage.setItem("login", true)
    history.push("/");
 
  
  } else {
    swal.fire({
      icon: 'error',
      title: 'faild',
      text: 'The User or password is wrong',

    });
  }
     

    /*if (user !== "maria2@maria.cl"
    ) {
      swal.fire({
        icon: 'error',
        title: 'faild',
        text: 'The User is wrong',

      })


    } else if (password !== "maria") {
      swal.fire({
        icon: 'error',
        title: 'faild',
        text: 'The password is wrong',

      })
    } else {
      setIslogin(true);
      localStorage.setItem("login", true)
      history.push("/map");
    }
*/


  };

  return (
    <Aux>
      <Breadcrumb />
      <div className="page-header clear-filter" filter-color="blue">
      <div className="page-header-image">
        <div className="container">
        <section className="material-half-bg">
        <div className="cover"></div>
      </section>
      <div className="sapace"></div>
     
      <div className="auth-wrapper ">
    
        <div className="auth-content">
         
          {/* <div className="auth-bg">
            <span className="r" />
            <span className="r s" />
            <span className="r s" />
            <span className="r" />
          </div> */}
           <div className="logo-singin">
            <img className="logoblue1Sing" src={logo1}></img>
          </div>
          <div className="card">
            <div className="card-body text-center">
              {/* <div className="mb-4">
                <i className="feather icon-unlock auth-icon" />
              // </div> */}
              <form className="login-form" onSubmit={handleSubmit} action="/signin">
                <h3 className="mb-4">
                  <i className="feather icon-unlock auth-icon" />
                  Login
                </h3>
                <div className="input-group mb-3">
                  <input
                    name="email"
                    type="email"
                    className="form-control"
                    placeholder="Email"
                    onChange={(e) => setUser(e.target.value)}
                    value={user}
                  />
                </div>
                <div className="input-group mb-4">
                  <input
                    name="password"
                    type="password"
                    className="form-control"
                    placeholder="password"
                    onChange={(e) => setPassword(e.target.value)}
                    value={password}
                  />
                </div>
                <div className="form-group text-left">
                  <div className="checkbox checkbox-fill d-inline">
                    <input
                      type="checkbox"
                      name="checkbox-fill-1"
                      id="checkbox-fill-a1"
                    />
                    <label htmlFor="checkbox-fill-a1" className="cr">
                      {" "}
                      <span className="Stay">Stay signed in</span> 
                    </label>
                  </div>
                </div>
                <button
                  className="btn btn-primary shadow-2 mb-4">
                  Login
                </button>
              </form>
              <p className="mb-2 text-muted">
                Forgot password?{" "}
                <NavLink to="/auth/reset-password-1">Reset</NavLink>
              </p>
              <p className="mb-0 text-muted">
                Don’t have an account?{" "}
                <NavLink to="/auth/signup-1">Signup</NavLink>
              </p>
            </div>
          </div>
        </div>
      </div>
        </div>
      </div>
      </div>
    </Aux>
  );
}






export default SignIn;
