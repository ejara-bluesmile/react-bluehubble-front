import React from 'react';
import { useState } from 'react';
import {
    Row,
    Col,
    Card,
    Form,
    Button,
} from "react-bootstrap";
import { css } from "@emotion/core";
import FadeLoader from "react-spinners/FadeLoader";
import Select from "react-select";
import axios from 'axios';
import { useEffect } from 'react';
import Aux from "../hoc/_Aux";
import Pagination from '../components/connection/Pagination';
import '../style/pagination.css';
import '../style/resposiveConectionYrecord.css';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const MySwal = withReactContent(Swal);

export default () => {

    const [selectedIp1, setselectedIp1] = useState(null);
    const [selectedIp2, setselectedIp2] = useState(null);
    const [ipA, setIpA] = useState(null);
    const [ipB, setIpB] = useState(null);
    const [source, setSource] = useState(null);
    const [sink, setSink] = useState(null);
    const [ips, setIps] = useState([]);
    const [layer, setLayer] = useState([]);
    const [loading, setLoading] = useState(true);
    const [spinLoading, setSpinLoading] = useState(false);
    const [ocSources, setOcSources] = useState([]);
    const [ocSinks, setOcSinks] = useState([]);
    const [sourceNames, setSourceNames] = useState([]);
    const [sinkNames, setSinkNames] = useState([]);
    const [selectedOcSource, setSelectedOcSource] = useState(null);
    const [selectedOcSink, setSelectedOcSink] = useState(null);
    const [selectedSourceName, setSelectedSourceName] = useState(null);
    const [selectedSinkName, setSelectedSinkName] = useState(null);
    const [paths, setPaths] = useState(null);

    const handleIp1Change = (ip) => {
        cleanAllFilters();
        setselectedIp1(ip);
    }
    const handleIp2Change = (ip) => {
        cleanAllFilters();
        setselectedIp2(ip);
    }

    useEffect(() => {
        async function getData() {
            getLayers();
            getIps();

        }
        getData()
    }, []);

    useEffect(() => {
        console.log(paths)
    }, [paths]);

    const getIps = async () => {
        try {
            const response = await axios.get('http://13.58.191.209:40000/ip');
            const ips = response.data.ips;
            fillIpsSelect(ips);
            setLoading(false);
        } catch (error) {
            setLoading(false);
            MySwal.fire({
                icon: 'error',
                title: 'Error',
                text: 'An error has occured while getting ips list',

            });
        }
    }

    const getLayers = async () => {
        try {
            const apiUrl2 = "http://3.129.97.184:3003/findLayers";
            const response2 = await axios.post(apiUrl2);
            fillLayerSelect(response2.data.result.data[1]);
            setLoading(false);
        } catch (e) {
            console.log(e);
            setLoading(false);
            MySwal.fire({
                icon: 'error',
                title: 'Error',
                text: 'An error has occured while getting advanced filters data',
            });
        }

    }
    const verifyConnection = async () => {
        console.log(ipA, ipB)
        const source = ipA || selectedIp1.label;
        const sink = ipB || selectedIp2.label;
        if (source && sink) {
            try {
                setSpinLoading(true);
                const response = await axios.post('http://3.129.97.184:3010/getRoutesKpis', {
                    ipA: source,
                    ipB: sink
                });
                setSource(source);
                setSink(sink);
                if (response.data.response) {
                    const { path } = response.data;
                    setPaths(path);
                    setSpinLoading(false);
                } else {
                    setSpinLoading(false);
                    MySwal.fire({
                        icon: 'warning',
                        title: 'Error',
                        text: `No paths were found between ${source} and ${sink}`,
                    });
                }

            } catch (error) {
                console.log(error);
                setSpinLoading(false);
                MySwal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'An error has occured',

                });
            }
        }
    }

    const fillIpsSelect = (devices) => {
        const ips = devices.map((e) => {
            return Object.assign({}, e, {
                value: e,
                label: e,
            });
        }).filter((valorActual, indiceActual, arreglo) => {
            return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        });
        setIps(ips);
    }

    const fillLayerSelect = (layer) => {
        setLayer(layer);

        const ocSources = layer.features
            .map((e) => {
                return {
                    value: e.properties.ID,
                    label: e.properties.OC_Source,
                    ocSink: e.properties.OC_Sink,
                    ocSource: e.properties.OC_Source,
                    ip: e.properties.NE_IP_Address_Source
                };
            })
            .filter((valorActual, indiceActual, arreglo) => {
                return (
                    arreglo.findIndex(
                        (valorDelArreglo) => valorDelArreglo.label === valorActual.label
                    ) === indiceActual
                );
            });

        const ocSinks = layer.features
            .map((e) => {
                return Object.assign({}, e, {
                    value: e.properties.ID,
                    label: e.properties.OC_Sink,
                    ocSource: e.properties.OC_Source,
                    ocSink: e.properties.OC_Sink,
                    ip: e.properties.NE_IP_Address_Sink
                });
            })
            .filter((valorActual, indiceActual, arreglo) => {
                return (
                    arreglo.findIndex(
                        (valorDelArreglo) => valorDelArreglo.label === valorActual.label
                    ) === indiceActual
                );
            });

        const sourceNames = layer.features
            .map((e) => {
                return Object.assign({}, e, {
                    value: e.properties.ID,
                    label: e.properties.Source_Ne_Name,
                    sink: e.properties.Sink_Ne_Name,
                    source: e.properties.Source_Ne_Name,
                    ip: e.properties.NE_IP_Address_Source
                });
            })
            .filter((valorActual, indiceActual, arreglo) => {
                return (
                    arreglo.findIndex(
                        (valorDelArreglo) => valorDelArreglo.label === valorActual.label
                    ) === indiceActual
                );
            });

        const sinkNames = layer.features
            .map((e) => {
                return Object.assign({}, e, {
                    value: e.properties.ID,
                    label: e.properties.Sink_Ne_Name,
                    sink: e.properties.Sink_Ne_Name,
                    source: e.properties.Source_Ne_Name,
                    ip: e.properties.NE_IP_Address_Sink
                });
            })
            .filter((valorActual, indiceActual, arreglo) => {
                return (
                    arreglo.findIndex(
                        (valorDelArreglo) => valorDelArreglo.label === valorActual.label
                    ) === indiceActual
                );
            });

        setOcSources(ocSources);
        setOcSinks(ocSinks);
        setSourceNames(sourceNames);
        setSinkNames(sinkNames);
        // setInitialOcSources(layer.features);
        // setInitialOcSinks(layer.features);
        // setInitialSourceNames(layer.features);
        // setInitialSinkNames(layer.features);

    }
    const handleOcSourceChange = async (selection) => {
        setSelectedOcSource(selection);
        setIpA(selection.ip);
        setSelectedSinkName(null);
        setSelectedSourceName(null);

        setselectedIp1(null);
        setselectedIp2(null);
    }
    const handleOcSinkChange = async (selection) => {
        setSelectedOcSink(selection);
        setIpB(selection.ip);
        setSelectedSinkName(null);
        setSelectedSourceName(null);

        setselectedIp1(null);
        setselectedIp2(null);

    }
    const handleSinkChange = async (selection) => {
        setSelectedSinkName(selection);
        setIpB(selection.ip);
        setSelectedOcSource(null);
        setSelectedOcSink(null);

        setselectedIp1(null);
        setselectedIp2(null);

    }
    const handleSourceChange = async (selection) => {
        setSelectedSourceName(selection);
        setIpA(selection.ip);
        setSelectedOcSource(null);
        setSelectedOcSink(null);

        setselectedIp1(null);
        setselectedIp2(null);

    }

    const cleanAllFilters = () => {
        setSelectedOcSink(null);
        setSelectedOcSource(null);
        setSelectedSinkName(null);
        setSelectedSourceName(null);
        setIpA(null);
        setIpB(null);
    }

    const override = css`
    display: block;
    margin: 0 auto;
    opacity: 0.5;
    position:sticky;
    transform: translate(30%, 0%);
    top: 50%;
    left: 50%;
    z-index:1;
  `
    if (loading) return <div className="App">Loading...</div>;

    return (
        <Aux>
            <FadeLoader color="#1D3582" loading={spinLoading} css={override} size={150} />
            <div className="mb-3">
                <center> <h2 className="text-center font-bold font-up deep-purple-text title">Check Connection Between:</h2></center>
            </div>

            {ips && <Col md={12}>
                <Card>
                    <center><Card.Title>Quick Search (IP)</Card.Title></center>
                    <Card.Body>
                        <Form.Group controlId="exampleForm.ControlSelect1">
                            <Row>
                                <Col md={10}>
                                    <Row>
                                        <Col md={2}>
                                            <label className="ml-2">IP 1: </label>
                                        </Col>
                                        <Col md={8}>
                                            <Select
                                                value={selectedIp1}
                                                options={ips}
                                                onChange={handleIp1Change}
                                            />
                                        </Col>
                                        <Col>
                                            <Button
                                                className="align-middle clear"
                                                variant="secondary"
                                                onClick={e => setselectedIp1(null)}>
                                                <span className="feather icon-x"></span>
                                            </Button>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2}>
                                            <label className="ml-2">IP 2: </label>
                                        </Col>

                                        <Col md={8}>
                                            <Select
                                                value={selectedIp2}
                                                options={ips}
                                                onChange={handleIp2Change}
                                            />
                                        </Col>
                                        <Col>
                                            <Button className="align-middle clear" variant="secondary"
                                                onClick={e => setselectedIp2(null)}>
                                                <span className="feather icon-x"></span>
                                            </Button>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <center>
                                <Button className="mt-3" variant="primary"
                                    onClick={verifyConnection}
                                >
                                    Search
                                     </Button>
                            </center>
                        </Form.Group>
                    </Card.Body>
                </Card>
            </Col>}
            {layer && <Col md={12}>
                <Card>
                    <center><Card.Title>Advanced Search</Card.Title></center>
                    <Card.Body>
                        <Form.Group controlId="exampleForm.ControlSelect1">
                            <Row>
                                <Col md={10}>
                                    <Row>
                                        <Col md={2}>
                                            <label className="ml-2">OC_Source: </label>
                                        </Col>

                                        <Col md={8}>
                                            <Select
                                                value={selectedOcSource}
                                                options={ocSources}
                                                onChange={handleOcSourceChange}
                                            />
                                        </Col>
                                        <Col>
                                            <Button className="align-middle clear" variant="secondary"
                                                onClick={e => setSelectedOcSource(null)}>
                                                <span className="feather icon-x"></span>
                                            </Button>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2}>
                                            <label className="ml-2">OC_Sink: </label>
                                        </Col>

                                        <Col md={8}>
                                            <Select options={ocSinks}
                                                onChange={handleOcSinkChange}
                                                value={selectedOcSink}
                                            />
                                        </Col>
                                        <Col>
                                            <Button className="align-middle clear" variant="secondary"
                                                onClick={e => setSelectedOcSink(null)}>
                                                <span className="feather icon-x"></span>
                                            </Button>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2}>
                                            <label className="ml-2">Source_Ne_Name: </label>
                                        </Col>
                                        <Col md={8}>
                                            <Select options={sourceNames} value={selectedSourceName}
                                                onChange={handleSourceChange}
                                            />
                                        </Col>
                                        <Col>
                                            <Button className="align-middle clear" variant="secondary"
                                                onClick={e => setSelectedSourceName(null)}>
                                                <span className="feather icon-x"></span>
                                            </Button>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2}>
                                            <label className="ml-2">Sink_Ne_Name: </label>
                                        </Col>

                                        <Col md={8}>
                                            <Select options={sinkNames} value={selectedSinkName}
                                                onChange={handleSinkChange}
                                            />
                                        </Col>
                                        <Col>
                                            <Button className="align-middle clear" variant="secondary"
                                                onClick={e => setSelectedSinkName(null)}>
                                                <span className="feather icon-x"></span>
                                            </Button>
                                        </Col>
                                    </Row>
                                </Col>

                            </Row>

                            <center className="botones abs-center">
                                <Button className="mt-3" variant="secondary"
                                    onClick={cleanAllFilters}
                                >
                                    Clear all
                                        </Button>

                                <Button className="mt-3" variant="primary"
                                    onClick={verifyConnection}
                                >
                                    Search
                                     </Button>



                            </center>


                        </Form.Group>
                    </Card.Body>
                </Card>
            </Col>}

            {paths && <Col md={12}>
                {source && sink && <Card>
                    <center><Card.Title>Paths Between {source} And {sink} </Card.Title></center>
                    <Card.Body>
                        <Row>
                            <Pagination paths={paths} source={source} sink={sink} />
                        </Row>
                    </Card.Body>
                </Card>}
            </Col>}

        </Aux>
    )
}