import React, { useEffect, useState } from 'react'
import "../assets/scss/chartData.scss"
import "../assets/scss/table.scss"


import swal from 'sweetalert2';
import axios from "axios";
import Table from '../components/charWarning/tablaWarning';



const ChartData = () => {


    const [info, setInfo] = useState(
        JSON.parse(localStorage.getItem("chartWarningData"))
    );
    const [packages, setPackages] = useState(false)
    const [warning, setWarning] = useState(false)


    const [name, setName] = useState(
        info.name
    );

    const [lastDate, setLastDate] = useState(
        localStorage.getItem('lastDate')
    )

    const [values, SetValue] = useState(new Array())

    const [queryType, setQueryType] = useState("")

    const [bdData, setBdData] = useState([])

    const [loadingid, setloadingid] = useState(false)




    useEffect(() => {


        if (name == 'With warnings') { setWarning(true); setQueryType(""); }
        if (name == 'Optical power out of range') { setWarning(true); setQueryType("optical"); }
        if (name == 'Temperature out of range') { setWarning(true); setQueryType("temperature"); }
        if (name == 'Bandwidth out of range') { setWarning(true); setQueryType("bandwidth"); }
        if (name == 'Cpu Usage out of range') { setWarning(true); setQueryType("cpu"); }
        if (name == 'Delay out of range') { setWarning(true); setQueryType("delay"); }
        if (name == 'Jitter out of range') { setWarning(true); setQueryType("jitter"); }
        if (name == 'Bit Error Rate out of range') { setWarning(true); setQueryType("bitErrorRate"); }
        if (name == 'Bit Error Ratio out of range') { setWarning(true); setQueryType("bitErrorRatio"); }
        if (name == 'With Timeouts') { setWarning(true); setQueryType("timeouts"); }
        if (name == 'With lost packages') setPackages(true);



        if (info.data.length > 0) {
            if (packages) {
                getPackagesData()
            }
            if (warning) {
                getWarningsData()
            }
            if (!warning && !packages) {
                graphNoWarnings()
            }
        } else {
            swal.fire('Warning', 'No links or data found for the selected segment.', 'warning',
                setloadingid(false)
            ).then((r) => {
                window.close()
            })
        }


    }, [info])

    const getPackagesData = async () => {
        try {
            await axios.post("http://3.129.97.184:3003/findLostPackagesCount", { date: lastDate, ids: info.data })
                .then(res => {

                    setBdData(res.data.result.data)
                    setloadingid(true)
                    graphWarnings()
                })


        } catch (error) {
            setloadingid(false)
            console.log(error)
        }

    }
    const graphWarnings = async () => {
        try {
            await axios.post("http://3.129.97.184:3003/findLostPackagesCount", { ids: bdData })
                .then(res => {
                    setBdData(res.data.result.data)
                    setloadingid(true)
                })

        } catch (error) {
            setloadingid(false)
            swal.fire('warning', 'Error', 'error')
            console.log(error)
        }
    }
    const graphNoWarnings = async () => {
        try {
            await axios.post("http://3.129.97.184:3003/findLinksDataByIds", { ids: info.data })
                .then(res => {
                    setBdData(res.data.result.data)
                    setloadingid(true)
                })

        } catch (error) {
            setloadingid(false)
            swal.fire('warning', 'Error', 'error')
            console.log(error)
        }
    }
    const getWarningsData = async () => {
        let queryDate = new Date()

        if (queryType === "timeouts") {
            try {
                await axios.post("http://3.129.97.184:3003/findTimeoutsCount", { ids: info.data, data: queryDate })
                    .then(res => {
                        setBdData(res.data.result.data)
                        setloadingid(true)
                        graphWarnings()
                    })

            } catch (error) {
                setloadingid(false)
                console.log(error)
            }
        }
    }

    return (
        <div>
            <h1 className="titleGraf">{name}</h1>
            {
                <Table
                    dataNew={bdData}
                    loadin={loadingid}

                />}
        </div>
    )
}

export default ChartData
