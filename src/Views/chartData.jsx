import React, { useEffect, useState, use } from 'react'
import "../assets/scss/chartData.scss"
import "../assets/scss/table.scss"
import Graft from '../components/charData/Graft';

import swal from 'sweetalert2';
import axios from "axios";
import Table from '../components/charData/table ';

//var info = JSON.parse(sessionStorage.getItem('chartData'))
//var data = info.data
//var name = info.name
//var interval = sessionStorage.getItem('chartInterval')
//const param = location.search.substring(1, location.search.length);
/*const arrayParam = param.split("=");
const LAYERTYPE = arrayParam[1];
var ids = new Array()
var values = new Array()*/
const ChartData = () => {


    const [info, setInfo] = useState(
        JSON.parse(localStorage.getItem('chartData'))
    );
    const [data, setData] = useState(
        info.data
    );
    const [name, setName] = useState(
        info.name
    );
    const [interval, setInterval] = useState(
        localStorage.getItem('chartInterval')
    )

    const [ids, setIds] = useState(new Array())

    const [values, setValues] = useState(new Array())

    const [dataNewState, setDataNew] = useState([])
    const [loadin, setLoading] = useState(false)

    const graph = async () => {
        try {


            var dataNew = data.filter(elemnte => elemnte.interval == interval)

            var idst = new Array();
            var valuest = new Array();
            if (dataNew.length > 0) {
                for (let record of dataNew) {


                    idst.push(record.name);
                    valuest.push(record.data)
                }



                setIds(idst)
                setValues(valuest)
                await axios.post("http://3.129.97.184:3003/findLinksDataByJsonIds", { ids: dataNew })
                    .then(res => {

                        setDataNew(res.data.result.data)
                        setLoading(true)
                    })

            } else {
                swal.fire('Warning', 'No links or data found for the selected segment.', 'warning',
                ).then((r) => {
                    window.close()
                })
            }
        } catch (error) {
            console.log(error)
        }


    }

    useEffect(() => {

        graph()
    }, [info])


    //FILTERING DATA TO ASSURE THE RECORD IS IN THE SPECIFIED INTERVAL
    const checkRange = (info) => {

        return info.interval == interval
    }
    return (
        <div>
            <h1 className="titleGraf">{name}</h1>
            <Graft
                ids={ids}
                values={values}
                loadin={loadin}
            />
            <Table
                dataNew={dataNewState}
                loadin={loadin}
                name={name}
            />
        </div>
    )
}

export default ChartData
