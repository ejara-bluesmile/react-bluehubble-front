import React from "react";
import { Container, Row, Col } from "shards-react";
import Aux from "../hoc/_Aux";

/*
*/

import UserAccountDetails from "../components/profilepage/UserAccountDetails"

import UserDetails from "../components/profilepage/UserDetails"
import PageTitle from "../components/profilepage/PageTitle"

const UserProfileLite = () => (
  <Aux >
  <Container fluid className="main-content-container px-4">
    <Row noGutters className="page-header py-4">
      <PageTitle
        title="User Profile"
        subtitle="Overview"
        md="12"
        className="ml-sm-auto mr-sm-auto"
      />
    </Row>
    <Row>
      <Col lg="4">
        <UserDetails />
      </Col>
      <Col lg="8">
        <UserAccountDetails />
      </Col>
    </Row>
  </Container>
  </Aux>
);

export default UserProfileLite;
