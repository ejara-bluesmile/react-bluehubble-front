import React, { Component } from "react";
import {
  Row,
  Col,
  Card,
  Form,
  Button,
  InputGroup,
  FormControl,
  FormGroup,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import { Line } from "react-chartjs-2";
import Chart from "chart.js";
import "chartjs-plugin-streaming";
import socketClusterClient from "socketcluster-client";
import "./map.css";
import "../assets/scss/stream.scss";

let chartColors = {
  red: "rgb(255, 99, 132)",
  orange: "rgb(255, 159, 64)",
  yellow: "rgb(255, 205, 86)",
  green: "rgb(75, 192, 192)",
  blue: "rgb(54, 162, 235)",
  purple: "rgb(153, 102, 255)",
  grey: "rgb(201, 203, 207)",
};
const color = Chart.helpers.color;
Chart.defaults.global.defaultFontColor = "white";

let socket = socketClusterClient.create({
  hostname: "3.129.97.184",
  secure: false,
  port: 3005,
});

export default class stream2 extends Component {
  state = {
    temperature1: 0,
    temperature2: 0,
    cpu1: 0,
    cpu2: 0,
    inBandwidth: [],
    outBandwidth: [],
    ip1: "",
    ip2: "",
    site1: "",
    site2: "",
    freeBandwidthIn: 1000,
    freeBandwidthOut: 1000,
    sourceModel: "",
    sinkModel: "",
    showSolveTraffic: false,
    rx: [],
    tx: [],
  };
  graphic1 = React.createRef();
  graphic2 = React.createRef();
  inVal = -20;
  outVal = -20;
  solveTraffic = () => {
   if(socket) socket.transmit('trafficSolved', 'traffic solved')
  }
  graph() {
    this.graphic2.current.chartInstance.chart.config.data.datasets[0].data.push(
      {
        x: Date.now(),
        y: -20,
      }
    );
    this.graphic2.current.chartInstance.chart.config.data.datasets[1].data.push(
      {
        x: Date.now(),
        y: -20,
      }
    );
    // update chart datasets keeping the current animation
    this.graphic2.current.chartInstance.update({
      preservation: true,
    });
    setInterval(() => {
      const inDiff = Math.random() * 0.5;
      const outDiff = Math.random() * 0.5;
      const inPlusOrMinus = Math.random() < 0.5 ? -1 : 1;
      const outPlusOrMinus = Math.random() < 0.5 ? -1 : 1;
      this.inVal += inDiff * inPlusOrMinus;
      this.outVal += outDiff * outPlusOrMinus;
      // append the new data to the existing chart data
      this.graphic2.current.chartInstance.chart.config.data.datasets[0].data.push(
        {
          x: Date.now(),
          y: this.inVal.toFixed(2),
        }
      );
      this.graphic2.current.chartInstance.chart.config.data.datasets[1].data.push(
        {
          x: Date.now(),
          y: this.outVal.toFixed(2),
        }
      );
      // update chart datasets keeping the current animation
      this.graphic2.current.chartInstance.update({
        preservation: true,
      });
    }, 10000);
  }

  componentDidMount() {
    let initialParam = this.props.location.search.split("&");
    let arrayParam = initialParam[0].split("=");
    let nameParam = initialParam[1].split("=");
    this.setState({ idDevice: arrayParam[1] });
    this.setState({ serviceName: nameParam[1] });
    this.setState({ ip1: initialParam[2].split("=")[1] });
    this.setState({ ip2: initialParam[3].split("=")[1] });
    this.setState({ oid1: initialParam[4].split("=")[1] });
    this.setState({ oid2: initialParam[5].split("=")[1] });
    this.setState({ site1: initialParam[6].split("=")[1].replace("%20", " ") });
    this.setState({ site2: initialParam[7].split("=")[1].replace("%20", " ") });
    this.setState({ sourceModel: initialParam[8].split("=")[1] });
    this.setState({ sinkModel: initialParam[9].split("=")[1] });
    if (initialParam[10])
    this.setState({ showSolveTraffic: true });
    this.graph();
    (async () => {
      const channel = socket.listener("connect");
      for await (const data of channel) {
        (async () => {
          let channel = socket.subscribe(this.state.idDevice);
          for await (let data of channel) {
            let values = data.msgrx.split("/");
            const bandWidthValue = 1000000000 * (parseFloat(values[8]) / 100);
            switch (data.way) {
              case "in":
                // append the new data to the existing chart data
                this.graphic1.current.chartInstance.chart.config.data.datasets[0].data.push(
                  {
                    x: Date.now(),
                    y: parseFloat(values[8]),
                  }
                );
                // update chart datasets keeping the current animation
                this.graphic1.current.chartInstance.update({
                  preservation: true,
                });
                this.setState({
                  freeBandwidthIn:
                    1000 - parseFloat(values[8]),
                });
                break;
              case "out":
                this.graphic1.current.chartInstance.chart.config.data.datasets[1].data.push(
                  {
                    x: Date.now(),
                    y: parseFloat(values[8]),
                  }
                );
                // update chart datasets keeping the current animation
                this.graphic1.current.chartInstance.update({
                  preservation: true,
                });
                this.setState({
                  freeBandwidthOut:
                    1000 - parseFloat(values[8]),
                });
                break;
              default:
                break;
            }
          }
        })();
      }
    })();
  }

  componentWillUnmount() {
    if (socket) socket.disconnect();
  }

  render() {
    const chartStyle = {
      width: 230,
      marginleft: 0,
    };
    return (
      <>
       <div className="chart-container">
      {this.state.showSolveTraffic && <div>
            <center>
              <button
                style={{ "margin-bottom": "20px", "margin-top": "10px" }}
                className="solve-button" id="btSolve" onClick={() => { this.solveTraffic() }}>
                Solve Problem
              </button>
            </center>
          </div>
          }
          </div>
        <div className="stream3-sites-container">

          <div className="stream3-site-container">
            <h5>{this.state.site1.replace("%20", " ")}</h5>
            <div className="stream3-site">
              <b>Model: </b>
              <span>{this.state.sourceModel}</span>
              <br></br>
              <b>IP: </b>
              <span>{this.state.ip1}</span>
              <br></br>
              <b>Frequency Band: </b>
              <span> 13GHz</span>
              <br></br>
              <b>TX Frequency: </b>
              <span> 13.115MHz</span>
              <br></br>
              <b>RX Frequency: </b>
              <span> 12.849MHz</span>
              <br></br>
              <b>TX Power: </b>
              <span> 23dB</span>
              <br></br>
              <b>Modulation: </b>
              <span>128QAM/300Mbit</span>
              <br></br>
              <b>Available Bandwidth: </b><br></br>
              <span>{this.state.freeBandwidthOut} Mb</span>
            </div>
          </div>
          <div className="stream3-site-container">
            <h5>{this.state.site2.replace("%20", " ")}</h5>
            <div className="stream3-site">
              <b>Model: </b>
              <span>{this.state.sinkModel}</span>
              <br></br>
              <b>IP: </b>
              <span>{this.state.ip2}</span>
              <br></br>
              <b>Frequency Band: </b>
              <span> 13GHz</span>
              <br></br>
              <b>TX Frequency: </b>
              <span> 12.849MHz</span>
              <br></br>
              <b>RX Frequency: </b>
              <span> 13.115MHz</span>
              <br></br>
              <b>TX Power: </b>
              <span> 23dB</span>
              <br></br>
              <b>Modulation: </b>
              <span>128QAM/300Mbit</span>
              <br></br>
              <b>Available Bandwidth: </b><br></br>
              <span> {this.state.freeBandwidthIn} Mb</span>
            </div>
          </div>
        </div>
        <div className="chart-container">
          <Line
            data={{
              datasets: [
                {
                  fontSize: 10,
                  label:
                    "Speed for ifInOctects/(delta time) IP: " + this.state.ip1,
                  backgroundColor: color(chartColors.red)
                    .alpha(0.5)
                    .rgbString(),
                  borderColor: chartColors.red,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  // lineTension: 0,
                  // borderDash: [8, 4],
                  data: this.state.inBandwidth,
                },
                {
                  label:
                    "Speed for ifOutOctects/(delta time) IP: " + this.state.ip2,
                  backgroundColor: color(chartColors.blue)
                    .alpha(0.5)
                    .rgbString(),
                  borderColor: chartColors.blue,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  data: this.state.outBandwidth,
                },
              ],
            }}
            options={{
              legend: {
                labels: {
                  fontSize: 11.5,
                },
              },
              title: {
                color: "white",
                display: true,
                text: "Bandwidth Utilization Graphs",
              },
              scales: {
                xAxes: [
                  {
                    ticks: {
                      fontSize: 10,
                    },
                    type: "realtime",
                    realtime: {
                      duration: 20000,
                      refresh: 0,
                      delay: 6000,
                    },
                  },
                ],
                yAxes: [
                  {
                    ticks: {
                      fontSize: 10,
                    },
                    scaleLabel: {
                      display: true,
                      labelString: "value [Mbps]",
                    },
                  },
                ],
              },
              tooltips: {
                mode: "nearest",
                intersect: false,
              },
              hover: {
                mode: "nearest",
                intersect: false,
              },
              pan: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 4000,
                },
                rangeMin: {
                  x: 0,
                },
              },
              zoom: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 20000,
                },
                rangeMin: {
                  x: 1000,
                },
              },
            }}
            ref={this.graphic1}
          />
          <Line
            data={{
              datasets: [
                {
                  label: "Rx IP: " + this.state.ip2,
                  backgroundColor: color(chartColors.red)
                    .alpha(0.5)
                    .rgbString(),
                  borderColor: chartColors.red,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  data: [],
                },
                {
                  label: "Tx IP: " + this.state.ip1,
                  backgroundColor: color(chartColors.blue)
                    .alpha(0.5)
                    .rgbString(),
                  borderColor: chartColors.blue,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  data: [],
                },
              ],
            }}
            options={{
              title: {
                display: true,
                text: "Rx/Tx Levels",
              },
              chartArea: {
                backgroundColor: "rgba(251, 85, 85, 0.4)",
              },
              scales: {
                xAxes: [
                  {
                    ticks: {
                      fontSize: 10,
                    },
                    type: "realtime",
                    realtime: {
                      duration: 20000,
                      refresh: 0,
                      delay: 11000,
                    },
                  },
                ],
                yAxes: [
                  {
                    ticks: {
                      fontSize: 10,
                      min: -90,
                      max: 0,
                    },
                    scaleLabel: {
                      display: true,
                      labelString: "dBm",
                    },
                  },
                ],
              },
              tooltips: {
                mode: "nearest",
                intersect: false,
              },
              hover: {
                mode: "nearest",
                intersect: false,
              },
              pan: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 4000,
                },
                rangeMin: {
                  x: 0,
                },
              },
              zoom: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 20000,
                },
                rangeMin: {
                  x: 1000,
                },
              },
            }}
            ref={this.graphic2}
          />
        </div>
      </>
    );
  }
}
