import React, { Component, useState, useEffect, setAppState } from "react";
import $ from "jquery";
import axios from "axios";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import Aux from "../hoc/_Aux";
import "leaflet/dist/leaflet.css";
import { MarkerCluster } from "../App/components/map-utils/MarkerCluster";
import MapMenu from "./MapMenu";
import "./map.css";

export default class MapView extends Component {
  state = {
    isLoading: true,
    markers: [],
    list: [{ a: 1, b: 2 }],
    layer: 'OCH',
    showWarnings: true,
  };

  // TODO: Make the layers be available globally when the app starts
  async getLayers() {
    try {
      const apiUrl = "http://3.129.97.184:3001/findLayers";
      const layers = await axios.post(apiUrl);
      this.state.markers = layers.data.result.data;
      try {
        const apiUrl2 = "http://3.129.97.184:3003/findLayers";
        const layers2 = await axios.post(apiUrl2);
        this.state.markers = this.state.markers.concat(
          layers2.data.result.data
        );
        this.setState({ isLoading: false });
      } catch (e) { }
    } catch (e) {
      console.log(e);
      try {
        const apiUrl = "http://3.129.97.184:3003/findLayers";
        const layers = await axios.post(apiUrl);
        this.state.markers = layers.data.result.data;
        this.setState({ isLoading: false });
      } catch (e) {
        console.log(e);
      }
    }
  }
  filter() {
    return <div id="ctl7Container"></div>;
  }
  async componentDidMount() {
    try {
      await this.filter;
      await this.getLayers();
    } catch (e) {
      console.log(e);
    }
  }

  handleLayerChange = (layer) => {
    this.setState({ layer });
  }
  handleShowWarningsChange = () => {
    this.setState({ showWarnings: !this.state.showWarnings });
  }
   solveTraffic = () => {
    console.log('solve')
    // socket2.transmit('trafficSolved', 'traffic solved')
    // markersArray.map(mark => {
    //   mark.path.setStyle({
    //     fillColor: 'gray',
    //     color: 'gray'
    //   })
    //   map.removeLayer(mark.path)
    //   openedMarker.getPopup().setContent(content)
    // })
    // Swal.fire(
    //   'Problem Solved',
    //   'Bandwidth capacity expanded',
    //   'success'
    // )
  }


  render() {
    const { isLoading, markers, layer } = this.state;

    if (isLoading) {
      return <div className="App">Loading...</div>;
    }

    return (
      <Aux >
        <MapMenu layer={layer} onLayerChange={this.handleLayerChange}
          markers={this.state.markers} />
        <Map
          center={{ lat: "-33.5318784", lng: "-70.5908377" }}
          zoom={5}
          style={{ height: "90vh" }}
        >
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <div
            id="ctl7Container"
            style={{ width: "150px", height: "100px", "marginTop": "100px" }}
          ></div>
          {markers.map((layer) => (
            <MarkerCluster layer={layer} name={layer.name} key={layer.name}
              showWarnings={this.state.showWarnings} />
          ))}
        </Map>
      </Aux>
    );
  }
}
