import React, { Component } from 'react';
import { Card } from "react-bootstrap";
import "../assets/scss/inventory.scss"
import "../assets/scss/table.scss"
import TableComponent from '../components/table/TableInventory';
import axios from "axios";
import 'jquery/dist/jquery.min.js';
import $ from 'jquery';

class Inventory extends Component {

    state = {
        devices: [],
        head: ["HOSTNAME", "VENDOR", "TYPE", "DESCRIPTION", "IP", "OS", "PORT"],
        load: false
    }

    async getInventory() {
        try {

            const apiUrl = "http://13.58.191.209:40000/devices"
            const info = await axios.get(apiUrl);

            //console.log("metodo get", devices)
            this.setState({
                devices: info.data,
                load: true
            })
        } catch (e) {
            console.log(e)
        }

    };

    async componentDidMount() {
        //  this.interval = setInterval(this.getInventory, 6000);
        await Promise.all([
            this.getInventory()
        ]);


    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {

        let head = ["HOSTNAME", "VENDOR", "TYPE", "DESCRIPTION", "IP", "OS", "PORT"];
        return (
            <Card>
                <div>
                    <h2 className="pt-3 pb-4 text-center font-bold font-up deep-purple-text inventoryTitle">inventory</h2>
                </div>
                <TableComponent
                    heads={this.state.head}
                    itemsTable1={this.state.devices}
                    load={this.state.load}
                />
            </Card>

        );
    }
}

export default Inventory;