import React, { Component } from "react";
import {
  Row,
  Col,
  Card,
  Form,
  Button,
  InputGroup,
  FormControl,
  FormGroup,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import GaugeChart from "react-gauge-chart";
import { Line } from "react-chartjs-2";
import Chart from "chart.js";
import "chartjs-plugin-streaming";
import socketClusterClient from "socketcluster-client";
import "./map.css";
import '../assets/scss/stream.scss';

let chartColors = {
  red: "rgb(255, 99, 132)",
  orange: "rgb(255, 159, 64)",
  yellow: "rgb(255, 205, 86)",
  green: "rgb(75, 192, 192)",
  blue: "rgb(54, 162, 235)",
  purple: "rgb(153, 102, 255)",
  grey: "rgb(201, 203, 207)",
};
const color = Chart.helpers.color;
Chart.defaults.global.defaultFontColor = "white";

let socket = socketClusterClient.create({
  hostname: "3.129.97.184",
  secure: false,
  port: 3005,
});

export default class stream2 extends Component {
  state = {
    temperature1: 0,
    temperature2: 0,
    cpu1: 0,
    cpu2: 0,
    ip1: '',
    ip2: '',
    site1: '',
    site2: '',
    showSolveTraffic: false,
    inBandwidth: [],
    outBandwidth: [],
    inOctects: [],
    outOctects: [],
    rx: [],
    tx: [],
    bitErrorRatio: [],
    inAvailability: [],
    outAvailability: [],
    inJitter: [],
    outJitter: [],
    bitErrorRate: [],
    inTemperature: 0,
    outTemperature: 0,
    inCpu: 0,
    outCpu: 0,
  };
  graphic1 = React.createRef();
  graphic2 = React.createRef();
  graphic3 = React.createRef();
  graphic4 = React.createRef();
  graphic5 = React.createRef();
  graphic6 = React.createRef();
  graphic7 = React.createRef();
  graphic8 = React.createRef();

  solveTraffic = () => {
    if(socket)socket.transmit('trafficSolved', 'traffic solved')
  }

  graph() {
    (async () => {
      let channel = socket.subscribe(this.state.idDevice);
      for await (let data of channel) {
        let values = data.msgrx.split("/");
        console.log(this.graphic1)
        switch (data.way) {
          case 'in':
            // append the new data to the existing chart data
            this.graphic1.current.chartInstance.chart.config.data.datasets[0].data.push({
              x: Date.now(),
              y: parseFloat(values[8])
            });
            this.graphic2.current.chartInstance.chart.config.data.datasets[0].data.push({
              x: Date.now(),
              y: parseFloat(values[0])
            });
            this.graphic4.current.chartInstance.chart.config.data.datasets[0].data.push({
              x: Date.now(),
              y: parseFloat(values[1])
            });
            this.graphic5.current.chartInstance.chart.config.data.datasets[0].data.push({
              x: Date.now(),
              y: parseFloat(values[11])
            });
            this.graphic6.current.chartInstance.chart.config.data.datasets[0].data.push({
              x: Date.now(),
              y: parseFloat(values[13])
            });
            this.graphic7.current.chartInstance.chart.config.data.datasets[0].data.push({
              x: Date.now(),
              y: parseFloat(values[15])
            });
            this.graphic8.current.chartInstance.chart.config.data.datasets[0].data.push({
              x: Date.now(),
              y: parseFloat(values[10])
            });

            // update chart datasets keeping the current animation
            this.graphic1.current.chartInstance.update({
              preservation: true
            });
            this.graphic2.current.chartInstance.update({
              preservation: true
            });
            this.graphic4.current.chartInstance.update({
              preservation: true
            });
            this.graphic5.current.chartInstance.update({
              preservation: true
            });
            this.graphic6.current.chartInstance.update({
              preservation: true
            });
            this.graphic7.current.chartInstance.update({
              preservation: true
            });
            this.graphic8.current.chartInstance.update({
              preservation: true
            });
            this.setState({
              inCpu: parseFloat(values[2]) / 100,
              inTemperature: parseFloat(values[3]) / 100
            });
            break;
          case 'out':
            this.graphic1.current.chartInstance.chart.config.data.datasets[1].data.push({
              x: Date.now(),
              y: parseFloat(values[8])
            });
            this.graphic3.current.chartInstance.chart.config.data.datasets[0].data.push({
              x: Date.now(),
              y: parseFloat(values[0])
            });
            this.graphic4.current.chartInstance.chart.config.data.datasets[1].data.push({
              x: Date.now(),
              y: parseFloat(values[1])
            });
            this.graphic6.current.chartInstance.chart.config.data.datasets[1].data.push({
              x: Date.now(),
              y: parseFloat(values[13])
            });
            this.graphic7.current.chartInstance.chart.config.data.datasets[1].data.push({
              x: Date.now(),
              y: parseFloat(values[15])
            });

            // update chart datasets keeping the current animation
            this.graphic1.current.chartInstance.update({
              preservation: true
            });
            this.graphic3.current.chartInstance.update({
              preservation: true
            });
            this.graphic4.current.chartInstance.update({
              preservation: true
            });
            this.graphic6.current.chartInstance.update({
              preservation: true
            });
            this.graphic7.current.chartInstance.update({
              preservation: true
            });
            this.setState({
              outCpu: parseFloat(values[2]) / 100,
              outTemperature: parseFloat(values[3]) / 100
            });
            break;
          default:
            break;
        }
      };
    })();
  }

  componentDidMount() {

    let initialParam = this.props.location.search.split("&");
    let arrayParam = initialParam[0].split("=");
    let nameParam = initialParam[1].split("=");
    this.setState({ idDevice: arrayParam[1] });
    this.setState({ serviceName: nameParam[1] });
    this.setState({ ip1: initialParam[2].split("=")[1] });
    this.setState({ ip2: initialParam[3].split("=")[1] });
    this.setState({ oid1: initialParam[4].split("=")[1] });
    this.setState({ oid2: initialParam[5].split("=")[1] });
    this.setState({ site1: initialParam[6].split("=")[1] });
    this.setState({ site2: initialParam[7].split("=")[1] });
    if (initialParam[8])
      this.setState({ showSolveTraffic: true });
    (async () => {
      const channel = socket.listener('connect');
      for await (const data of channel) {
        this.graph();
      }
    })();
  }

  componentWillUnmount() {
    if (socket) socket.disconnect();
  }

  render() {
    const chartStyle = {
      width: 230,
      marginleft: 0,
    };
    return (
      <>
        <div className="chart-container">
          {this.state.showSolveTraffic && <div>
            <center>
              <button
                style={{ "margin-bottom": "20px", "margin-top": "10px" }}
                className="solve-button" id="btSolve" onClick={() => { this.solveTraffic() }}>
                Solve Problem
              </button>
            </center>
          </div>
          }
          <center>
            <h4 class="white-color">Blue Hubble RealTime Streaming</h4>
          </center>
          <Line
            data={{
              datasets: [
                {
                  fontSize: 10,
                  label:
                    "Speed for ifInOctects/(delta time) IP: " + this.state.ip1,
                  backgroundColor: color(chartColors.red).alpha(0.5).rgbString(),
                  borderColor: chartColors.red,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  // lineTension: 0,
                  // borderDash: [8, 4],
                  data: this.state.inBandwidth,
                },
                {
                  label:
                    "Speed for ifOutOctects/(delta time) IP: " + this.state.ip2,
                  backgroundColor: color(chartColors.blue).alpha(0.5).rgbString(),
                  borderColor: chartColors.blue,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  data: this.state.outBandwidth,
                },
              ],
            }}
            options={{
              legend: {
                labels: {
                  fontSize: 11.5,
                }
              },
              title: {
                color: "white",
                display: true,
                text: "Bandwidth Utilization Graphs",
              },
              scales: {
                xAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    type: "realtime",
                    realtime: {
                      duration: 20000,
                      refresh: 0,
                      delay: 6000,
                    },
                  },
                ],
                yAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    scaleLabel: {
                      display: true,
                      labelString: "value [Mbps]",
                    },
                  },
                ],
              },
              tooltips: {
                mode: "nearest",
                intersect: false,
              },
              hover: {
                mode: "nearest",
                intersect: false,
              },
              pan: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 4000,
                },
                rangeMin: {
                  x: 0,
                },
              },
              zoom: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 20000,
                },
                rangeMin: {
                  x: 1000,
                },
              },
            }}
            ref={this.graphic1}
          />
          <Line
            data={{
              datasets: [
                {
                  label: "ifInOctects IP: " + this.state.ip1,
                  backgroundColor: color(chartColors.red).alpha(0.5).rgbString(),
                  borderColor: chartColors.red,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  //lineTension: 0,
                  //borderDash: [8, 4],
                  data: this.state.inOctects,
                },
              ],
            }}
            options={{
              legend: {
                labels: {
                  fontSize: 11.5,
                }
              },
              title: {
                display: true,
                text: "ifInOctects Counter Graphs",
              },
              chartArea: {
                backgroundColor: "rgba(251, 85, 85, 0.4)",
              },
              scales: {
                xAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    type: "realtime",
                    realtime: {
                      duration: 20000,
                      refresh: 0,
                      delay: 6000,
                    },
                  },
                ],
                yAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    scaleLabel: {
                      display: true,
                      labelString: "value [Pkts]",
                    },
                  },
                ],
              },
              tooltips: {
                mode: "nearest",
                intersect: false,
              },
              hover: {
                mode: "nearest",
                intersect: false,
              },
              pan: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 4000,
                },
                rangeMin: {
                  x: 0,
                },
              },
              zoom: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 20000,
                },
                rangeMin: {
                  x: 1000,
                },
              },
            }}
          ref={this.graphic2}
          />
          <Line
            data={{
              datasets: [
                {
                  label: "ifOutOctects IP: " + this.state.ip2,
                  backgroundColor: color(chartColors.blue).alpha(0.5).rgbString(),
                  borderColor: chartColors.blue,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  data: this.state.outOctects,
                },
              ],
            }}
            options={{
              legend: {
                labels: {
                  fontSize: 11.5,
                }
              },
              title: {
                display: true,
                text: "ifOutOctects Counter Graphs",
              },
              chartArea: {
                backgroundColor: "rgba(251, 85, 85, 0.4)",
              },
              scales: {
                xAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    type: "realtime",
                    realtime: {
                      duration: 20000,
                      refresh: 0,
                      delay: 6000,
                    },
                  },
                ],
                yAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    scaleLabel: {
                      display: true,
                      labelString: "value [Pkts]",
                    },
                  },
                ],
              },
              tooltips: {
                mode: "nearest",
                intersect: false,
              },
              hover: {
                mode: "nearest",
                intersect: false,
              },
              pan: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 4000,
                },
                rangeMin: {
                  x: 0,
                },
              },
              zoom: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 20000,
                },
                rangeMin: {
                  x: 1000,
                },
              },
            }}
          ref={this.graphic3}
          />
          <Line
            data={{
              datasets: [
                {
                  label: "Rx IP: " + this.state.ip1,
                  backgroundColor: color(chartColors.red).alpha(0.5).rgbString(),
                  borderColor: chartColors.red,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  // borderWidth: 1,
                  data: this.state.rx,
                },
                {
                  label: "Tx IP: " + this.state.ip2,
                  backgroundColor: color(chartColors.blue).alpha(0.5).rgbString(),
                  borderColor: chartColors.blue,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  // borderWidth: 1,
                  data: this.state.tx,
                },
              ],
            }}
            options={{
              title: {
                display: true,
                text: "Optical power",
              },
              chartArea: {
                backgroundColor: "rgba(251, 85, 85, 0.4)",
              },
              scales: {
                xAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    type: "realtime",
                    realtime: {
                      duration: 20000,
                      refresh: 0,
                      delay: 6000,
                    },
                  },
                ],
                yAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    scaleLabel: {
                      display: true,
                      labelString: "dBm",
                    },
                  },
                ],
              },
              tooltips: {
                mode: "nearest",
                intersect: false,
              },
              hover: {
                mode: "nearest",
                intersect: false,
              },
              pan: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 4000,
                },
                rangeMin: {
                  x: 0,
                },
              },
              zoom: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 20000,
                },
                rangeMin: {
                  x: 1000,
                },
              },
            }}
          ref={this.graphic4}
          />
          <Line
            data={{
              legend: {
                labels: {
                  fontSize: 11.5,
                }
              },
              datasets: [
                {
                  label: "Bit Error Ratio IP: " + this.state.ip1,
                  backgroundColor: color(chartColors.red).alpha(0.5).rgbString(),
                  borderColor: chartColors.red,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  //lineTension: 0,
                  //borderDash: [8, 4],
                  data: this.state.bitErrorRatio,
                },
              ],
            }}
            options={{
              title: {
                display: true,
                text: "Bit Error Ratio Graphs",
              },
              chartArea: {
                backgroundColor: "rgba(251, 85, 85, 0.4)",
              },
              scales: {
                xAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    type: "realtime",
                    realtime: {
                      duration: 20000,
                      refresh: 0,
                      delay: 6000,
                    },
                  },
                ],
                yAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    scaleLabel: {
                      display: true,
                      labelString: "value [Bits]",
                    },
                  },
                ],
              },
              tooltips: {
                mode: "nearest",
                intersect: false,
              },
              hover: {
                mode: "nearest",
                intersect: false,
              },
              pan: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 4000,
                },
                rangeMin: {
                  x: 0,
                },
              },
              zoom: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 20000,
                },
                rangeMin: {
                  x: 1000,
                },
              },
            }}
            ref={this.graphic5}
          />
          <Line
            data={{
              legend: {
                labels: {
                  fontSize: 11.5,
                }
              },
              datasets: [
                {
                  label: "Availability for IN device IP: " + this.state.ip1,
                  backgroundColor: color(chartColors.red).alpha(0.5).rgbString(),
                  borderColor: chartColors.red,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  //lineTension: 0,
                  //borderDash: [8, 4],
                  data: this.state.inAvailability,
                },
                {
                  label: "Availability for OUT device IP: " + this.state.ip2,
                  backgroundColor: color(chartColors.blue).alpha(0.5).rgbString(),
                  borderColor: chartColors.blue,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  data: this.state.outAvailability,
                },
              ],
            }}
            options={{
              title: {
                display: true,
                text: "Availability Graphs",
              },
              scales: {
                xAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    type: "realtime",
                    realtime: {
                      duration: 20000,
                      refresh: 0,
                      delay: 6000,
                    },
                  },
                ],
                yAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    scaleLabel: {
                      display: true,
                      labelString: "value [%]",
                    },
                  },
                ],
              },
              tooltips: {
                mode: "nearest",
                intersect: false,
              },
              hover: {
                mode: "nearest",
                intersect: false,
              },
              pan: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 4000,
                },
                rangeMin: {
                  x: 0,
                },
              },
              zoom: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 20000,
                },
                rangeMin: {
                  x: 1000,
                },
              },
            }}
            ref={this.graphic6}
         />
          <Line
            data={{
              legend: {
                labels: {
                  fontSize: 11.5,
                }
              },
              datasets: [
                {
                  label: "Jitter for IN device IP: " + this.state.ip1,
                  backgroundColor: color(chartColors.red).alpha(0.5).rgbString(),
                  borderColor: chartColors.red,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  //lineTension: 0,
                  //borderDash: [8, 4],
                  data: this.state.inJitter,
                },
                {
                  label: "Jitter for OUT device IP: " + this.state.ip2,
                  backgroundColor: color(chartColors.blue).alpha(0.5).rgbString(),
                  borderColor: chartColors.blue,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  data: this.state.outJitter,
                },
              ],
            }}
            options={{
              title: {
                display: true,
                text: "Jitter Graphs",
              },
              scales: {
                xAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    type: "realtime",
                    realtime: {
                      duration: 20000,
                      refresh: 0,
                      delay: 6000,
                    },
                  },
                ],
                yAxes: [
                  {
                    ticks: {
                      fontSize: 10
                    },
                    scaleLabel: {
                      display: true,
                      labelString: "value [ms]",
                    },
                  },
                ],
              },
              tooltips: {
                mode: "nearest",
                intersect: false,
              },
              hover: {
                mode: "nearest",
                intersect: false,
              },
              pan: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 4000,
                },
                rangeMin: {
                  x: 0,
                },
              },
              zoom: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 20000,
                },
                rangeMin: {
                  x: 1000,
                },
              },
            }}
            ref={this.graphic7}
          />
          <Line
            data={{
              datasets: [
                {
                  label: "Bit Error Rate IP: " + this.state.ip1,
                  backgroundColor: color(chartColors.red).alpha(0.5).rgbString(),
                  borderColor: chartColors.red,
                  fill: true,
                  cubicInterpolationMode: "monotone",
                  //lineTension: 0,
                  //borderDash: [8, 4],
                  data: this.state.bitErrorRate,
                },
              ],
            }}
            options={{
              title: {
                display: true,
                text: "Bit Error Rate Graphs",
              },
              chartArea: {
                backgroundColor: "rgba(251, 85, 85, 0.4)",
              },
              scales: {
                xAxes: [
                  {
                    type: "realtime",
                    realtime: {
                      duration: 20000,
                      refresh: 0,
                      delay: 6000,
                    },
                  },
                ],
                yAxes: [
                  {
                    scaleLabel: {
                      display: true,
                      labelString: "value [Bits]",
                    },
                  },
                ],
              },
              tooltips: {
                mode: "nearest",
                intersect: false,
              },
              hover: {
                mode: "nearest",
                intersect: false,
              },
              pan: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 4000,
                },
                rangeMin: {
                  x: 0,
                },
              },
              zoom: {
                enabled: true,
                mode: "x",
                rangeMax: {
                  x: 20000,
                },
                rangeMin: {
                  x: 1000,
                },
              },
            }}
            ref={this.graphic8}
          />

          <div style={{ display: "inline-flex", marginTop: "100px" }}>
            <div className="gauge-container" style={{ display: "flex" }}>
              <h6 className="site">{this.state.site1.replace("%20", " ")}</h6>
              <h6 className="white-color cpu-temp" id="degreeh7">Degree</h6>
              <GaugeChart
                id="gauge"
                formatTextValue={(value) => `${value}°C`}
                nrOfLevels={6}
                percent={this.state.inTemperature}
                style={chartStyle}
              ></GaugeChart>{" "}
              <h6 className="white-color cpu-temp" id="CPUh7">CPU Utilization</h6>
              <GaugeChart
                id="gauge2"
                vertical-align="middle"
                percent={this.state.inCpu}
                nrOfLevels={5}
                style={chartStyle}
              ></GaugeChart>
            </div>
            <div className="gauge-container" style={{ display: "flex" }}>
              <h6 className="site">{this.state.site2.replace("%20", " ")}</h6>
              <h6 className="white-color cpu-temp" id="degreeh7">Degree</h6>
              <GaugeChart
                id="gauge3"
                vertical-align="middle"
                nrOfLevels={6}
                formatTextValue={(value) => `${value}°C`}
                percent={this.state.outTemperature}
                style={chartStyle}
              ></GaugeChart>{" "}
              <h6 className="white-color cpu-temp" id="CPUh7">CPU Utilization</h6>
              <GaugeChart
                id="gauge4"
                vertical-align="middle"
                percent={this.state.outCpu}
                nrOfLevels={5}
                style={chartStyle}
              ></GaugeChart>
            </div>
          </div>
          <div style={{ margin: "40px", "margin-bottom": "0px" }}>
            <center>
              <div>
                {/* <button className="" id="btConectar">
                Connect
              </button>
              <button className="" id="btLoop">
                Query(RT)
              </button> */}
                <button
                  style={{ "margin-bottom": "5px" }}
                  className="graph-button" id="btPesquisar" onClick={() => { this.graph() }}>
                  Graph
              </button>
              </div>
            </center>
          </div>
          <br />
          <div>
            <center>
              <span>View Historic Records for this device</span>
            </center>
            <center style={{ "margin-top": "20px" }}>
              {" "}
              <button className="graph-button" id="btHistory">
                View Records
            </button>
            </center>
          </div>
        </div>
      </>
    );
  }
}
