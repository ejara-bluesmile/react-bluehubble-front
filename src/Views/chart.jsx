import React, { Component } from "react";
import axios from "axios";
import {
    Row,
    Col,
    Card,
    Form,
    Button,
    InputGroup,
    FormControl,
    DropdownButton,
    Dropdown,
} from "react-bootstrap";
import "./map.css";
import socketClusterClient from "socketcluster-client";
import EasyTimer from "easytimer";
import Aux from "../hoc/_Aux";
import math from "math";
import redButton from "../assets/images/redbutton.png";
import greenButton from "../assets/images/greenbutton.png";
import yellowButton from "../assets/images/yellowbutton.png";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import highchartsAccessibility from "highcharts/modules/accessibility";
import HC_exporting from "highcharts/modules/exporting";
import HC_exportingOffline from "highcharts/modules/offline-exporting";
import HC_exportingData from "highcharts/modules/export-data";
import HC_more from "highcharts/highcharts-more";
import Boost from "highcharts/modules/boost";
import "../style/col-8.css";
import Chart from "../Charts/pie";
import Donut from "../Charts/donut";
import Semicircle from "../Charts/semicircle";
import Line from "../Charts/line";

import ErrorCardBoody from "../components/graphComponent/errorCardBody";
import InformationTable from "../components/graphComponent/informationTable"
import CardGraphEvents from "../components/graphComponent/cardGraphEvents"
import CardGraphAvalibility from "../components/graphComponent/cardGraphAvailability"
import CardGraphEvent2 from "../components/graphComponent/cardGraphEvent2"
import CardGraphBandwithEvent from "../components/graphComponent/cardGraphBandwithEvent"
import CardGraphOpticalpowerEvent from '../components/graphComponent/cardGraphOpticalPowerEvents';
import CardGraphErrorRatioEvent from './../components/graphComponent/cardGraphErrorRatioEvent';
import CardgraphErrorRateEvent from './../components/graphComponent/cardGraphErrorRateEvent';
import CardgraphDelayEvent from './../components/graphComponent/cardGraphDelayEvent';
import CardGraphJitterEvents from './../components/graphComponent/cardGraphJitterEvents';
import CardGraphCpuEvent from './../components/graphComponent/cardGraphCpuEvent';
import CardGraphTemperatureEvent from './../components/graphComponent/cardGraphTemperatureEvent';
import CardGraphBandWith from './../components/graphComponent/cardgraphBandWith';
import CardGraphOpticalPower from '../components/graphComponent/cardgraphOpticalPower';
import CardGraphErrorRatio from './../components/graphComponent/cardGraphErrorRatio'
import CardGraphErrorRate from './../components/graphComponent/cardGraphErrorRate'
import CardGraphDelay from './../components/graphComponent/cardGraphDelay'
import CardGraphJitter from './../components/graphComponent/cardGraphJitter'
import CardGraphCpu from './../components/graphComponent/cardGraphCpu'
import CardGraphTemperature from './../components/graphComponent/cardGraphTemperature'
import CardGraphFailures from './../components/graphComponent/cardGraphFailures'
import CardGraphLostPacket from './../components/graphComponent/cardGraphLostPacket'
import CardGraphTimeouts from './../components/graphComponent/cardGraphTimeouts'


const startedTime = new Date();
let isVpnDown = false;
let socket;

export default class AnaliticIP extends Component {
    state = {
        tipo: "IP",

        jitter: [],
        delay: [],
        bitErrorRate: [],
        bitErrorRatio: [],
        opticalPower: [],
        availability: {
            intervals: [{
                values: []
            }, {
                values: []
            }, {
                values: []
            }, {
                values: []
            }, {
                values: []
            }, {
                values: []
            }, {
                values: []
            }, {
                values: []
            }, {
                values: []
            }, {
                values: []
            }, {
                values: []
            }]
        },
        devicesWithOutLostPackages: [],

        OndatafindLastDayAnalitica: true,

        timer: new EasyTimer(),
        onlineTimer: new EasyTimer(),
        timeValues: "",
        onlineTime: {
            Day: "0",
            Hours: "0",
            Minutes: "0",
            Seconds: "0"
        },

        isLoading: true,
        status: redButton,
        queries: 0,
        timeouts: 0,
        packets: 0,
        warninigs: 0,
        ids: [],
        lastRecord: {
            availability: {
                intervals: [{
                    values: []
                }, {
                    values: []
                }, {
                    values: []
                }, {
                    values: []
                }, {
                    values: []
                }, {
                    values: []
                }, {
                    values: []
                }, {
                    values: []
                }, {
                    values: []
                }, {
                    values: []
                }, {
                    values: []
                }]
            }
        },
        lastDate: "",
        lastDateTimeout: "",
        devicesWithLostPackets: [],
        devicesWithOutLostPackets: [],
        devicesWithOutTimeouts: [],
        devicesWithTimeouts: [],
        devicesWithWarnings: [],
        devicesWithOutWarnings: [],
        devicesWithBandWarnings: [],
        devicesWithOutBandWarnings: [],
        devicesWithTempWarnings: [],
        devicesWithOutTempWarnings: [],
        devicesWithOpticalWarnings: [],
        devicesWithOutOpticalWarnings: [],
        devicesWithCpuWarnings: [],
        devicesWithOutCpuWarnings: [],
        devicesWithDelayWarnings: [],
        devicesWithOutDelayWarnings: [],
        devicesWithJitterWarnings: [],
        devicesWithOutJitterWarnings: [],
        devicesWithBitErrorRateWarnings: [],
        devicesWithOutBitErrorRateWarnings: [],
        devicesWithBitErrorRatioWarnings: [],
        devicesWithOutBitErrorRatioWarnings: [],
        availabilities: [],
        delays: [],
        jitters: [],
        beRates: [],
        beRatios: [],
        cpus: [],
        bandWidths: [],
        temperatures: [],
        opticals: [],
        mtbfs: [],
        RecordAvailabilities: [],
        RecordDelays: [],
        RecordJitters: [],
        RecordBitErrorRates: [],
        RecordBitErrorRatios: [],
        RecordCpus: [],
        RecordBandwidths: [],
        RecordTemperatures: [],
        RecordOpticalPowers: [],
        RecordMtbfs: [],
        bandwidthWarnCounts: 0,
        temperatureWarnCounts: 0,
        opticalWarnsCounts: 0,
        cpuWarnsCounts: 0,
        delayWarnsCounts: 0,
        jitterWarnsCounts: 0,
        bitErrorRateWarnsCounts: 0,
        bitErrorRatioWarnsCounts: 0,
        lastDateBandwidth: "",
        lastDateTemperature: "",
        lastDateOptical: "",
        lastDateCpu: "",
        lastDateDelay: "",
        lastDateJitter: "",
        lastDateBitErrorRate: "",
        lastDateBitErrorRatio: "",
        lastDatePacket: "",
        lastValueBandwidth: 0,
        lastValueTemperature: 0,
        lastValueOptical: 0,
        lastValueCpu: 0,
        lastValueDelay: 0,
        lastValueJitter: 0,
        lastValueBitErrorRate: 0,
        lastValueBitErrorRatio: 0,
        minBandwidth: 0,
        maxBandwidth: 0,
        avgBandwidth: 0,
        stdBandwidth: 0,
        minTemperature: 0,
        maxTemperature: 0,
        avgTemperature: 0,
        stdTemperature: 0,
        minDelay: 0,
        maxDelay: 0,
        avgDelay: 0,
        stdDelay: 0,
        minJitter: 0,
        maxJitter: 0,
        avgJitter: 0,
        stdJitter: 0,
        minOptical: 0,
        maxOptical: 0,
        avgOptical: 0,
        stdOptical: 0,
        minCpu: 0,
        maxCpu: 0,
        avgCpu: 0,
        stdCpu: 0,
        minBitErrorRate: 0,
        maxBitErrorRate: 0,
        avgBitErrorRate: 0,
        stdBitErrorRate: 0,
        minBitErrorRatio: 0,
        maxBitErrorRatio: 0,
        avgBitErrorRatio: 0,
        stdBitErrorRatio: 0,
        minMtbf: 0,
        maxMtbf: 0,
        avgMtbf: 0,
        stdMtbf: 0,
        minAvailability: 0,
        maxAvailability: 0,
        avgAvailability: 0,
        stdAvailability: 0,
        dataQuantity: 0,
    };

    async clean() {
        this.setState({
            availabilitys: [],
            devicesWithOutLostPackages: [],

            devicesWithLostPackets: [],
            devicesWithOutLostPackets: [],
            devicesWithOutTimeouts: [],
            devicesWithTimeouts: [],
            devicesWithWarnings: [],
            devicesWithOutWarnings: [],
            devicesWithBandWarnings: [],
            devicesWithOutBandWarnings: [],
            devicesWithTempWarnings: [],
            devicesWithOutTempWarnings: [],
            devicesWithOpticalWarnings: [],
            devicesWithOutOpticalWarnings: [],
            devicesWithCpuWarnings: [],
            devicesWithOutCpuWarnings: [],
            devicesWithDelayWarnings: [],
            devicesWithOutDelayWarnings: [],
            devicesWithJitterWarnings: [],
            devicesWithOutJitterWarnings: [],
            devicesWithBitErrorRateWarnings: [],
            devicesWithOutBitErrorRateWarnings: [],
            devicesWithBitErrorRatioWarnings: [],
            devicesWithOutBitErrorRatioWarnings: [],
            availabilities: [],
            delays: [],
            jitters: [],
            beRates: [],
            beRatios: [],
            cpus: [],
            bandWidths: [],
            temperatures: [],
            opticals: [],
            mtbfs: [],
            RecordAvailabilities: [],
            RecordDelays: [],
            RecordJitters: [],
            RecordBitErrorRates: [],
            RecordBitErrorRatios: [],
            RecordCpus: [],
            RecordBandwidths: [],
            RecordTemperatures: [],
            RecordOpticalPowers: [],
            RecordMtbfs: [],
            bandwidthWarnCounts: 0,
            temperatureWarnCounts: 0,
            opticalWarnsCounts: 0,
            cpuWarnsCounts: 0,
            delayWarnsCounts: 0,
            jitterWarnsCounts: 0,
            bitErrorRateWarnsCounts: 0,
            bitErrorRatioWarnsCounts: 0,
        });
    }

    async getDevices() {
        try {
            const apiUrl = "http://3.129.97.184:3001/findIds";
            const info = await axios.post(apiUrl);
            let ids = info.data.result.data;
            this.state.ids = ids.flatMap((i) => [`${i}in`, `${i}out`]);
        } catch (e) {
            console.log(e);
        }
    }


    async getRecords() {
        try {
            const apiUrl = "http://3.129.97.184:3001/findLastDayAnalitica";
            const info = await axios.post(apiUrl);



            if (info.data.result.data.length > 0) {
                const delaysAvgs = this.state.delays.map((avg) => avg.data);
                let dataQuantity = delaysAvgs.length
                const lastRecord = info.data.result.data[0];


                this.setState({
                    jitter: lastRecord.jitter,
                    delay: lastRecord.delay,
                    dataQuantity,
                    availability: lastRecord.availability,
                    opticalPower: lastRecord.opticalPower,
                    stdOptical: lastRecord.opticalPower.std,
                    bitErrorRate: lastRecord.bitErrorRate,
                    bitErrorRatio: lastRecord.bitErrorRatio,

                    devicesWithLostPackets: lastRecord.packages.lost,
                    devicesWithOutLostPackets: lastRecord.packages.withoutLost,
                    lastDate: lastRecord.time,
                    devicesWithLostPackets: lastRecord.packages.lost,
                    devicesWithOutLostPackets: lastRecord.packages.withoutLost,
                    // devicesWithOutTimeouts:lastRecord.
                    lastRecord,
                    RecordAvailabilities: lastRecord.availability,
                    RecordDelays: lastRecord.delay,
                    RecordJitters: lastRecord.jitter,
                    RecordBitErrorRates: lastRecord.bitErrorRate,
                    RecordBitErrorRatios: lastRecord.bitErrorRatio,
                    RecordCpus: lastRecord.cpuUsage,
                    RecordBandwidths: lastRecord.bandwidth,
                    RecordTemperatures: lastRecord.temperature,
                    RecordOpticalPowers: lastRecord.opticalPower,
                    RecordMtbfs: lastRecord.mtbf,
                });
                for (let i = 0; i < 10; i++) {
                    this.setState({
                        delays: this.state.delays.concat(
                            lastRecord.delay.intervals[i].values
                        ),
                        jitters: this.state.jitters.concat(
                            lastRecord.jitter.intervals[i].values
                        ),
                        beRates: this.state.beRates.concat(
                            lastRecord.bitErrorRate.intervals[i].values
                        ),
                        beRatios: this.state.beRatios.concat(
                            lastRecord.bitErrorRatio.intervals[i].values
                        ),
                        cpus: this.state.cpus.concat(
                            lastRecord.cpuUsage.intervals[i].values
                        ),
                        bandWidths: this.state.bandWidths.concat(
                            lastRecord.bandwidth.intervals[i].values
                        ),
                        availabilities: this.state.availabilities.concat(
                            lastRecord.availability.intervals[i].values
                        ),
                        temperatures: this.state.temperatures.concat(
                            lastRecord.temperature.intervals[i].values
                        ),
                        opticals: this.state.opticals.concat(
                            lastRecord.opticalPower.intervals[i].values
                        ),
                        mtbfs: this.state.mtbfs.concat(lastRecord.mtbf.intervals[i].values),

                    });
                }
                this.setState({
                    availabilities: this.state.availabilities.concat(
                        lastRecord.availability.intervals[10].values
                    ),
                    temperatures: this.state.temperatures.concat(
                        lastRecord.temperature.intervals[10].values
                    ),
                    opticals: this.state.opticals.concat(
                        lastRecord.opticalPower.intervals[10].values
                    ),
                    mtbfs: this.state.mtbfs.concat(lastRecord.mtbf.intervals[10].values),
                });
            } else {
                let availabilities = this.state.ids.map((id) => {
                    let OndatafindLastDayAnalitica = false
                    this.setState({ OndatafindLastDayAnalitica })
                    return {
                        name: id,
                        data: 100,
                        sum: 100,
                        dataAmount: 1,
                        interval: 10,
                    };
                });
                this.setState({ availabilities })
                this.state.devicesWithOutLostPackets = this.state.ids;
            }
        } catch (e) {
            console.log(e);
        }
    }
    //filtra las alertas 
    async getGeneralWarnings() {
        const apiUrl = "http://3.129.97.184:3001/findWarnDevices";
        const info = await axios.post(apiUrl);
        let devicesWithWarnings = info.data.result.data
            .filter((obj) => {
                return this.state.ids.includes(obj._id);
            })
            .map((device) => device._id);
        this.setState({
            devicesWithWarnings,
            devicesWithOutWarnings: this.state.ids.filter(
                (x) => !devicesWithWarnings.includes(x)
            ),
        });
    }

    async getWarningsTypes() {
        const apiUrl = "http://3.129.97.184:3001/findWarnDevicesByType";
        const info = await axios.post(apiUrl);
        if (info.data.result.dataFin) {
            let dataFin = JSON.parse(info.data.result.dataFin);
            this.setState({
                bandwidthWarnCounts: (this.state.bandwidthWarnCounts +=
                    dataFin.quantity),
                devicesWithBandWarnings: this.state.devicesWithBandWarnings.concat(
                    dataFin.ids
                ),
                lastDateBandwidth: dataFin.lastTime.replace("T", " ").replace("Z", ""),
                lastValueBandwidth: dataFin.lastValue,
            });
        }
        if (info.data.result.dataFout) {
            let dataFout = JSON.parse(info.data.result.dataFout);
            this.setState({
                bandwidthWarnCounts: (this.state.bandwidthWarnCounts +=
                    dataFout.quantity),
                bandwidthOutWarnCounts: (this.state.bandwidthWarnCounts +=
                    dataFout.quantity),
                devicesWithBandWarnings: this.state.devicesWithBandWarnings.concat(
                    dataFout.ids
                ),
                lastDateBandwidth: dataFout.lastTime.replace("T", " ").replace("Z", ""),
                lastValueBandwidth: dataFout.lastValue,
            });
        }
        if (info.data.result.datatemperature) {
            let datatemperature = JSON.parse(info.data.result.datatemperature);
            this.setState({
                temperatureWarnCounts: (this.state.temperatureWarnCounts +=
                    datatemperature.quantity),
                devicesWithTempWarnings: this.state.devicesWithTempWarnings.concat(
                    datatemperature.ids
                ),
                lastDateTemperature: datatemperature.lastTime
                    .replace("T", " ")
                    .replace("Z", ""),
                lastValueTemperature: datatemperature.lastValue,
            });
        }
        if (info.data.result.dataRx) {
            let dataRx = JSON.parse(info.data.result.dataRx);
            this.setState({
                opticalWarnsCounts: (this.state.opticalWarnsCounts += dataRx.quantity),
                devicesWithOpticalWarnings: this.state.devicesWithOpticalWarnings.concat(
                    dataRx.ids
                ),
                lastDateOptical: dataRx.lastTime.replace("T", " ").replace("Z", ""),
                lastValueOptical: dataRx.lastValue,
            });
        }
        if (info.data.result.dataTx) {
            let dataTx = JSON.parse(info.data.result.dataTx);
            this.setState({
                opticalWarnsCounts: (this.state.opticalWarnsCounts += dataTx.quantity),
                opticalTxWarnsCounts: (this.state.opticalWarnsCounts +=
                    dataTx.quantity),
                devicesWithOpticalWarnings: this.state.devicesWithOpticalWarnings.concat(
                    dataTx.ids
                ),
                lastDateOptical: dataTx.lastTime.replace("T", " ").replace("Z", ""),
                lastValueOptical: dataTx.lastValue,
            });
        }
        if (info.data.result.datacpu) {
            let datacpu = JSON.parse(info.data.result.datacpu);
            this.setState({
                cpuWarnsCounts: (this.state.cpuWarnsCounts += datacpu.quantity),
                devicesWithCpuWarnings: this.state.devicesWithCpuWarnings.concat(
                    datacpu.ids
                ),
                lastDateCpu: datacpu.lastTime.replace("T", " ").replace("Z", ""),
                lastValueCpu: datacpu.lastValue,
            });
        }
        if (info.data.result.datadelay) {
            let datadelay = JSON.parse(info.data.result.datadelay);
            this.setState({
                delayWarnsCounts: (this.state.delayWarnsCounts += datadelay.quantity),
                devicesWithDelayWarnings: this.state.devicesWithDelayWarnings.concat(
                    datadelay.ids
                ),
                lastDateDelay: datadelay.lastTime.replace("T", " ").replace("Z", ""),
                lastValueDelay: datadelay.lastValue,
            });
        }
        if (info.data.result.datajitter) {
            let datajitter = JSON.parse(info.data.result.datajitter);
            this.setState({
                jitterWarnsCounts: (this.state.datajitter += datajitter.quantity),
                devicesWithJitterWarnings: this.state.devicesWithJitterWarnings.concat(
                    datajitter.ids
                ),
                lastDateJitter: datajitter.lastTime.replace("T", " ").replace("Z", ""),
                lastValueJitter: datajitter.lastValue,
            });
        }
        if (info.data.result.databitErrorRate) {
            let databitErrorRate = JSON.parse(info.data.result.databitErrorRate);
            this.setState({
                bitErrorRateWarnsCounts: (this.state.bitErrorRateWarnsCounts +=
                    databitErrorRate.quantity),
                devicesWithBitErrorRateWarnings: this.state.devicesWithBitErrorRateWarnings.concat(
                    databitErrorRate.ids
                ),
                lastDateBitErrorRate: databitErrorRate.lastTime
                    .replace("T", " ")
                    .replace("Z", ""),
                lastValueBitErrorRate: databitErrorRate.lastValue,
            });
        }
        if (info.data.result.databitErrorRatio) {
            let databitErrorRatio = JSON.parse(info.data.result.databitErrorRatio);
            this.setState({
                bitErrorRatioWarnsCounts: (this.state.bitErrorRatioWarnsCounts +=
                    databitErrorRatio.quantity),
                devicesWithBitErrorRatioWarnings: this.state.devicesWithBitErrorRatioWarnings.concat(
                    databitErrorRatio.ids
                ),
                lastDateBitErrorRatio: databitErrorRatio.lastTime
                    .replace("T", " ")
                    .replace("Z", ""),
                lastValueBitErrorRatio: databitErrorRatio.lastValue,
            });
        }
        this.setState({
            devicesWithOutBandWarnings: this.state.ids.filter(
                (x) => !this.state.devicesWithBandWarnings.includes(x)
            ),
            devicesWithOutTempWarnings: this.state.ids.filter(
                (x) => !this.state.devicesWithTempWarnings.includes(x)
            ),
            devicesWithOutOpticalWarnings: this.state.ids.filter(
                (x) => !this.state.devicesWithOpticalWarnings.includes(x)
            ),
            devicesWithOutCpuWarnings: this.state.ids.filter(
                (x) => !this.state.devicesWithCpuWarnings.includes(x)
            ),
            devicesWithOutDelayWarnings: this.state.ids.filter(
                (x) => !this.state.devicesWithDelayWarnings.includes(x)
            ),
            devicesWithOutJitterWarnings: this.state.ids.filter(
                (x) => !this.state.devicesWithJitterWarnings.includes(x)
            ),
            devicesWithOutBitErrorRateWarnings: this.state.ids.filter(
                (x) => !this.state.devicesWithBitErrorRateWarnings.includes(x)
            ),
            devicesWithOutBitErrorRatioWarnings: this.state.ids.filter(
                (x) => !this.state.devicesWithBitErrorRatioWarnings.includes(x)
            ),
        });
    }

    async getTimeouts() {
        try {
            const apiUrl = "http://3.129.97.184:3001/findTimeoutDevices";
            const info = await axios.post(apiUrl);
            const devicesWithTimeouts = info.data.result.data.map(
                (device) => device._id
            );
            this.setState({
                lastDateTimeout:
                    info.data.result.lastTime || this.state.lastDateTimeout,
                devicesWithTimeouts: devicesWithTimeouts,
                devicesWithOutTimeouts: this.state.ids.filter(
                    (x) => !devicesWithTimeouts.includes(x)
                ),
            });
        } catch (e) {
            console.error(e);
        }
    }

    async updateWarnings(msg) {
        if (!this.state.devicesWithWarnings.includes(msg.idDevice)) {
            let index = this.state.devicesWithOutWarnings.indexOf(msg.idDevice);
            this.setState({
                devicesWithWarnings: this.state.devicesWithWarnings.concat(
                    msg.idDevice
                ),
                devicesWithOutWarnings: this.state.devicesWithOutWarnings.splice(
                    index,
                    1
                ),
            });
            --this.refs.packetChart.chart.series[0].data[0].y;
            ++this.refs.packetChart.chart.series[0].data[1].y;
            this.refs.packetChart.chart.redraw();
        }
    }

    async updateOpticalWarnings(msg) {
        const time = new Date(msg.time);
        const timeZ = time.getTimezoneOffset() / 60;
        let dateFinal = new Date(time.getTime());
        dateFinal.setHours(dateFinal.getHours() - timeZ);
        let lastDateOptical = dateFinal.toISOString()
            .replace("T", " ")
            .replace("Z", "");
        let lastValueOptical = msg.value;
        let opticalWarnsCounts = ++this.state.opticalWarnsCounts

        this.setState({
            lastDateOptical,
            lastValueOptical,
            opticalWarnsCounts
        });
        if (!this.state.devicesWithOpticalWarnings.includes(msg.idDevice)) {
            let index = this.state.devicesWithOutOpticalWarnings.indexOf(
                msg.idDevice
            );
            this.setState({
                devicesWithOpticalWarnings: this.state.devicesWithOpticalWarnings.push(
                    msg.idDevice
                ),
                devicesWithOutOpticalWarnings: this.state.devicesWithOutOpticalWarnings.splice(
                    index,
                    1
                ),
            });
            --this.refs.opticalPowerEventsChart.chart.series[0].data[0].y;
            ++this.refs.opticalPowerEventsChart.chart.series[0].data[1].y;
            this.refs.opticalPowerEventsChart.chart.redraw();
        }
    }

    async updateBandwidthWarnings(msg) {
        const time = new Date(msg.time);
        const timeZ = time.getTimezoneOffset() / 60;
        let dateFinal = new Date(time.getTime());
        dateFinal.setHours(dateFinal.getHours() - timeZ);
        this.setState({
            lastDateBandwidth: dateFinal
                .toISOString()
                .replace("T", " ")
                .replace("Z", ""),
            lastValueBandwidth: msg.value,
            bandwidthWarnCounts: ++this.state.bandwidthWarnCounts,
        });
        if (!this.state.devicesWithBandWarnings.includes(msg.idDevice)) {
            let index = this.state.devicesWithOutBandWarnings.indexOf(msg.idDevice);
            this.setState({
                devicesWithBandWarnings: this.state.devicesWithBandWarnings.push(
                    msg.idDevice
                ),
                devicesWithOutBandWarnings: this.state.devicesWithOutBandWarnings.splice(
                    index,
                    1
                ),
            });
            --this.refs.bandwidthEventsChart.chart.series[0].data[0].y;
            ++this.refs.bandwidthEventsChart.chart.series[0].data[1].y;
            this.refs.bandwidthEventsChart.chart.redraw();
        }
    }

    async updateCpuWarnings(msg) {
        const time = new Date(msg.time);
        const timeZ = time.getTimezoneOffset() / 60;
        let dateFinal = new Date(time.getTime());
        dateFinal.setHours(dateFinal.getHours() - timeZ);
        this.setState({
            lastDateCpu: dateFinal.toISOString().replace("T", " ").replace("Z", ""),
            lastValueCpu: msg.value,
            cpuWarnsCounts: ++this.state.cpuWarnsCounts,
        });
        if (!this.state.devicesWithCpuWarnings.includes(msg.idDevice)) {
            let index = this.state.devicesWithOutCpuWarnings.indexOf(msg.idDevice);
            this.setState({
                devicesWithCpuWarnings: this.state.devicesWithCpuWarnings.push(
                    msg.idDevice
                ),
                devicesWithOutCpuWarnings: this.state.devicesWithOutCpuWarnings.splice(
                    index,
                    1
                ),
            });
            --this.refs.cpuEventsChart.chart.series[0].data[0].y;
            ++this.refs.cpuEventsChart.chart.series[0].data[1].y;
            this.refs.cpuEventsChart.chart.redraw();
        }
    }

    async updateTemperatureWarnings(msg) {
        const time = new Date(msg.time);
        const timeZ = time.getTimezoneOffset() / 60;
        let dateFinal = new Date(time.getTime());
        dateFinal.setHours(dateFinal.getHours() - timeZ);
        this.setState({
            lastDateTemperature: dateFinal
                .toISOString()
                .replace("T", " ")
                .replace("Z", ""),
            lastValueTemperature: msg.value,
            temperatureWarnCounts: ++this.state.temperatureWarnCounts,
        });
        if (!this.state.devicesWithTempWarnings.includes(msg.idDevice)) {
            let index = this.state.devicesWithOutTempWarnings.indexOf(msg.idDevice);
            this.setState({
                devicesWithTempWarnings: this.state.devicesWithTempWarnings.push(
                    msg.idDevice
                ),
                devicesWithOutTempWarnings: this.state.devicesWithOutTempWarnings.splice(
                    index,
                    1
                ),
            });
            --this.refs.temperatureEventsChart.chart.series[0].data[0].y;
            ++this.refs.temperatureEventsChart.chart.series[0].data[1].y;
            this.refs.temperatureEventsChart.chart.redraw();
        }
    }

    async updateDelayWarnings(msg) {
        const time = new Date(msg.time);
        const timeZ = time.getTimezoneOffset() / 60;
        let dateFinal = new Date(time.getTime());
        dateFinal.setHours(dateFinal.getHours() - timeZ);
        this.setState({
            lastDateDelay: dateFinal.toISOString().replace("T", " ").replace("Z", ""),
            lastValueDelay: msg.value,
            delayWarnsCounts: ++this.state.delayWarnsCounts,
        });
        if (!this.state.devicesWithDelayWarnings.includes(msg.idDevice)) {
            let index = this.state.devicesWithOutDelayWarnings.indexOf(msg.idDevice);
            this.setState({
                devicesWithDelayWarnings: this.state.devicesWithDelayWarnings.push(
                    msg.idDevice
                ),
                devicesWithOutDelayWarnings: this.state.devicesWithOutDelayWarnings.splice(
                    index,
                    1
                ),
            });
            --this.refs.delayEventsChart.chart.series[0].data[0].y;
            ++this.refs.delayEventsChart.chart.series[0].data[1].y;
            this.refs.delayEventsChart.chart.redraw();
        }
    }

    async updateJitterWarnings(msg) {
        const time = new Date(msg.time);
        const timeZ = time.getTimezoneOffset() / 60;
        let dateFinal = new Date(time.getTime());
        dateFinal.setHours(dateFinal.getHours() - timeZ);
        this.setState({
            lastDateJitter: dateFinal
                .toISOString()
                .replace("T", " ")
                .replace("Z", ""),
            lastValueJitter: msg.value,
            jitterWarnsCounts: ++this.state.jitterWarnsCounts,
        });
        if (!this.state.devicesWithJitterWarnings.includes(msg.idDevice)) {
            let index = this.state.devicesWithOutJitterWarnings.indexOf(msg.idDevice);
            this.setState({
                devicesWithJitterWarnings: this.state.devicesWithJitterWarnings.push(
                    msg.idDevice
                ),
                devicesWithOutJitterWarnings: this.state.devicesWithOutJitterWarnings.splice(
                    index,
                    1
                ),
            });
            --this.refs.jitterEventsChart.chart.series[0].data[0].y;
            ++this.refs.jitterEventsChart.chart.series[0].data[1].y;
            this.refs.jitterEventsChart.chart.redraw();
        }
    }

    async updateBitRatioWarnings(msg) {
        const time = new Date(msg.time);
        const timeZ = time.getTimezoneOffset() / 60;
        let dateFinal = new Date(time.getTime());
        dateFinal.setHours(dateFinal.getHours() - timeZ);
        this.setState({
            lastDateBitErrorRatio: dateFinal
                .toISOString()
                .replace("T", " ")
                .replace("Z", ""),
            lastValueBitErrorRatio: msg.value,
            bitErrorRatioWarnsCounts: ++this.state.bitErrorRatioWarnsCounts,
        });
        if (!this.state.devicesWithBitErrorRatioWarnings.includes(msg.idDevice)) {
            let index = this.state.devicesWithOutBitErrorRatioWarnings.indexOf(
                msg.idDevice
            );
            this.setState({
                devicesWithBitErrorRatioWarnings: this.state.devicesWithBitErrorRatioWarnings.push(
                    msg.idDevice
                ),
                devicesWithOutBitErrorRatioWarnings: this.state.devicesWithOutBitErrorRatioWarnings.splice(
                    index,
                    1
                ),
            });
            --this.refs.bitErrorRatioEventsChart.chart.series[0].data[0].y;
            ++this.refs.bitErrorRatioEventsChart.chart.series[0].data[1].y;
            this.refs.bitErrorRatioEventsChart.chart.redraw();
        }
    }

    async updateBitRateWarnings(msg) {
        const time = new Date(msg.time);
        const timeZ = time.getTimezoneOffset() / 60;
        let dateFinal = new Date(time.getTime());
        dateFinal.setHours(dateFinal.getHours() - timeZ);
        this.setState({
            lastDateBitErrorRate: dateFinal
                .toISOString()
                .replace("T", " ")
                .replace("Z", ""),
            lastValueBitErrorRate: msg.value,
            bitErrorRateWarnsCounts: ++this.state.bitErrorRateWarnsCounts,
        });
        if (!this.state.devicesWithBitErrorRateWarnings.includes(msg.idDevice)) {
            let index = this.state.devicesWithOutBitErrorRateWarnings.indexOf(
                msg.idDevice
            );
            this.setState({
                devicesWithBitErrorRateWarnings: this.state.devicesWithBitErrorRateWarnings.push(
                    msg.idDevice
                ),
                devicesWithOutBitErrorRateWarnings: this.state.devicesWithOutBitErrorRateWarnings.splice(
                    index,
                    1
                ),
            });
            --this.refs.bitErrorRateEventsChart.chart.series[0].data[0].y;
            ++this.refs.bitErrorRateEventsChart.chart.series[0].data[1].y;
            this.refs.bitErrorRateEventsChart.chart.redraw();
        }
    }

    async loadData() {
        this.clean();
        await this.getDevices();
        await this.getGeneralWarnings();
        await this.getWarningsTypes();
    }

    /**
     * FUNCTION EXECUTED EVERY TIME THE SOCKET RECEIVES DATA
     * TO VERIFY IF ONE OR MORE GRAPHS HAVE TO CHANGE ACCORDING TO THE NEW AVG
     */
    async update(msgrx) {
        let msg = msgrx.split("/");

        //EVAULUATES IF THE DEVICE HAS LOST PACKAGES AND DID NOT HAVE THEM IN THE PAST
        if (parseFloat(msg[9]) > 0) {
            let actualTime = new Date();
            let actualTimeZ = actualTime.getTimezoneOffset() / 60;
            var actualDateFinal = new Date(actualTime.getTime());
            actualDateFinal.setHours(actualDateFinal.getHours() - actualTimeZ);
            this.setState({
                lastDatePacket: actualDateFinal
                    .toISOString()
                    .replace("T", " ")
                    .replace("Z", ""),
            });
            if (!this.state.devicesWithLostPackets.includes(msg[17])) {
                let index = this.state.devicesWithOutLostPackets.indexOf(msg.idDevice);
                this.setState({
                    devicesWithLostPackets: this.state.devicesWithLostPackets.push(
                        msg[17]
                    ),
                    devicesWithOutLostPackets: this.state.devicesWithOutLostPackets.splice(
                        index,
                        1
                    ),
                });
                --this.refs.packetChart.chart.series[0].data[0].y;
                ++this.refs.packetChart.chart.series[0].data[1].y;
                this.refs.packetChart.chart.redraw();
            }
        }
        /**
         * CALCULATES THE NEW AVG FOR EACH VALUE ACORDING TO THE NEW DATA RECEIVED
         * AND PUTS IT IN ITS RESPECTIVE INTERVAL (PERCENTAGE)
         */
        if (
            !isNaN(parseFloat(msg[14])) &&
            !isNaN(parseFloat(msg[15])) &&
            !isNaN(parseFloat(msg[10])) &&
            !isNaN(parseFloat(msg[12])) &&
            !isNaN(parseFloat(msg[11])) &&
            !isNaN(parseFloat(msg[2])) &&
            !isNaN(parseFloat(msg[8])) &&
            parseFloat(msg[8]) !== Infinity &&
            parseFloat(msg[8]) !== -Infinity &&
            !isNaN(parseFloat(msg[13]))
        ) {
            if (this.state.ids.includes(msg[17])) {
                let mtbfFound = false;
                var mtbf, mtbfaux;
                let delay = this.state.delays.find((e) => e.name === msg[17]);
                let jitter = this.state.jitters.find((e) => e.name === msg[17]);
                let beRate = this.state.beRates.find((e) => e.name === msg[17]);
                let beRatio = this.state.beRatios.find((e) => e.name === msg[17]);
                // let confidence = this.state.confidences.find(e => e.name === msg[17])
                let cpu = this.state.cpus.find((e) => e.name === msg[17]);
                let bandWidth = this.state.bandWidths.find((e) => e.name === msg[17]);
                let availability = this.state.availabilities.find(
                    (e) => e.name === msg[17]
                );
                let temperature = this.state.temperatures.find(
                    (e) => e.name === msg[17]
                );
                let optical = this.state.opticals.find((e) => e.name === msg[17]);
                if (parseFloat(msg[16]) !== -100) {
                    mtbf = this.state.mtbfs.find((e) => e.name === msg[17]);
                    mtbfFound = true;
                }
                if (
                    delay &&
                    jitter &&
                    beRate &&
                    beRatio &&
                    cpu &&
                    bandWidth &&
                    availability &&
                    temperature &&
                    optical
                ) {
                    delay.sum += parseFloat(msg[14]);
                    jitter.sum += parseFloat(msg[15]);
                    beRate.sum += parseFloat(msg[10]);
                    beRatio.sum += parseFloat(msg[11]);
                    // confidence.sum += parseFloat(msg[12])
                    cpu.sum += parseFloat(msg[2]);
                    bandWidth.sum += parseFloat(msg[8]);
                    availability.sum += parseFloat(msg[13]);
                    temperature.sum += parseFloat(msg[3]);
                    optical.sum += parseFloat(msg[1]);
                    delay.dataAmount++;
                    delay.data = delay.sum / delay.dataAmount;
                    jitter.data = jitter.sum / delay.dataAmount;
                    beRate.data = beRate.sum / delay.dataAmount;
                    beRatio.data = beRatio.sum / delay.dataAmount;
                    // confidence.data = (confidence.sum / delay.dataAmount)
                    cpu.data = cpu.sum / delay.dataAmount;
                    bandWidth.data = bandWidth.sum / delay.dataAmount;
                    availability.data = availability.sum / delay.dataAmount;
                    temperature.data = temperature.sum / delay.dataAmount;
                    optical.data = optical.sum / delay.dataAmount;

                    let delayaux = delay.interval;
                    let jitteraux = jitter.interval;
                    let beRateaux = beRate.interval;
                    let beRatioaux = beRatio.interval;
                    // let confidenceaux = confidence.interval
                    let cpuaux = cpu.interval;
                    let bandWidthaux = bandWidth.interval;
                    let availabilityaux = availability.interval;
                    let temperatureaux = temperature.interval;
                    let opticalaux = optical.interval;
                    if (parseFloat(msg[16]) !== -100 && mtbfFound) {
                        mtbfaux = mtbf.interval;
                        if (mtbf.data !== -100) {
                            mtbf.sum += parseFloat(msg[16]);
                            mtbf.dataAmount++;
                            mtbf.data = mtbf.sum / mtbf.dataAmount;
                        } else {
                            mtbf.sum = parseFloat(msg[16]);
                            mtbf.dataAmount = 1;
                            mtbf.data = parseFloat(msg[16]);
                        }
                    }
                    for (let i = 0; i < 10; i++) {
                        if (
                            delay.data > this.state.RecordDelays.intervals[i].min &&
                            delay.data <= this.state.RecordDelays.intervals[i].max
                        ) {
                            // eval("delayIntervalValue" + (i + 1) + "++")
                            delay.interval = i;
                        }
                        if (
                            jitter.data > this.state.RecordJitters.intervals[i].min &&
                            jitter.data <= this.state.RecordJitters.intervals[i].max
                        ) {
                            // eval("jitterIntervalValue" + (i + 1) + "++")
                            jitter.interval = i;
                        }
                        if (
                            beRate.data > this.state.RecordBitErrorRates.intervals[i].min &&
                            beRate.data <= this.state.RecordBitErrorRates.intervals[i].max
                        ) {
                            // eval("beRateIntervalValue" + (i + 1) + "++")
                            beRate.interval = i;
                        }
                        if (
                            beRatio.data > this.state.RecordBitErrorRatios.intervals[i].min &&
                            beRatio.data <= this.state.RecordBitErrorRatios.intervals[i].max
                        ) {
                            // eval("beRatioIntervalValue" + (i + 1) + "++")
                            beRatio.interval = i;
                        }
                        // if (confidence.data > this.state.RecordonfidenceLevel.intervals[i].min && confidence.data <= lastRecord.confidenceLevel.intervals[i].max) {
                        //   // eval("confidenceIntervalValue" + (i + 1) + "++")
                        //   confidence.interval = i
                        // }
                        if (
                            cpu.data > this.state.RecordCpus.intervals[i].min &&
                            cpu.data <= this.state.RecordCpus.intervals[i].max
                        ) {
                            // eval("cpuIntervalValue" + (i + 1) + "++")
                            cpu.interval = i;
                        }
                        if (
                            bandWidth.data > this.state.RecordBandwidths.intervals[i].min &&
                            bandWidth.data <= this.state.RecordBandwidths.intervals[i].max
                        ) {
                            // eval("bandWidthIntervalValue" + (i + 1) + "++")
                            bandWidth.interval = i;
                        }
                        if (
                            availability.data >=
                            this.state.RecordAvailabilities.intervals[i].min &&
                            availability.data <
                            this.state.RecordAvailabilities.intervals[i].max
                        ) {
                            // eval("availabilityIntervalValue" + (i + 1) + "++")
                            availability.interval = i;
                        }
                        if (
                            temperature.data >
                            this.state.RecordTemperatures.intervals[i].min &&
                            temperature.data <= this.state.RecordTemperatures.intervals[i].max
                        ) {
                            // eval("availabilityIntervalValue" + (i + 1) + "++")
                            temperature.interval = i;
                        }
                        if (
                            optical.data > this.state.RecordOpticalPowers.intervals[i].min &&
                            optical.data <= this.state.RecordOpticalPowers.intervals[i].max
                        ) {
                            // eval("availabilityIntervalValue" + (i + 1) + "++")
                            optical.interval = i;
                        }
                        if (parseFloat(msg[16]) !== -100 && mtbfFound) {
                            if (
                                this.state.RecordMtbfs.intervals[0].min &&
                                this.state.RecordMtbfs.intervals[0].max &&
                                this.state.RecordMtbfs.intervals[1].min &&
                                this.state.RecordMtbfs.intervals[1].max &&
                                this.state.RecordMtbfs.intervals[2].min &&
                                this.state.RecordMtbfs.intervals[2].max &&
                                this.state.RecordMtbfs.intervals[3].min &&
                                this.state.RecordMtbfs.intervals[3].max &&
                                this.state.RecordMtbfs.intervals[4].min &&
                                this.state.RecordMtbfs.intervals[4].max &&
                                this.state.RecordMtbfs.intervals[5].min &&
                                this.state.RecordMtbfs.intervals[5].max &&
                                this.state.RecordMtbfs.intervals[6].min &&
                                this.state.RecordMtbfs.intervals[6].max &&
                                this.state.RecordMtbfs.intervals[7].min &&
                                this.state.RecordMtbfs.intervals[7].max &&
                                this.state.RecordMtbfs.intervals[8].min &&
                                this.state.RecordMtbfs.intervals[8].max &&
                                this.state.RecordMtbfs.intervals[9].min &&
                                this.state.RecordMtbfs.intervals[9].max
                            ) {
                                if (
                                    this.state.RecordMtbfs.intervals[i].min &&
                                    this.state.RecordMtbfs.intervals[i].max
                                )
                                    if (
                                        mtbf.data > this.state.RecordMtbfs.intervals[i].min &&
                                        mtbf.data <= this.state.RecordMtbfs.intervals[i].max
                                    ) {
                                        // eval("availabilityIntervalValue" + (i + 1) + "++")
                                        mtbf.interval = i;
                                    }
                            } else {
                                const graphicMtbf = this.refs.mtbfChart.chart;
                                for (let i = 0; i < 10; i++) {
                                    let segment = graphicMtbf.series[0].data[i].name.split("-");
                                    if (
                                        mtbf.data > parseFloat(segment[0]) &&
                                        mtbf.data <= parseFloat(segment[1])
                                    ) {
                                        mtbf.interval = i;
                                    }
                                }
                            }
                        }
                    }
                    if (
                        temperature.data > this.state.RecordTemperatures.intervals[9].max
                    ) {
                        temperature.interval = 10;
                    }
                    if (optical.data > this.state.RecordOpticalPowers.intervals[9].max) {
                        optical.interval = 10;
                    }
                    if (availability.data === 100) {
                        availability.interval = 10;
                    }
                    if (delayaux !== delay.interval) {
                        console.log(
                            this.state.lastRecord.delay.intervals[delayaux].values.length,
                            this.state.lastRecord.delay.intervals[delay.interval].values
                                .length
                        );
                        --this.state.lastRecord.delay.intervals[delayaux].values.length;
                        ++this.state.lastRecord.delay.intervals[delay.interval].values
                            .length;
                        --this.refs.delayChart.chart.series[0].data[delayaux].y;
                        ++this.refs.delayChart.chart.series[0].data[delay.interval].y;
                        this.refs.delayChart.chart.redraw();
                    }
                    if (jitteraux !== jitter.interval) {
                        --this.state.lastRecord.jitter.intervals[jitteraux].values.length;
                        ++this.state.lastRecord.jitter.intervals[jitter.interval].values
                            .length;
                        --this.refs.jitterChart.chart.series[0].data[jitteraux].y;
                        ++this.refs.jitterChart.chart.series[0].data[jitter.interval].y;
                        this.refs.jitterChart.chart.redraw();
                    }
                    if (beRateaux !== beRate.interval) {
                        --this.state.lastRecord.bitErrorRate.intervals[beRateaux].values
                            .length;
                        ++this.state.lastRecord.bitErrorRate.intervals[beRate.interval]
                            .values.length;
                        --this.refs.bitErrorRateChart.chart.series[0].data[beRateaux].y;
                        ++this.refs.bitErrorRateChart.chart.series[0].data[beRate.interval]
                            .y;
                        this.refs.bitErrorRateChart.chart.redraw();
                    }
                    if (beRatioaux !== beRatio.interval) {
                        --this.state.lastRecord.bitErrorRatio.intervals[beRatioaux].values
                            .length;
                        ++this.state.lastRecord.bitErrorRatio.intervals[beRatio.interval]
                            .values.length;
                        --this.refs.bitErrorRatioChart.chart.series[0].data[beRatioaux].y;
                        ++this.refs.bitErrorRatioChart.chart.series[0].data[
                            beRatio.interval
                        ].y;
                        this.refs.bitErrorRatioChart.chart.redraw();
                    }
                    if (cpuaux !== cpu.interval) {
                        --this.state.lastRecord.cpuUsage.intervals[cpuaux].values.length;
                        ++this.state.lastRecord.cpuUsage.intervals[cpu.interval].values
                            .length;
                        --this.refs.cpuChart.chart.series[0].data[cpuaux].y;
                        ++this.refs.cpuChart.chart.series[0].data[cpu.interval].y;
                        this.refs.cpuChart.chart.redraw();
                    }
                    if (bandWidthaux !== bandWidth.interval) {
                        --this.state.lastRecord.bandwidth.intervals[bandWidthaux].values
                            .length;
                        ++this.state.lastRecord.bandwidth.intervals[bandWidth.interval]
                            .values.length;
                        --this.refs.bandwidthChart.chart.series[0].data[bandWidthaux].y;
                        ++this.refs.bandwidthChart.chart.series[0].data[bandWidth.interval]
                            .y;
                        this.refs.bandwidthChart.chart.redraw();
                    }
                    if (availabilityaux !== availability.interval) {
                        --this.state.lastRecord.availability.intervals[availabilityaux]
                            .values.length;
                        ++this.state.lastRecord.availability.intervals[
                            availability.interval
                        ].values.length;
                        --this.refs.availabilityChart.chart.series[0].data[availabilityaux]
                            .y;
                        ++this.refs.availabilityChart.chart.series[0].data[
                            availability.interval
                        ].y;
                        this.refs.availabilityChart.chart.redraw();
                    }
                    if (temperatureaux !== temperature.interval) {
                        --this.state.lastRecord.temperature.intervals[temperatureaux].values
                            .length;
                        ++this.state.lastRecord.temperature.intervals[temperature.interval]
                            .values.length;
                        --this.refs.temperatureChart.chart.series[0].data[temperatureaux].y;
                        ++this.refs.temperatureChart.chart.series[0].data[
                            temperature.interval
                        ].y;
                        this.refs.temperatureChart.chart.redraw();
                    }
                    if (opticalaux !== optical.interval) {
                        --this.state.lastRecord.opticalPower.intervals[opticalaux].values
                            .length;
                        ++this.state.lastRecord.opticalPower.intervals[optical.interval]
                            .values.length;
                        --this.refs.opticalPowerChart.chart.series[0].data[opticalaux].y;
                        ++this.refs.opticalPowerChart.chart.series[0].data[optical.interval]
                            .y;
                        this.refs.opticalPowerChart.chart.redraw();
                    }
                    if (parseFloat(msg[16]) !== -100 && mtbfFound) {
                        if (mtbf.data !== -100 && mtbfaux) {
                            if (mtbfaux !== mtbf.interval) {
                                --this.state.lastRecord.mtbf.intervals[mtbfaux].length;
                                this.state.lastRecord.mtbf.intervals[mtbf.interval].length += 1;
                                --this.refs.mtbfChart.chart.series[0].data[mtbfaux].y;
                                ++this.refs.mtbfChart.chart.series[0].data[mtbf.interval].y;
                                this.refs.mtbfChart.chart.redraw();
                            }
                        }
                    }
                    this.updateValues();
                } else if (availability) {
                    availability.sum += parseFloat(msg[13]);
                    availability.dataAmount++;
                    availability.data = availability.sum / availability.dataAmount;
                    let availabilityaux = availability.interval;
                    for (let i = 0; i < 10; i++) {
                        if (
                            availability.data > i * 10 + 1 &&
                            availability.data <= (i + 1) * 10
                        ) {
                            // eval("availabilityIntervalValue" + (i + 1) + "++")
                            availability.interval = i;
                        }
                    }
                    if (availability.data === 100) {
                        availability.interval = 10;
                    }
                    if (availabilityaux !== availability.interval) {
                        --this.state.lastRecord.availability.intervals[availabilityaux]
                            .values.length;
                        ++this.state.lastRecord.availability.intervals[
                            availability.interval
                        ].values.length;
                        --this.refs.availabilityChart.chart.series[0].data[availabilityaux]
                            .y;
                        ++this.refs.availabilityChart.chart.series[0].data[
                            availability.interval
                        ].y;
                        this.refs.availabilityChart.chart.redraw();
                    }
                }
            }
        }
    }

    updateValues() {
        let delaysAvgs = this.state.delays.map((avg) => avg.data);
        let jittersAvgs = this.state.jitters.map((avg) => avg.data);
        let beRatesAvgs = this.state.beRates.map((avg) => avg.data);
        let beRatiosAvgs = this.state.beRatios.map((avg) => avg.data);
        let cpusAvgs = this.state.cpus.map((avg) => avg.data);
        let bandWidthsAvgs = this.state.bandWidths.map((avg) => avg.data);
        let availabilitiesAvgs = this.state.availabilities.map((avg) => avg.data);
        let opticalsAvgs = this.state.opticals.map((avg) => avg.data);
        let temperaturesAvgs = this.state.temperatures.map((avg) => avg.data);
        let mtbfsAvgs = this.state.mtbfs
            .filter((obj) => {
                return obj.data != -100;
            })
            .map((avg) => avg.data);
        this.setState({
            minAvailability: Math.min(...availabilitiesAvgs),
            maxAvailability: Math.max(...availabilitiesAvgs),
            avgAvailability:
                availabilitiesAvgs.reduce(
                    (previous, current) => (current += previous)
                ) / availabilitiesAvgs.length,
            stdAvailability: 0,
            minBandwidth: Math.min(...bandWidthsAvgs),
            maxBandwidth: Math.max(...bandWidthsAvgs),
            avgBandwidth:
                bandWidthsAvgs.reduce((previous, current) => (current += previous)) /
                bandWidthsAvgs.length,
            stdBandwidth: 0,
            minTemperature: Math.min(...temperaturesAvgs),
            maxTemperature: Math.max(...temperaturesAvgs),
            avgTemperature:
                temperaturesAvgs.reduce((previous, current) => (current += previous)) /
                temperaturesAvgs.length,
            stdTemperature: 0,
            minDelay: Math.min(...delaysAvgs),
            maxDelay: Math.max(...delaysAvgs),
            avgDelay:
                delaysAvgs.reduce((previous, current) => (current += previous)) /
                delaysAvgs.length,
            stdDelay: 0,
            minJitter: Math.min(...jittersAvgs),
            maxJitter: Math.max(...jittersAvgs),
            avgJitter:
                jittersAvgs.reduce((previous, current) => (current += previous)) /
                jittersAvgs.length,
            stdJitter: 0,
            minOptical: Math.min(...opticalsAvgs),
            maxOptical: Math.max(...opticalsAvgs),
            avgOptical:
                opticalsAvgs.reduce((previous, current) => (current += previous)) /
                opticalsAvgs.length,
            stdOptical: 0,
            minCpu: Math.min(...cpusAvgs),
            maxCpu: Math.max(...cpusAvgs),
            avgCpu:
                cpusAvgs.reduce((previous, current) => (current += previous)) /
                cpusAvgs.length,
            stdCpu: 0,
            minBitErrorRate: Math.min(...beRatesAvgs),
            maxBitErrorRate: Math.max(...beRatesAvgs),
            avgBitErrorRate:
                beRatesAvgs.reduce((previous, current) => (current += previous)) /
                beRatesAvgs.length,
            stdBitErrorRate: 0,
            minBitErrorRatio: Math.min(...beRatiosAvgs),
            maxBitErrorRatio: Math.max(...beRatiosAvgs),
            avgBitErrorRatio:
                beRatiosAvgs.reduce((previous, current) => (current += previous)) /
                beRatiosAvgs.length,
            stdBitErrorRatio: 0,
            dataQuantity: delaysAvgs.length,
        });

        if (mtbfsAvgs.length > 0) {
            this.setState({
                minMtbf: Math.min(...mtbfsAvgs),
                maxMtbf: Math.max(...mtbfsAvgs),
                avgMtbf:
                    mtbfsAvgs.reduce((previous, current) => (current += previous)) /
                    mtbfsAvgs.length,
                stdMtbf: 0,
            });
        }
    }

    async realTime() {
        (async () => {
            let channel = socket.subscribe("scriptDateStarted");
            for await (let time of channel) {
                let auxTime = new Date(time);
                let timeNow = new Date();
                var seconds = Math.floor((timeNow - auxTime) / 1000);
                var days = Math.floor(seconds / 86400);
                seconds -= days * 86400;
                var hours = Math.floor(seconds / 3600) % 24;
                seconds -= hours * 3600;
                var minutes = Math.floor(seconds / 60) % 60;
                seconds -= minutes * 60;
                seconds = seconds % 60;
                this.state.onlineTimer.start({
                    startValues: [0, seconds, minutes, hours, days],
                });

                let onlineTime = {}
                this.state.onlineTimer.addEventListener("secondsUpdated", (e) => {
                    onlineTime = {
                        Days: this.state.onlineTimer.getTimeValues().days,
                        Hours: this.state.onlineTimer.getTimeValues().hours,
                        Minutes: this.state.onlineTimer.getTimeValues().minutes,
                        Seconds: this.state.onlineTimer.getTimeValues().seconds,
                    },
                        this.setState({ onlineTime })
                },
                    { passive: true }
                );
                let onlineTimerisActive = true;
                this.setState({ onlineTimerisActive })
            }
        })();
        (async () => {
            let channel = socket.subscribe("toQueryCounter");
            for await (let queries of channel) {
                if (queries >= 1000000) {
                    queries = (queries / 100).toFixed(0) + "K";
                }
                this.setState({ queries, status: greenButton });
                // if (onlineTimerisActive) onlineTimer.start()

                // console.log(queries);
            }
        })();
        (async () => {
            let channel = socket.subscribe("timeoutCounter");
            for await (let data of channel) {
                if (data >= 1000000) {
                    data = (data / 100).toFixed(0) + "K";
                }
                this.setState({ timeouts: data.timeouts });
                if (data.timeouts > 1 && data.id !== "") {
                    if (!isVpnDown) this.setState({ status: yellowButton });
                    // this.updateTimeouts(data.id, data.mtbf)
                }
            }
        })();
        (async () => {
            let channel = socket.subscribe("packetsCounter");
            for await (let packets of channel) {
                if (packets >= 1000000) {
                    packets = (packets / 100).toFixed(0) + "K";
                }
                isVpnDown = false;
                this.setState({ packets });
            }
        })();
        (async () => {
            let channel = socket.subscribe("analiticaDay");
            for await (let data of channel) {
                this.loadData();
            }
        })();
        (async () => {
            let channel = socket.subscribe("vpnDown");
            for await (let data of channel) {
                isVpnDown = true;
                this.setState({
                    state: redButton,
                });
                // onlineTimer.pause()
            }
        })();
        (async () => {
            let channel = socket.subscribe("clientModal");
            for await (let msg of channel) {
                switch (msg.type) {
                    case "Rx":
                        this.updateOpticalWarnings(msg);
                        break;
                    case "Tx":
                        this.updateOpticalWarnings(msg);
                        break;
                    case "Fin":
                        this.updateBandwidthWarnings(msg);
                        break;
                    case "Fout":
                        this.updateBandwidthWarnings(msg);
                        break;
                    case "cpu":
                        this.updateCpuWarnings(msg);
                        break;
                    case "temperature":
                        this.updateTemperatureWarnings(msg);
                        break;
                    case "delay":
                        this.updateDelayWarnings(msg);
                        break;
                    case "jitter":
                        this.updateJitterWarnings(msg);
                        break;
                    case "bitErrorRatio":
                        this.updateBitRatioWarnings(msg);
                        break;
                    case "bitErrorRate":
                        this.updateBitRateWarnings(msg);
                        break;
                    default:
                        break;
                }
                this.updateWarnings(msg);
            }
        })();
        socket.transmit(
            "toLogin",
            "Blue Hubble ha establecido conexión con cliente \n"
        ); //Registro en el servidor de entrada de cliente
        for (let index = 0; index < this.state.ids.length; index += 2) {
            (async () => {
                let channel = socket.subscribe(this.state.ids[index].replace("in", ""));
                for await (let data of channel) {
                    // this.state.lastRecord.delay.intervals[3].values.length = 50;
                    // this.state.lastRecord.delay.intervals[9].values.length = 50;
                    await this.update(data.msgrx);
                }
            })();
        }
        // this.setState({ isLoading: false });
    }


    async componentDidMount() {

        try {
            socket = socketClusterClient.create({
                hostname: "3.129.97.184",
                secure: false,
                port: 3000,
            });
            //this.updateValues()
            let { timer } = this.state;
            timer.start({
                startValues: [
                    0,
                    startedTime.getSeconds(),
                    startedTime.getMinutes(),
                    startedTime.getHours(),
                    0,
                ],
            });
            timer.addEventListener("secondsUpdated", (e) => {
                this.setState({
                    timeValues: timer.getTimeValues().toString(),
                })
            },
                { passive: true }
            );
            await this.getDevices();
            await Promise.all([
                this.getRecords(),
                this.getTimeouts(),
                this.getWarningsTypes(),
                this.getGeneralWarnings(),
            ]);
            highchartsAccessibility(Highcharts);
            HC_exporting(Highcharts);
            HC_exportingOffline(Highcharts);
            HC_exportingData(Highcharts);
            HC_more(Highcharts);
            Boost(Highcharts);
            this.realTime();
            this.setState({ isLoading: false });
        } catch (e) {
            console.log(e);
        }
    }

    componentWillUnmount() {
        if (socket) {
            socket.disconnect()
            this.state.timer.stop()
            this.state.onlineTimer.stop()
        }
    }

    render() {
        if (this.state.isLoading) {
            return <div className="App">Loading...</div>;
        }

        return (
            <Aux>
                <InformationTable
                    onlineTime={this.state.onlineTime}
                    status={this.state.status}
                    timeValues={this.state.timeValues}
                    queries={this.state.queries}
                    timeouts={this.state.timeouts}
                    warninigs={this.state.warninigs}
                    packets={this.state.packets}
                //page="OCH"
                />

                <Row className="m-0">
                    {" "}
                    <Col xl={4} xs={12} className="p-1">
                        <CardGraphEvents
                            devicesWithOutWarnings={this.state.devicesWithOutWarnings}
                            devicesWithWarnings={this.state.devicesWithWarnings}

                        />
                    </Col>
                    <Col xl={4} xs={12} className="p-1">
                        <CardGraphAvalibility
                            OndatafindLastDayAnalitica={this.state.OndatafindLastDayAnalitica}
                            availabilities={this.state.availabilities}
                            availability={this.state.availability}
                        // lastRecord={this.state.lastRecord}
                        />
                    </Col>
                    <Col xl={4} xs={12} className="p-1">
                        <CardGraphEvent2

                            bandwidthWarnCounts={this.state.bandwidthWarnCounts}
                            temperatureWarnCounts={this.state.temperatureWarnCounts}
                            opticalWarnsCounts={this.state.opticalWarnsCounts}
                            cpuWarnsCounts={this.state.cpuWarnsCounts}
                            delayWarnsCounts={this.state.delayWarnsCounts}
                            jitterWarnsCounts={this.state.jitterWarnsCounts}
                            bitErrorRateWarnsCounts={this.state.bitErrorRateWarnsCounts}
                            bitErrorRatioWarnsCounts={this.state.bitErrorRatioWarnsCounts}
                        />

                    </Col>

                </Row>
                <Row className="m-0">
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl">
                        <CardGraphBandwithEvent
                            devicesWithOutBandWarnings={this.state.devicesWithOutBandWarnings}
                            devicesWithBandWarnings={this.state.devicesWithBandWarnings}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl">
                        <CardGraphOpticalpowerEvent
                            devicesWithOutOpticalWarnings={this.state.devicesWithOutOpticalWarnings}
                            devicesWithOpticalWarnings={this.state.devicesWithOpticalWarnings}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl">
                        <CardGraphErrorRatioEvent
                            devicesWithOutBitErrorRatioWarnings={this.state.devicesWithOutBitErrorRatioWarnings}
                            devicesWithBitErrorRatioWarnings={this.state.devicesWithBitErrorRatioWarnings}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl">
                        <CardgraphErrorRateEvent
                            devicesWithOutBitErrorRateWarnings={this.state.devicesWithOutBitErrorRateWarnings}
                            devicesWithBitErrorRateWarnings={this.state.devicesWithBitErrorRateWarnings}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl">
                        <CardgraphDelayEvent
                            devicesWithOutDelayWarnings={this.state.devicesWithOutDelayWarnings}
                            devicesWithDelayWarnings={this.state.devicesWithDelayWarnings}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl">
                        <CardGraphJitterEvents
                            devicesWithOutJitterWarnings={this.state.devicesWithOutJitterWarnings}
                            devicesWithJitterWarnings={this.state.devicesWithJitterWarnings}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl">
                        <CardGraphCpuEvent
                            devicesWithOutCpuWarnings={this.state.devicesWithOutCpuWarnings}
                            devicesWithCpuWarnings={this.state.devicesWithCpuWarnings}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl">
                        <CardGraphTemperatureEvent
                            devicesWithOutTempWarnings={this.state.devicesWithOutTempWarnings}
                            devicesWithTempWarnings={this.state.devicesWithTempWarnings}
                        />
                    </Col>

                </Row>
                <Row className="m-0">
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl ">
                        <CardGraphBandWith
                            tipo="largo"
                            lastDateBandwidth={this.state.lastDateBandwidth}
                            lastValueBandwidth={this.state.lastValueBandwidth}
                            minBandwidth={this.state.minBandwidth}
                            maxBandwidth={this.state.maxBandwidth}
                            avgBandwidth={this.state.avgBandwidth}
                            dataQuantity={this.state.dataQuantity}
                            stdBandwidth={this.state.stdBandwidth}
                            lastRecord={this.state.lastRecord}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl ">
                        <CardGraphOpticalPower
                            tipo="largo"
                            lastDateOptical={this.state.lastDateOptical}
                            lastValueOptical={this.state.lastValueOptical}
                            minOptical={this.state.minOptical}
                            maxOptical={this.state.maxOptical}
                            avgOptical={this.state.avgOptical}
                            dataQuantity={this.state.dataQuantity}
                            stdOptical={this.state.stdOptical}
                            opticalPower={this.state.opticalPower}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl ">
                        <CardGraphErrorRate
                            tipo="largo"
                            lastDateBitErrorRate={this.state.lastDateBitErrorRate}
                            lastValueBitErrorRate={this.state.lastValueBitErrorRate}
                            minBitErrorRate={this.state.minBitErrorRate}
                            maxBitErrorRate={this.state.maxBitErrorRate}
                            avgBitErrorRate={this.state.avgBitErrorRate}
                            dataQuantity={this.state.dataQuantity}
                            stdBitErrorRate={this.state.stdBitErrorRate}
                            bitErrorRate={this.state.bitErrorRate}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl ">
                        <CardGraphErrorRatio
                            tipo="largo"
                            lastDateBitErrorRatio={this.state.lastDateBitErrorRatio}
                            lastValueBitErrorRatio={this.state.lastValueBitErrorRatio}
                            minBitErrorRatio={this.state.minBitErrorRatio}
                            maxBitErrorRatio={this.state.maxBitErrorRatio}
                            avgBitErrorRatio={this.state.avgBitErrorRatio}
                            dataQuantity={this.state.dataQuantity}
                            stdBitErrorRatio={this.state.stdBitErrorRatio}
                            bitErrorRatio={this.state.bitErrorRatio}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl ">
                        <CardGraphDelay
                            tipo="largo"
                            lastDateDelay={this.state.lastDateDelay}
                            lastValueDelay={this.state.lastValueDelay}
                            minDelay={this.state.minDelay}
                            maxDelay={this.state.maxDelay}
                            avgDelay={this.state.avgDelay}
                            dataQuantity={this.state.dataQuantity}
                            stdDelay={this.state.stdDelay}
                            delay={this.state.delay}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl ">
                        <CardGraphJitter
                            tipo="largo"
                            lastDateJitter={this.state.lastDateJitter}
                            lastValueJitter={this.state.lastValueJitter}
                            minJitter={this.state.minJitter}
                            maxJitter={this.state.maxJitter}
                            avgJitter={this.state.avgJitter}
                            dataQuantity={this.state.dataQuantity}
                            stdJitter={this.state.stdJitter}
                            jitter={this.state.jitter}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl ">
                        <CardGraphCpu
                            tipo="largo"
                            lastDateCpu={this.state.lastDateCpu}
                            lastValueCpu={this.state.lastValueCpu}
                            minCpu={this.state.minCpu}
                            maxCpu={this.state.maxCpu}
                            avgCpu={this.state.avgCpu}
                            dataQuantity={this.state.dataQuantity}
                            stdCpu={this.state.stdCpu}
                            lastRecord={this.state.lastRecord}
                        />
                    </Col>
                    <Col md={3} xl={3} xs={6} className="p-1 col-xxl ">
                        <CardGraphTemperature
                            tipo="largo"
                            lastDateTemperature={this.state.lastDateTemperature}
                            lastValueTemperature={this.state.lastValueTemperature}
                            minTemperature={this.state.minTemperature}
                            maxTemperature={this.state.maxTemperature}
                            avgTemperature={this.state.avgTemperature}
                            dataQuantity={this.state.dataQuantity}
                            stdTemperature={this.state.stdTemperature}
                            lastRecord={this.state.lastRecord}
                        />
                    </Col>
                </Row>
                <Row className="m-0">
                    <Col md={4} xl={4} xs={12} className="p-1">
                        <CardGraphFailures
                            tipo="medio"
                            minMtbf={this.state.minMtbf}
                            maxMtbf={this.state.maxMtbf}
                            avgMtbf={this.state.avgMtbf}
                            dataQuantity={this.state.dataQuantity}
                            stdMtbf={this.state.stdMtbf}
                            lastRecord={this.state.lastRecord}
                        />
                    </Col>
                    <Col md={4} xl={4} xs={12} className="p-1">
                        <CardGraphLostPacket
                            devicesWithOutLostPackets={this.state.devicesWithOutLostPackets}
                            devicesWithLostPackets={this.state.devicesWithLostPackets}
                            lastDatePacket={this.state.lastDatePacket}
                        />
                    </Col>
                    <Col md={4} xl={4} xs={12} className="p-1">
                        <CardGraphTimeouts
                            devicesWithOutTimeouts={this.state.devicesWithOutTimeouts}
                            devicesWithTimeouts={this.state.devicesWithTimeouts}
                            lastDateTimeout={this.state.lastDateTimeout}
                        />
                    </Col>
                </Row>

            </Aux>
        );
    }

}
