import React from "react";
import {
  Row,
  Col,
  Card,
  Table,
  Tabs,
  Tab,
  Form,
  Button,
} from "react-bootstrap";
import axios from "axios";
import Aux from "../hoc/_Aux";
import Line from "../Charts/line";
import Line2 from "../Charts/line2";
import Warning from "../Charts/warning";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import ReactHighcharts from 'react-highcharts'
import highchartsAccessibility from "highcharts/modules/accessibility";
import HC_exporting from "highcharts/modules/exporting";
import HC_exportingOffline from "highcharts/modules/offline-exporting";
import HC_exportingData from "highcharts/modules/export-data";
import HC_more from "highcharts/highcharts-more";
import Boost from "highcharts/modules/boost";
import Select from "react-select";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import DatePicker from "react-datepicker";
import RecordsGraphFooter from '../components/RecordsGraphFooter';
import { css } from "@emotion/core";
import FadeLoader from "react-spinners/FadeLoader";
import "react-datepicker/dist/react-datepicker.css";
import '../style/resposiveConectionYrecord.css';
// const HighchartsExporting = require('highcharts-exporting')
HC_exporting(Highcharts)
require('highcharts-export-csv')(Highcharts)

const initialHighcharts = Highcharts.Chart.prototype.getDataRows;
var variance = require("compute-variance");

const MySwal = withReactContent(Swal);
class Records extends React.Component {
  state = {
    date: new Date(),
    type: 'day',
    time: null,
    ip: true,
    markers: [],

    isLoading: true,
    spinLoading: false,
    octectsIn: [],
    octectsOut: [],
    ifinoctects: [],
    ifoutoctects: [],
    opticalRx: [],
    opticalTx: [],
    bitErrorRatio: [],
    bitErrorRate: [],
    availabilityIn: [],
    availabilityOut: [],
    jitterGraphsIn: [],
    jitterGraphsOut: [],
    warning: [],

    formInW: [],
    formOutW: [],
    ifInW: [],
    ifOutW: [],
    rxW: [],
    txW: [],
    tempW: [],
    cpuW: [],
    delayW: [],
    jitterW: [],
    bitRateW: [],
    bitRatioW: [],

    valIfinoctec: [],
    valIfoutoctec: [],
    valOctectsOut: [],
    valOpticalTx: [],
    valAvailabilityOut: [],
    valJitterGraphsOut: [],
    valOpticalRx: [],
    valOctectsIn: [],
    valBitErrorRate: [],
    valBitErrorRatio: [],
    valAvailabilityIn: [],
    valJitterGraphsIn: [],

    layers: [],
    layer: [],
    layerSelected: 'IP_INTERFACE',
    names: [],
    sourceNames: [],
    sinkNames: [],
    ocSources: [],
    ocSinks: [],
    selectedOcSource: null,
    selectedOcSink: null,
    selectedSourceName: null,
    selectedSinkName: null,
    selectedName: null,
    selectedFilter: null,
    foundLayer: null,
    reportOcSource: null,
    reportOcSink: null,
    reportId: null,

    timeouts: 0,
    packets: 0,
    errors: 0,

    dataisFetched: false,
  };
  graphic1 = React.createRef();
  graphic2 = React.createRef();
  graphic3 = React.createRef();
  graphic4 = React.createRef();
  graphic5 = React.createRef();
  graphic6 = React.createRef();
  graphic7 = React.createRef();
  graphic8 = React.createRef();

  async mapOutRecords(layers) {
    return layers.data.result.dataOut.map((data) => {
      var valores = data.deviceRecord.split("/");
      var fechas = data.time;
      var fecha = Date.parse(new Date(fechas));
      this.state.octectsOut.push([fecha, parseFloat(valores[0])]);
      this.state.opticalTx.push([fecha, parseFloat(valores[1]) / 100]);
      this.state.ifoutoctects.push([fecha, parseFloat(valores[8])]);
      this.state.availabilityOut.push([fecha, parseFloat(valores[13])]);
      this.state.jitterGraphsOut.push([fecha, parseFloat(valores[15])]);

      this.state.valIfoutoctec.push(Math.abs(parseFloat(valores[8])));
      this.state.valOctectsOut.push(Math.abs(parseFloat(valores[0])));
      this.state.valOpticalTx.push(Math.abs(parseFloat(valores[1])));
      this.state.valAvailabilityOut.push(Math.abs(parseFloat(valores[13])));
      this.state.valJitterGraphsOut.push(Math.abs(parseFloat(valores[15])));
      return data;
    });
  }

  async mapInRecords(layers) {
    return layers.data.result.datain.map((data) => {
      var valores = data.deviceRecord.split("/");
      var fechas = data.time;
      var fecha = Date.parse(new Date(fechas));
      this.state.octectsIn.push([fecha, parseFloat(valores[0])]);
      this.state.opticalRx.push([fecha, parseFloat(valores[1]) / 100]);
      this.state.ifinoctects.push([fecha, parseFloat(valores[8])]);
      this.state.bitErrorRate.push([fecha, parseFloat(valores[10])]);
      this.state.bitErrorRatio.push([fecha, parseFloat(valores[11])]);
      this.state.availabilityIn.push([fecha, parseFloat(valores[13])]);
      this.state.jitterGraphsIn.push([fecha, parseFloat(valores[15])]);

      this.state.valIfinoctec.push(Math.abs(parseFloat(valores[8])));
      this.state.valOpticalRx.push(Math.abs(parseFloat(valores[1])));
      this.state.valOctectsIn.push(Math.abs(parseFloat(valores[0])));
      this.state.valBitErrorRate.push(Math.abs(parseFloat(valores[10])));
      this.state.valBitErrorRatio.push(Math.abs(parseFloat(valores[11])));
      this.state.valAvailabilityIn.push(Math.abs(parseFloat(valores[13])));
      this.state.valJitterGraphsIn.push(Math.abs(parseFloat(valores[15])));

      return data;
    });
  }

  async getMinandMax() {
    this.setState({
      minIfinoct: Math.min(...this.state.valIfinoctec).toFixed(16),
    });
    this.setState({
      maxIfinoct: Math.max(...this.state.valIfinoctec).toFixed(16),
    });
    this.setState({
      minIfoutoct: Math.min(...this.state.valIfoutoctec).toFixed(16),
    });
    this.setState({
      maxIfoutoct: Math.max(...this.state.valIfoutoctec).toFixed(16),
    });
    this.setState({
      minInoct: Math.min(...this.state.valOctectsIn).toFixed(16),
    });
    this.setState({
      maxInoct: Math.max(...this.state.valOctectsIn).toFixed(16),
    });
    this.setState({
      minOutoct: Math.min(...this.state.valOctectsOut).toFixed(16),
    });
    this.setState({
      maxOutoct: Math.max(...this.state.valOctectsOut).toFixed(16),
    });
    this.setState({ minvalOpticalRx: Math.min(...this.state.valOpticalRx) });
    this.setState({ maxvalOpticalRx: Math.max(...this.state.valOpticalRx) });
    this.setState({ minvalOpticalTx: Math.min(...this.state.valOpticalTx) });
    this.setState({ maxvalOpticalTx: Math.max(...this.state.valOpticalTx) });
    this.setState({ minvalOctectsIn: Math.min(...this.state.valOctectsIn) });
    this.setState({ maxvalOctectsIn: Math.max(...this.state.valOctectsIn) });
    this.setState({
      minvalOctectsOut: Math.min(...this.state.valOctectsOut),
    });
    this.setState({
      maxvalOctectsOut: Math.max(...this.state.valOctectsOut),
    });
    this.setState({
      minvalBitErrorRate: Math.min(...this.state.valBitErrorRate),
    });
    this.setState({
      maxvalBitErrorRate: Math.max(...this.state.valBitErrorRate),
    });

    this.setState({
      minvalBitErrorRatio: Math.min(...this.state.valBitErrorRatio),
    });
    this.setState({
      maxvalBitErrorRatio: Math.max(...this.state.valBitErrorRatio),
    });
    this.setState({
      minvalAvailabilityOut: Math.min(...this.state.valAvailabilityOut),
    });
    this.setState({
      maxvalAvailabilityOut: Math.max(...this.state.valAvailabilityOut),
    });
    this.setState({
      minvalAvailabilityIn: Math.min(...this.state.valAvailabilityIn),
    });
    this.setState({
      maxvalAvailabilityIn: Math.max(...this.state.valAvailabilityIn),
    });
    this.setState({
      minvalJitterGraphsIn: Math.min(...this.state.valJitterGraphsIn),
    });
    this.setState({
      maxvalJitterGraphsIn: Math.max(...this.state.valJitterGraphsIn),
    });
    this.setState({
      minvalJitterGraphsOut: Math.min(...this.state.valJitterGraphsOut),
    });
    this.setState({
      maxvalJitterGraphsOut: Math.max(...this.state.valJitterGraphsOut),
    });
  }
  async getAvgs() {

    this.setState({
      AvgsIfinoctec:
        this.state.valIfinoctec.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valIfinoctec.length,
    });
    this.setState({
      AvgsIfoutoctec:
        this.state.valIfoutoctec.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valIfoutoctec.length,
    });
    this.setState({
      AvgsInoctec:
        this.state.valOctectsIn.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valIfinoctec.length,
    });
    this.setState({
      AvgsOutoctec:
        this.state.valOctectsOut.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valIfoutoctec.length,
    });
    this.setState({
      AvgsOpticalRx:
        this.state.valOpticalRx.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valOpticalRx.length,
    });
    this.setState({
      AvgsOpticalTx:
        this.state.valOpticalTx.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valOpticalTx.length,
    });
    this.setState({
      AvgsOctectsIn:
        this.state.valOctectsIn.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valOctectsIn.length,
    });
    this.setState({
      AvgsOctectsOut:
        this.state.valOctectsOut.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valOctectsOut.length,
    });
    this.setState({
      AvgsBitErrorRate:
        this.state.valBitErrorRate.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valBitErrorRate.length,
    });
    this.setState({
      AvgsBitErrorRatio:
        this.state.valBitErrorRatio.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valBitErrorRatio.length,
    });
    this.setState({
      AvgsAvailabilityOut:
        this.state.valAvailabilityOut.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valAvailabilityOut.length,
    });
    this.setState({
      AvgsAvailabilityIn:
        this.state.valAvailabilityIn.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valAvailabilityIn.length,
    });
    this.setState({
      AvgsJitterGraphsIn:
        this.state.valJitterGraphsIn.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valJitterGraphsIn.length,
    });
    this.setState({
      AvgsJitterGraphsOut:
        this.state.valJitterGraphsOut.reduce(
          (previous, current) => (current += previous)
        ) / this.state.valJitterGraphsOut.length,
    });
  }
  async getMedian() {
    this.setState({
      mediaIfinoctec:
        (this.state.valIfinoctec[(this.state.valIfinoctec.length - 1) >> 1] +
          this.state.valIfinoctec[this.state.valIfinoctec.length >> 1]) /
        2,
    });
    this.setState({
      mediaIfoutoctec:
        (this.state.valIfoutoctec[
          (this.state.valIfoutoctec.length - 1) >> 1
        ] +
          this.state.valIfoutoctec[this.state.valIfoutoctec.length >> 1]) /
        2,
    });
    this.setState({
      mediaInoctec:
        (this.state.valOctectsIn[(this.state.valOctectsIn.length - 1) >> 1] +
          this.state.valOctectsIn[this.state.valOctectsIn.length >> 1]) /
        2,
    });
    this.setState({
      mediaOutoctec:
        (this.state.valOctectsOut[
          (this.state.valOctectsOut.length - 1) >> 1
        ] +
          this.state.valOctectsOut[this.state.valOctectsOut.length >> 1]) /
        2,
    });
    this.setState({
      mediaOpticalRx:
        (this.state.valOpticalRx[(this.state.valOpticalRx.length - 1) >> 1] +
          this.state.valOpticalRx[this.state.valOpticalRx.length >> 1]) /
        2,
    });
    this.setState({
      mediaOpticalTx:
        (this.state.valOpticalTx[(this.state.valOpticalTx.length - 1) >> 1] +
          this.state.valOpticalTx[this.state.valOpticalTx.length >> 1]) /
        2,
    });
    this.setState({
      mediaOctectsIn:
        (this.state.valOctectsIn[(this.state.valOctectsIn.length - 1) >> 1] +
          this.state.valOctectsIn[this.state.valOctectsIn.length >> 1]) /
        2,
    });
    this.setState({
      mediaOctectsOut:
        (this.state.valOctectsOut[
          (this.state.valOctectsOut.length - 1) >> 1
        ] +
          this.state.valOctectsOut[this.state.valOctectsOut.length >> 1]) /
        2,
    });
    this.setState({
      mediaBitErrorRate:
        (this.state.valBitErrorRate[
          (this.state.valBitErrorRate.length - 1) >> 1
        ] +
          this.state.valBitErrorRate[
          this.state.valBitErrorRate.length >> 1
          ]) /
        2,
    });
    this.setState({
      mediaBitErrorRatio:
        (this.state.valBitErrorRatio[
          (this.state.valBitErrorRatio.length - 1) >> 1
        ] +
          this.state.valBitErrorRatio[
          this.state.valBitErrorRatio.length >> 1
          ]) /
        2,
    });
    this.setState({
      mediaAvailabilityOut:
        (this.state.valAvailabilityOut[
          (this.state.valAvailabilityOut.length - 1) >> 1
        ] +
          this.state.valAvailabilityOut[
          this.state.valAvailabilityOut.length >> 1
          ]) /
        2,
    });
    this.setState({
      mediaAvailabilityIn:
        (this.state.valAvailabilityIn[
          (this.state.valAvailabilityIn.length - 1) >> 1
        ] +
          this.state.valAvailabilityIn[
          this.state.valAvailabilityIn.length >> 1
          ]) /
        2,
    });
    this.setState({
      mediaJitterGraphsIn:
        (this.state.valJitterGraphsIn[
          (this.state.valJitterGraphsIn.length - 1) >> 1
        ] +
          this.state.valJitterGraphsIn[
          this.state.valJitterGraphsIn.length >> 1
          ]) /
        2,
    });
    this.setState({
      mediaJitterGraphsOut:
        (this.state.valJitterGraphsOut[
          (this.state.valJitterGraphsOut.length - 1) >> 1
        ] +
          this.state.valJitterGraphsOut[
          this.state.valJitterGraphsOut.length >> 1
          ]) /
        2,
    });
  }
  async getVariance() {
    this.setState({ varIfinoctec: variance(this.state.valIfinoctec) });
    this.setState({ varIfoutoctec: variance(this.state.valIfoutoctec) });
    this.setState({ varInoctec: variance(this.state.valOctectsIn) });
    this.setState({ varOutoctec: variance(this.state.valOctectsOut) });
    this.setState({ varOpticalRx: variance(this.state.valOpticalRx) });
    this.setState({ varOpticalTx: variance(this.state.valOpticalTx) });
    this.setState({ varOctectsIn: variance(this.state.valOctectsIn) });
    this.setState({ varOctectsOut: variance(this.state.valOctectsOut) });
    this.setState({ varBitErrorRate: variance(this.state.valBitErrorRate) });
    this.setState({
      varBitErrorRatio: variance(this.state.valBitErrorRatio),
    });
    this.setState({
      varAvailabilityOut: variance(this.state.valAvailabilityOut),
    });
    this.setState({
      varAvailabilityIn: variance(this.state.valAvailabilityIn),
    });
    this.setState({
      varJitterGraphsIn: variance(this.state.valJitterGraphsIn),
    });
    this.setState({
      varJitterGraphsOut: variance(this.state.valJitterGraphsOut),
    });
  }
  async getStd() {
    this.setState({
      devIfinoctec: Math.sqrt(variance(this.state.valIfinoctec)),
    });
    this.setState({
      devIfoutoctec: Math.sqrt(variance(this.state.valIfoutoctec)),
    });
    this.setState({
      devInoctec: Math.sqrt(variance(this.state.valOctectsIn)),
    });
    this.setState({
      devOutoctec: Math.sqrt(variance(this.state.valOctectsOut)),
    });
    this.setState({
      devOpticalRx: Math.sqrt(variance(this.state.valOpticalRx)),
    });
    this.setState({
      devOpticalTx: Math.sqrt(variance(this.state.valOpticalTx)),
    });
    this.setState({
      devOctectsIn: Math.sqrt(variance(this.state.valOctectsIn)),
    });
    this.setState({
      devOctectsOut: Math.sqrt(variance(this.state.valOctectsOut)),
    });
    this.setState({
      devBitErrorRate: Math.sqrt(variance(this.state.valBitErrorRate)),
    });
    this.setState({
      devBitErrorRatio: Math.sqrt(variance(this.state.valBitErrorRatio)),
    });
    this.setState({
      devAvailabilityOut: Math.sqrt(variance(this.state.valAvailabilityOut)),
    });
    this.setState({
      devAvailabilityIn: Math.sqrt(variance(this.state.valAvailabilityIn)),
    });
    this.setState({
      devJitterGraphsIn: Math.sqrt(variance(this.state.valJitterGraphsIn)),
    });
    this.setState({
      devJitterGraphsOut: Math.sqrt(variance(this.state.valJitterGraphsOut)),
    });
  }
  async getAmounts() {
    this.setState({ amtIfinoctec: this.state.valIfinoctec.length });
    this.setState({ amtIfoutoctec: this.state.valIfoutoctec.length });
    this.setState({ amtInoctec: this.state.valOctectsIn.length });
    this.setState({ amtOutoctec: this.state.valOctectsOut.length });
    this.setState({ amtOpticalRx: this.state.valOpticalRx.length });
    this.setState({ amtOpticalTx: this.state.valOpticalTx.length });
    this.setState({ amtOctectsIn: this.state.valOctectsIn.length });
    this.setState({ amtOctectsOut: this.state.valOctectsOut.length });
    this.setState({ amtBitErrorRate: this.state.valBitErrorRate.length });
    this.setState({ amtBitErrorRatio: this.state.valBitErrorRatio.length });
    this.setState({
      amtAvailabilityOut: this.state.valAvailabilityOut.length,
    });
    this.setState({ amtAvailabilityIn: this.state.valAvailabilityIn.length });
    this.setState({ amtJitterGraphsIn: this.state.valJitterGraphsIn.length });
    this.setState({
      amtJitterGraphsOut: this.state.valJitterGraphsOut.length,
    });
  }
  async findWarnings() {
    try {
      const apiUrl2 = "http://3.129.97.184:3003/findWarnRecordsById";
      const warning = await axios.post(apiUrl2, {
        id: this.state.idDevice,
        date: this.state.date,
        type: this.state.type,
      });
      const { result } = warning.data;
      const errors =
        result.dataBitErrorRate.length +
        result.dataBitErrorRatio.length +
        result.dataCpu.length +
        result.dataDelay.length +
        result.dataJitter.length +
        result.dataFin.length +
        result.dataFout.length +
        result.dataRx.length +
        result.dataTx.length +
        result.dataifInOctects.length +
        result.dataifOutOctects.length +
        result.dataTemperature.length;
      this.setState({
        errors,
        bitRatioW: warning.data.result.dataBitErrorRatio.map((data) => {
          return [Date.parse(new Date(data.time)), data.value];
        }),
        bitRateW: warning.data.result.dataBitErrorRate.map((data) => {
          return [Date.parse(new Date(data.time)), data.value];
        }),
        cpuW: warning.data.result.dataCpu.map((data) => {
          return [Date.parse(new Date(data.time)), data.value];
        }),
        delayW: warning.data.result.dataDelay.map((data) => {
          return [Date.parse(new Date(data.time)), data.value];
        }),
        ifInW: warning.data.result.dataFin.map((data) => {
          return [Date.parse(new Date(data.time)), data.value];
        }),
        ifOutW: warning.data.result.dataFout.map((data) => {
          return [Date.parse(new Date(data.time)), data.value];
        }),
        jitterW: warning.data.result.dataJitter.map((data) => {
          return [Date.parse(new Date(data.time)), data.value];
        }),
        rxW: warning.data.result.dataRx.map((data) => {
          return [Date.parse(new Date(data.time)), data.value];
        }),
        tempW: warning.data.result.dataTemperature.map((data) => {
          return [Date.parse(new Date(data.time)), data.value];
        }),
        txW: warning.data.result.dataTx.map((data) => {
          return [Date.parse(new Date(data.time)), data.value];
        }),
        formInW: warning.data.result.dataifInOctects.map((data) => {
          return [Date.parse(new Date(data.time)), data.value];
        }),
        formOutW: warning.data.result.dataifOutOctects.map((data) => {
          return [Date.parse(new Date(data.time)), data.value];
        }),
      });
    } catch (e) {

    }
  }

  async getLayers() {
    try {
      const apiUrlSelect = "http://3.129.97.184:3003/findLayers";
      const response = await axios.post(apiUrlSelect);
      const { result } = response.data;
      this.setState({
        layers: [...this.state.layers, result.data[1]],
        layerSelected: result.data[1]
      });


      // try {
      //   console.log(true)
      const apiUrlSelect2 = "http://3.129.97.184:3001/findLayers";
      const response2 = await axios.post(apiUrlSelect2);
      const result2 = response2.data.result;
      this.setState({
        layers: [...this.state.layers, result2.data[1]],
      });
      this.fillLayersSelect(result.data[1]);
      this.setState({ isLoading: false });
      // } catch (e) {
      //   console.log(false)
      //   console.log(e);
      //   this.setState({ isLoading: false });
      // }

    } catch (e) {
      console.log(e)
      // try {
      //   console.log(e);
      //   const apiUrlSelect = "http://3.129.97.184:3003/findLayers";
      //   const response = await axios.post(apiUrlSelect);
      //   const { result } = response.data;
      //   this.setState({
      //     layers: [...this.state.layers, ...result.data],
      //     layerSelected: result.data[1]
      //   });

      //   this.fillLayersSelect(this.state.layerSelected);
      //   this.setState({ isLoading: false });
      // } catch (e) {
      //   console.log(e);
      //   this.setState({ isLoading: false });
      // }
      this.setState({ isLoading: false });
      MySwal.fire({
        icon: 'error',
        title: 'Error',
        text: 'An error has occured',

      });
    }
  }

  findRecords = async () => {
    try {
      await this.getId();
      if (this.state.type && this.state.date && this.state.idDevice) {
        this.setState({ spinLoading: true });
        this.findPacketsAndTimeouts();
        const apiUrl = "http://3.129.97.184:3003/findRecordsByIdDate";
        const records = await axios.post(apiUrl, {
          idDevice: this.state.idDevice,
          date: this.state.date,
          type: this.state.type,
        });
        if (records.data.result.dataOut.length > 0 && records.data.result.datain.length > 0) {
          const deviceRecords = this.mapOutRecords(records);
          const deviceRecords2 = this.mapInRecords(records);

          // MIN Y MAX
          this.getMinandMax();
          // PROMEDIOS
          this.getAvgs();
          //MEDIANA
          this.getMedian();
          //VARIANZA
          this.getVariance();
          //DESVIACION ESTANDAR
          this.getStd();
          //Amount
          this.getAmounts();
          await this.findWarnings();
          this.setState({ isLoading: false, dataisFetched: true });
          this.setState({ spinLoading: false });
        } else {
          this.setState({ isLoading: false, dataisFetched: false });
          this.setState({ spinLoading: false });
          MySwal.fire({
            icon: 'warning',
            title: 'Error',
            text: 'No data found',
          });
        }
      } else {
        this.setState({ spinLoading: false });
        this.setState({ isLoading: false, dataisFetched: false });
        MySwal.fire({
          icon: 'warning',
          title: 'Error',
          text: 'Please specify the required information',
        });
      }
    } catch (e) {
      console.log(e)
      this.setState({ spinLoading: false });
      MySwal.fire({
        icon: 'error',
        title: 'Error',
        text: 'An error has occured',
      })
    }
  }

  findPacketsAndTimeouts = async () => {
    try {
      const response = await axios.post("http://3.129.97.184:3003/findPacketsAndTimeouts", {
        idDevice: this.state.idDevice,
        date: this.state.date,
        type: this.state.type,
      });
      const { result } = response.data;
      if (result.packets >= 0 && result.timeouts >= 0) {
        this.setState({
          packets: result.packets,
          timeouts: result.timeouts,
        })
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    try {
      await this.getLayers();
    } catch (e) {
      MySwal.fire({
        icon: 'error',
        title: 'Error',
        text: 'An error has occured',

      });
    }
  }

  fillLayersSelect = (layer) => {
    this.setState({
      initialOcSources: layer.features,
      initialOcSinks: layer.features,
      initialSources: layer.features,
      initialSinks: layer.features,
      initialNames: layer.features,
      layerNames: layer.features.map(function (
        obj
      ) {
        obj.label = obj.properties.Trail_name;
        obj.value = obj.value || obj.properties.ID;
        return obj;
      }),

      layerSourceNames: layer.features
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Source_Ne_Name,
            sink: e.properties.Sink_Ne_Name,
            source: e.properties.Source_Ne_Name,
          });
        })
        .filter((valorActual, indiceActual, arreglo) => {
          return (
            arreglo.findIndex(
              (valorDelArreglo) => valorDelArreglo.label === valorActual.label
            ) === indiceActual
          );
        }),

      layerSinkNames: layer.features
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Sink_Ne_Name,
            sink: e.properties.Sink_Ne_Name,
            source: e.properties.Source_Ne_Name,
          });
        })
        .filter((valorActual, indiceActual, arreglo) => {
          return (
            arreglo.findIndex(
              (valorDelArreglo) => valorDelArreglo.label === valorActual.label
            ) === indiceActual
          );
        }),

      layerOcSources: layer.features
        .map((e) => {
          return {
            value: e.properties.ID,
            label: e.properties.OC_Source,
            ocSink: e.properties.OC_Sink,
            ocSource: e.properties.OC_Source,
          };
        })
        .filter((valorActual, indiceActual, arreglo) => {
          return (
            arreglo.findIndex(
              (valorDelArreglo) => valorDelArreglo.label === valorActual.label
            ) === indiceActual
          );
        }),

      layerOcSinks: layer.features
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.OC_Sink,
            ocSource: e.properties.OC_Source,
            ocSink: e.properties.OC_Sink,
          });
        })
        .filter((valorActual, indiceActual, arreglo) => {
          return (
            arreglo.findIndex(
              (valorDelArreglo) => valorDelArreglo.label === valorActual.label
            ) === indiceActual
          );
        }),
    })
    this.setState({
      ocSinks: this.state.layerOcSinks,
      ocSources: this.state.layerOcSources,
      sourceNames: this.state.layerSourceNames,
      sinkNames: this.state.layerSinkNames,
      names: this.state.layerNames,
    })
  }

  handleTypeChange = (event) => {
    const { value } = event.target;
    const type = value.split(' ')[1];
    this.setState({ type });
    if (type === 'day') this.setState({ time: null });
  }

  handleLayerChange = async (name) => {
    const layer = this.state.layers.find((layer) => layer.name === name);
    await this.setState({ layerSelected: layer });
    this.fillLayersSelect(this.state.layerSelected);
  }

  handleOcSourceChange = async (selection) => {
    if (this.state.ocSourceIsSelected) { await this.cleanAllFilters() };
    const {
      ocSourceIsSelected,
      ocSinkIsSelected,
      sourceIsSelected,
      sinkIsSelected,
      nameIsSelected,
      layerSelected
    } = this.state;

    this.setState({ selectedOcSource: selection, ocSourceIsSelected: true });

    const selectionName = selection.label;

    if (
      !ocSinkIsSelected &&
      !sourceIsSelected &&
      !sinkIsSelected &&
      !nameIsSelected
    )
      this.setState({ selectedFilter: "ocSource" });


    if (!ocSinkIsSelected) {
      const ocSinks = this.state.initialOcSinks
        .filter((layer) => layer.properties.OC_Source.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.OC_Sink,
            ocSource: e.properties.OC_Source
          });
        })
        .filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialOcSinks: ocSinks,
        ocSinks: ocSinks,
      })
    }

    if (!sourceIsSelected) {
      const sourceNames = this.state.initialSources
        .filter((layer) => layer.properties.OC_Source.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Source_Ne_Name,
            ocSource: e.properties.OC_Source
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialSources: sourceNames,
        sourceNames,
      })
    }

    if (!sinkIsSelected) {
      const sinkNames = this.state.initialSinks
        .filter((layer) => layer.properties.OC_Source.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Sink_Ne_Name,
            ocSource: e.properties.OC_Source
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialSinks: sinkNames,
        sinkNames,
      })
    }

    if (!nameIsSelected) {
      const names = this.state.initialNames
        .filter((layer) => layer.properties.OC_Source.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Trail_name,
            ocSource: e.properties.OC_Source
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialNames: names,
        names,
      })
    }

  }
  handleOCSinkChange = async (selection) => {
    if (this.state.ocSinkIsSelected) { await this.cleanAllFilters() };
    const {
      ocSourceIsSelected,
      sourceIsSelected,
      sinkIsSelected,
      nameIsSelected,
      layerSelected,
    } = this.state;

    this.setState({ selectedOcSink: selection, ocSinkIsSelected: true });
    const selectionName = selection.label;

    if (
      !ocSourceIsSelected &&
      !sourceIsSelected &&
      !sinkIsSelected &&
      !nameIsSelected
    )
      this.setState({ selectedFilter: "ocSink" });

    if (!ocSourceIsSelected) {
      const ocSources = this.state.initialOcSources
        .filter((layer) => layer.properties.OC_Sink.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.OC_Source,
            ocSink: e.properties.OC_Sink
          });
        })
        .filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialOcSources: ocSources,
        ocSources,
      })
    }

    if (!sourceIsSelected) {
      const sourceNames = this.state.initialSources
        .filter((layer) => layer.properties.OC_Sink.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Source_Ne_Name,
            ocSink: e.properties.OC_Sink
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialSources: sourceNames,
        sourceNames,
      })
    }

    if (!sinkIsSelected) {
      const sinkNames = this.state.initialSinks
        .filter((layer) => layer.properties.OC_Sink.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Sink_Ne_Name,
            ocSink: e.properties.OC_Sink
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialSinks: sinkNames,
        sinkNames,
      })
    }

    if (!nameIsSelected) {
      const names = this.state.initialNames
        .filter((layer) => layer.properties.OC_Sink.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Trail_name,
            ocSink: e.properties.OC_Sink
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialNames: names,
        names,
      })
    }
  }
  handleSourceChange = async (selection) => {
    if (this.state.sourceIsSelected) { await this.cleanAllFilters() };
    const {
      ocSinkIsSelected,
      ocSourceIsSelected,
      sinkIsSelected,
      nameIsSelected,
    } = this.state;

    this.setState({ selectedSourceName: selection, sourceIsSelected: true });
    const selectionName = selection.label;

    if (
      !ocSinkIsSelected &&
      !ocSourceIsSelected &&
      !sinkIsSelected &&
      !nameIsSelected
    )
      this.setState({ selectedFilter: "sourceName" });

    if (!ocSourceIsSelected) {
      const ocSources = this.state.initialOcSources
        .filter((layer) => layer.properties.Source_Ne_Name.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.OC_Source,
            source: e.properties.Source_Ne_Name
          });
        })
        .filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialOcSources: ocSources,
        ocSources,
      })
    }

    if (!ocSinkIsSelected) {
      const ocSinks = this.state.initialOcSinks
        .filter((layer) => layer.properties.Source_Ne_Name.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.OC_Sink,
            source: e.properties.Source_Ne_Name
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialOcSinks: ocSinks,
        ocSinks,
      })
    }

    if (!sinkIsSelected) {
      const sinkNames = this.state.initialSinks
        .filter((layer) => layer.properties.Source_Ne_Name.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Sink_Ne_Name,
            source: e.properties.Source_Ne_Name
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialSinks: sinkNames,
        sinkNames,
      })
    }

    if (!nameIsSelected) {
      const names = this.state.initialNames
        .filter((layer) => layer.properties.Source_Ne_Name.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Trail_name,
            source: e.properties.Source_Ne_Name
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialNames: names,
        names,
      })
    }
  }
  handleSinkChange = async (selection) => {
    if (this.state.sinkIsSelected) { await this.cleanAllFilters() };
    const {
      ocSinkIsSelected,
      ocSourceIsSelected,
      sourceIsSelected,
      nameIsSelected,
      layerSelected,
    } = this.state;
    this.setState({ selectedSinkName: selection, sinkIsSelected: true });

    const selectionName = selection.label;

    if (
      !ocSinkIsSelected &&
      !ocSourceIsSelected &&
      !sourceIsSelected &&
      !nameIsSelected
    )
      this.setState({ selectedFilter: "sinkName" });


    if (!ocSourceIsSelected) {
      const ocSources = this.state.initialOcSources
        .filter((layer) => layer.properties.Sink_Ne_Name.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.OC_Source,
            sink: e.properties.Sink_Ne_Name
          });
        })
        .filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialOcSources: ocSources,
        ocSources,
      })
    }

    if (!ocSinkIsSelected) {
      const ocSinks = this.state.initialOcSinks
        .filter((layer) => layer.properties.Sink_Ne_Name.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.OC_Sink,
            sink: e.properties.Sink_Ne_Name
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialOcSinks: ocSinks,
        ocSinks,
      })
    }

    if (!sourceIsSelected) {
      const sourceNames = this.state.initialSources
        .filter((layer) => layer.properties.Sink_Ne_Name.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Source_Ne_Name,
            sink: e.properties.Sink_Ne_Name
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialSources: sourceNames,
        sourceNames,
      })
    }

    if (!nameIsSelected) {
      const names = this.state.initialNames
        .filter((layer) => layer.properties.Sink_Ne_Name.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Trail_name,
            sink: e.properties.Sink_Ne_Name
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialNames: names,
        names,
      })
    }
  }
  handleNameChange = async (selection) => {
    if (this.state.nameIsSelected) { await this.cleanAllFilters() };
    const {
      ocSinkIsSelected,
      ocSourceIsSelected,
      sourceIsSelected,
      sinkIsSelected,
      layerSelected,
    } = this.state;
    this.setState({ selectedName: selection, nameIsSelected: true });

    const selectionName = selection.label;

    if (
      !ocSinkIsSelected &&
      !ocSourceIsSelected &&
      !sourceIsSelected &&
      !sinkIsSelected
    )
      this.setState({ selectedFilter: "name" });

    if (!ocSourceIsSelected) {
      const ocSources = this.state.initialOcSources
        .filter((layer) => layer.properties.Trail_name.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.OC_Source,
            name: e.properties.Trail_name
          });
        })
        .filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialOcSources: ocSources,
        ocSources,
      })
    }

    if (!ocSinkIsSelected) {
      const ocSinks = this.state.initialOcSinks
        .filter((layer) => layer.properties.Trail_name.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.OC_Sink,
            name: e.properties.Trail_name
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialOcSinks: ocSinks,
        ocSinks,
      })
    }

    if (!sourceIsSelected) {
      const sourceNames = this.state.initialSources
        .filter((layer) => layer.properties.Trail_name.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Source_Ne_Name,
            name: e.properties.Trail_name
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialSources: sourceNames,
        sourceNames,
      })
    }

    if (!sinkIsSelected) {
      const sinkNames = this.state.initialSinks
        .filter((layer) => layer.properties.Trail_name.trim() === selectionName.trim())
        .map((e) => {
          return Object.assign({}, e, {
            value: e.properties.ID,
            label: e.properties.Sink_Ne_Name,
            name: e.properties.Trail_name
          });
        }).filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => valorDelArreglo.label === valorActual.label) === indiceActual
        })

      this.setState({
        initialSinks: sinkNames,
        sinkNames,
      })
    }
  }

  cleanAllFilters = async () => {
    this.setState({
      ocSinks: this.state.layerOcSinks,
      ocSources: this.state.layerOcSources,
      sourceNames: this.state.layerSourceNames,
      sinkNames: this.state.layerSinkNames,
      names: this.state.layerNames,
      selectedOcSource: null,
      selectedOcSink: null,
      selectedSourceName: null,
      selectedSinkName: null,
      selectedName: null,
      ocSourceIsSelected: false,
      ocSinkIsSelected: false,
      sourceIsSelected: false,
      sinkIsSelected: false,
      nameIsSelected: false,
      initialOcSources: this.state.layerSelected.features,
      initialOcSinks: this.state.layerSelected.features,
      initialSources: this.state.layerSelected.features,
      initialSinks: this.state.layerSelected.features,
      initialNames: this.state.layerSelected.features,
      selectedFilter: null,
      idDevice: null,
    })
  }

  cleanFilter = (filter) => {
    if (this.state.selectedFilter === filter) {
      this.cleanAllFilters();
    } else {
      const filterName = `${filter[0].toUpperCase()}${filter.substring(1)}`;
      const newState = {};
      newState[`selected${filterName}`] = null;
      console.log(newState)
      this.setState(newState);
    }
  }

  getId = () => {
    const {
      ocSourceIsSelected,
      ocSinkIsSelected,
      sourceIsSelected,
      sinkIsSelected,
      nameIsSelected,
      layerSelected,
      selectedOcSource,
      selectedOcSink,
      selectedSourceName,
      selectedSinkName,
      selectedName,
    } = this.state;

    let found;
    if (nameIsSelected) {
      found = layerSelected.features.find(e => e.properties.Trail_name === selectedName.label)

    } else {
      if (sourceIsSelected && sinkIsSelected) {
        found = layerSelected.features.find(e => e.properties.Source_Ne_Name === selectedSourceName.label && e.properties.Sink_Ne_Name === selectedSinkName.label)

      } else if (ocSourceIsSelected && ocSinkIsSelected) {
        found = layerSelected.features.find(e => e.properties.OC_Source === selectedOcSource.label && e.properties.OC_Sink === selectedOcSink.label)
      }
    }
    if (found) {
      this.setState({
        idDevice: found.properties.ID,
        reportId: found.properties.ID,
        reportOcSource: found.properties.OC_Source,
        reportOcSink: found.properties.OC_Sink,
        foundLayer: found
      })
    }
  }

  genReport = async () => {
    (function (H) {
      var pick = H.pick,
          defined = H.defined,
          each = H.each;
      H.Chart.prototype.getDataRows = function (multiLevelHeaders) {
          var time = this.time,
              csvOptions = (this.options.exporting && this.options.exporting.csv) || {},
              xAxis,
              xAxes = this.xAxis,
              rows = {},
              rowArr = [],
              dataRows,
              topLevelColumnTitles = [],
              columnTitles = [],
              columnTitleObj,
              xAxisIndices = [],
              i = 0,
              x,
              xTitle,
              // Options
              columnHeaderFormatter = function (item, key, keyLength) {
                  if (csvOptions.columnHeaderFormatter) {
                      var s = csvOptions.columnHeaderFormatter(item, key, keyLength);
                      if (s !== false) {
                          return s;
                      }
                  }

                  if (!item) {
                      return;
                  }

                  if (item instanceof Highcharts.Axis) {
                      return (item.options.title && item.options.title.text) ||
                          (item.isDatetimeAxis ? 'DateTime' : 'Category');
                  }

                  if (multiLevelHeaders) {
                      return {
                          columnTitle: keyLength > 1 ? key : item.name,
                          topLevelColumnTitle: item.name
                      };
                  }

                  return item.name + (keyLength > 1 ? ' (' + key + ')' : '');
              },
              xAxisIndices = [];

          // Loop the series and index values
          i = 0;

          this.setUpKeyToAxis();
          each(H.charts, function (chart) {
              each(chart.series, function (series) {
                  var keys = series.options.keys,
                      pointArrayMap = keys || series.pointArrayMap || ['y'],
                      valueCount = pointArrayMap.length,
                      xTaken = !series.requireSorting && {},
                      categoryMap = {},
                      datetimeValueAxisMap = {},
                      xAxisIndex = Highcharts.inArray(series.xAxis, xAxes),
                      mockSeries,
                      j;
                  // Map the categories for value axes
                  each(pointArrayMap, function (prop) {
                      var axisName = (
                          (series.keyToAxis && series.keyToAxis[prop]) ||
                          prop
                      ) + 'Axis';

                      categoryMap[prop] = (
                          series[axisName] &&
                          series[axisName].categories
                      ) || [];
                      datetimeValueAxisMap[prop] = (
                          series[axisName] &&
                          series[axisName].isDatetimeAxis
                      );
                  });

                  if (
                      series.options.includeInCSVExport !== false &&
                      !series.options.isInternal &&
                      series.visible !== false // #55
                  ) {

                      // Build a lookup for X axis index and the position of the first
                      // series that belongs to that X axis. Includes -1 for non-axis
                      // series types like pies.
                      if (!Highcharts.find(xAxisIndices, function (index) {
                          return index[0] === xAxisIndex;
                      })) {
                          xAxisIndices.push([xAxisIndex, i]);
                      }

                      // Compute the column headers and top level headers, usually the
                      // same as series names
                      j = 0;
                      while (j < valueCount) {
                          columnTitleObj = columnHeaderFormatter(
                              series,
                              pointArrayMap[j],
                              pointArrayMap.length
                          );
                          columnTitles.push(
                              columnTitleObj.columnTitle || columnTitleObj
                          );
                          if (multiLevelHeaders) {
                              topLevelColumnTitles.push(
                                  columnTitleObj.topLevelColumnTitle || columnTitleObj
                              );
                          }
                          j++;
                      }

                      mockSeries = {
                          chart: series.chart,
                          autoIncrement: series.autoIncrement,
                          options: series.options,
                          pointArrayMap: series.pointArrayMap
                      };

                      // Export directly from options.data because we need the uncropped
                      // data (#7913), and we need to support Boost (#7026).
                      each(series.options.data, function eachData(options, pIdx) {
                          var key,
                              prop,
                              val,
                              point;

                          point = {
                              series: mockSeries
                          };
                          series.pointClass.prototype.applyOptions.apply(
                              point, [options]
                          );
                          key = point.x;

                          if (xTaken) {
                              if (xTaken[key]) {
                                  key += '|' + pIdx;
                              }
                              xTaken[key] = true;
                          }

                          j = 0;

                          if (!rows[key]) {
                              // Generate the row
                              rows[key] = [];
                              // Contain the X values from one or more X axes
                              rows[key].xValues = [];
                          }
                          rows[key].x = point.x;
                          rows[key].xValues[xAxisIndex] = point.x;

                          // Pies, funnels, geo maps etc. use point name in X row
                          if (!series.xAxis || series.exportKey === 'name') {
                              rows[key].name = (
                                  series.data[pIdx] &&
                                  series.data[pIdx].name
                              );
                          }

                          while (j < valueCount) {
                              prop = pointArrayMap[j]; // y, z etc
                              val = point[prop];
                              rows[key][i + j] = pick(
                                  categoryMap[prop][val], // Y axis category if present
                                  datetimeValueAxisMap[prop] ?
                                      time.dateFormat(csvOptions.dateFormat, val) :
                                      null,
                                  val
                              );
                              j++;
                          }

                      });
                      i = i + j;
                  }
              });
          });

          // Make a sortable array
          for (x in rows) {
              if (rows.hasOwnProperty(x)) {
                  rowArr.push(rows[x]);
              }
          }

          var xAxisIndex, column;

          // Add computed column headers and top level headers to final row set
          dataRows = multiLevelHeaders ? [topLevelColumnTitles, columnTitles] : [columnTitles];

          i = xAxisIndices.length;
          while (i--) { // Start from end to splice in
              xAxisIndex = xAxisIndices[i][0];
              column = xAxisIndices[i][1]; // 24 48
              xAxis = xAxes[xAxisIndex];

              // Sort it by X values
              rowArr.sort(function (a, b) { // eslint-disable-line no-loop-func
                  return a.xValues[xAxisIndex] - b.xValues[xAxisIndex];
              });

              // Add header row
              xTitle = columnHeaderFormatter(xAxis);
              dataRows[0].splice(column, 0, xTitle);
              if (multiLevelHeaders && dataRows[1]) {
                  // If using multi level headers, we just added top level header.
                  // Also add for sub level
                  dataRows[1].splice(column, 0, xTitle);
              }

              // Add the category column
              // each(rowArr, function (row) { // eslint-disable-line no-loop-func
              //     var category = row.name;
              //     if (xAxis && !defined(category)) {
              //       // if (xAxis.isDatetimeAxis) {
              //         console.log(row.x)
              //             if (row.x instanceof Date) {
              //                 row.x = row.x.getTime();
              //             }
              //             category = time.dateFormat(
              //                 csvOptions.dateFormat,
              //                 row.x
              //             );
              //             // row.splice(column, 0, category);
              //         // }
              //         // else if (xAxis.categories) {
              //         //     category = pick(
              //         //         xAxis.names[row.x],
              //         //         xAxis.categories[row.x],
              //         //         row.x
              //         //     );

              //         // } else {
              //         //     category = row.x;
              //         // }
              //     }
              //     // Add the X/date/category
              // });
          }
          dataRows = dataRows.concat(rowArr);
          return dataRows;
      };
  })(Highcharts);
    let graphicTitle = this.graphic1.current.graphic.current.chart.options.title.text
    let graphicFilename = this.graphic1.current.graphic.current.chart.options.exporting.filename
    this.graphic1.current.graphic.current.chart.options.title.text = 'OC_Source: ' + this.state.reportOcSource.trim() + '. OC_Sink: ' + this.state.reportOcSink.trim() +
      '. Date: ' + this.state.date.toISOString().substring(0, 19).replace("T", " ")
    this.graphic1.current.graphic.current.chart.options.exporting.filename = this.state.reportId + ' - ' + this.state.date.toISOString().substring(0, 19).replace("T", " ")
    this.graphic1.current.graphic.current.chart.downloadXLS();
    this.graphic1.current.graphic.current.chart.options.title.text = graphicTitle
    this.graphic1.current.graphic.current.chart.options.exporting.filename = graphicFilename
    Highcharts.Chart.prototype.getDataRows = initialHighcharts
    Highcharts.Chart.prototype.getDataRows = initialHighcharts
  }

  render() {
    const { isLoading, spinLoading, date, layers,
      ocSources, ocSinks, sinkNames, sourceNames, names,
      selectedOcSource, selectedOcSink, selectedSourceName, selectedSinkName, selectedName } = this.state;

    const override = css`
      display: block;
      margin: 0 auto;
      opacity: 0.5;
      position:sticky;
      transform: translate(30%, 0%);
      top: 50%;
      left: 50%;
      z-index:1;
    `

    if (isLoading) {
      return <div className="App">Loading...</div>;
    }

    highchartsAccessibility(Highcharts);
    HC_exporting(Highcharts);
    HC_exportingOffline(Highcharts);
    HC_exportingData(Highcharts);
    HC_more(Highcharts);
    Boost(Highcharts);

    if (this.state.dataisFetched) {
      return (
        <Aux>
          <FadeLoader color="#1D3582" loading={spinLoading} css={override} size={150} />
          <div className="mb-3">
            <a style={{ fontSize: "30px" }}>History Records</a>
          </div>
          <Row>
            <Col md={12}>
              <Card>
                <Card.Body>
                  <Form.Group controlId="exampleForm.ControlSelect1">
                    <Row>
                      <Col md={4}>
                        <label>Select record type:</label>
                        <Form.Control
                          as="select"
                          onChange={(value) => this.handleTypeChange(value)}
                        >
                          <option>By day</option>
                          <option>By hour</option>
                        </Form.Control>
                      </Col>
                      <Col md={4}>
                        <Row>
                          <Col>
                            <div style={{ "margin-left": "25%" }}>
                              <label>Date: </label>
                              <br></br>
                              <DatePicker
                                className="form-control custom-select"
                                as="input"
                                type="date"
                                selected={this.state.date}
                                onChange={(e) => this.setState({ date: e })}
                              ></DatePicker>
                            </div>
                          </Col>
                        </Row>
                        <Row>
                          <Col>

                            {this.state.type === 'hour' && (
                              <>
                                <Row>
                                  <Col>
                                    <br></br>
                                    <label>Time:</label>
                                    <Form.Control
                                      as="select"
                                      id="LayerTypeSelect"
                                      onChange={(e) => this.setState({ time: e.target.value })}
                                    >
                                      <option>Select hour</option>
                                      <option>01:00</option>
                                      <option>02:00</option>
                                      <option>03:00</option>
                                      <option>04:00</option>
                                      <option>05:00</option>
                                      <option>06:00</option>
                                      <option>07:00</option>
                                      <option>08:00</option>
                                      <option>09:00</option>
                                      <option>10:00</option>
                                      <option>11:00</option>
                                      <option>12:00</option>
                                      <option>13:00</option>
                                      <option>14:00</option>
                                      <option>15:00</option>
                                      <option>16:00</option>
                                      <option>17:00</option>
                                      <option>18:00</option>
                                      <option>19:00</option>
                                      <option>20:00</option>
                                      <option>21:00</option>
                                      <option>22:00</option>
                                      <option>23:00</option>
                                      <option>24:00</option>
                                    </Form.Control>
                                  </Col>
                                </Row>
                              </>
                            )}
                          </Col>
                        </Row>
                      </Col>
                      <Col md={4}>

                        <label> Select layer name: </label>
                        <Form.Control as="select"
                          id="LayerTypeSelect"
                          onChange={(e) => this.handleLayerChange(e.target.value)}>
                          {layers.map((option) => (
                            <option key={option._id} value={option.name}>
                              {option.name}
                            </option>
                          ))}
                        </Form.Control>
                      </Col>
                    </Row>
                  </Form.Group>
                </Card.Body>
              </Card>
            </Col>
            <Col md={12}>
              <Card>
                <Card.Body>
                  <center className="mb-4">
                    <a style={{ fontSize: "20px" }}>{this.state.layerSelected.name}</a>
                  </center>

                  <Form.Group controlId="exampleForm.ControlSelect1">
                    <Row>
                      <Col md={10}>
                        <Row>
                          <Col md={2}>
                            <label className="ml-2">OC_Source: </label>
                          </Col>

                          <Col md={8}>
                            <Select
                              value={selectedOcSource}
                              options={ocSources}
                              onChange={this.handleOcSourceChange}
                            />
                          </Col>
                          <Col>
                            <Button className="align-middle clear" variant="secondary" onClick={(e) => this.cleanFilter('ocSource')}>

                              <span className="feather icon-x"></span>
                            </Button>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={2}>
                            <label className="ml-2">OC_Sink: </label>
                          </Col>

                          <Col md={8}>
                            <Select options={ocSinks}
                              onChange={this.handleOCSinkChange} value={selectedOcSink} />
                          </Col>
                          <Col>
                            <Button className="align-middle clear" variant="secondary" onClick={(e) => this.cleanFilter('ocSink')}>

                              <span className="feather icon-x"></span>
                            </Button>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={2}>
                            <label className="ml-2">Source_Ne_Name: </label>
                          </Col>
                          <Col md={8}>
                            <Select options={sourceNames} value={selectedSourceName} onChange={this.handleSourceChange} />
                          </Col>
                          <Col>
                            <Button className="align-middle   clear" variant="secondary" onClick={(e) => this.cleanFilter('sourceName')}>

                              <span className="feather icon-x"></span>
                            </Button>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={2}>
                            <label className="ml-2">Sink_Ne_Name: </label>
                          </Col>

                          <Col md={8}>
                            <Select options={sinkNames} value={selectedSinkName} onChange={this.handleSinkChange} />
                          </Col>
                          <Col>
                            <Button className="align-middle clear" variant="secondary" onClick={(e) => this.cleanFilter('sinkName')}>

                              <span className="feather icon-x"></span>
                            </Button>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={2}>
                            <label className="ml-2">Trail Name: </label>
                          </Col>

                          <Col md={8}>
                            <Select options={names} value={selectedName} onChange={this.handleNameChange} />
                          </Col>
                          <Col>
                            <Button className="align-middle clear" variant="secondary" onClick={(e) => this.cleanFilter('name')}>

                              <span className="feather icon-x"></span>
                            </Button>
                          </Col>
                        </Row>
                      </Col>

                    </Row>
                    <center className="botones abs-center">
                      <Button className="mt-3" variant="secondary" onClick={this.cleanAllFilters}>
                        Clear all
                      </Button>
                      <Button className="mt-3" variant="primary" onClick={this.findRecords}>
                        Search
                    </Button>
                    </center>
                  </Form.Group>
                </Card.Body>
              </Card>
            </Col>
            <Col md={6} xl={4}>
              <Card>
                <Card.Body>
                  <h5 className="mb-2 text-center">Timeouts</h5>
                  <div>
                    <h1 className="text-center">{this.state.timeouts}</h1>
                  </div>
                  <div className="progress m-t-30" style={{ height: "7px" }}>
                    <div
                      className="progress-bar progress-c-theme"
                      role="progressbar"
                      style={{ width: "50%" }}
                      aria-valuenow="50"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </Card.Body>
              </Card>
            </Col>
            <Col md={6} xl={4}>
              <Card>
                <Card.Body>
                  <h5 className="text-center mb-2">Packets</h5>
                  <div>
                    <h1 className="text-center">{this.state.packets}</h1>
                  </div>
                  <div className="progress m-t-30" style={{ height: "7px" }}>
                    <div
                      className="progress-bar progress-c-theme2"
                      role="progressbar"
                      style={{ width: "35%" }}
                      aria-valuenow="35"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </Card.Body>
              </Card>
            </Col>
            <Col xl={4}>
              <Card>
                <Card.Body>
                  <h5 className="text-center mb-2">Errors</h5>
                  <div>
                    <h1 className="text-center">{this.state.errors}</h1>
                  </div>
                  <div className="progress m-t-30" style={{ height: "7px" }}>
                    <div
                      className="progress-bar progress-c-theme"
                      role="progressbar"
                      style={{ width: "70%" }}
                      aria-valuenow="70"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </Card.Body>
              </Card>
            </Col>
            <Col md={12}>
              <center>
                <a style={{ fontSize: "20px" }}>
                  Historic records from {this.state.foundLayer.properties.OC_Source} to {this.state.foundLayer.properties.OC_Sink}
                </a>
                <br></br>
                <Button className="mt-3" variant="primary" onClick={this.genReport}>
                  Download Report
                </Button>
              </center>
              <br></br>
            </Col>
            <Col md={12}>
              <Form.Group controlId="exampleForm.ControlSelect1">
                <Card>
                  <Card.Body>
                    <Line2
                      ref={this.graphic1}
                      title="Bandwidth Utilization Graphs"
                      data={this.state.ifinoctects}
                      name="Speed for ifInOctects/(delta time)"
                      data2={this.state.ifoutoctects}
                      name2="Speed for ifOutOctects/(delta time)"
                      yvalue="value [Mbps]"
                    />
                  </Card.Body>
                  <Card.Footer>
                    <Row>
                      <Col md="6">
                        <RecordsGraphFooter
                          title='Speed for ifInOctects'
                          min={this.state.minIfinoct}
                          max={this.state.maxIfinoct}
                          avg={this.state.AvgsIfinoctec}
                          median={this.state.mediaIfinoctec}
                          variance={this.state.varIfinoctec}
                          std={this.state.devIfinoctec}
                          amount={this.state.amtIfinoctec}
                        />
                      </Col>
                      <Col md="6">
                        <RecordsGraphFooter
                          title='Speed for ifOutOctects'
                          min={this.state.minIfoutoct}
                          max={this.state.maxIfoutoct}
                          avg={this.state.AvgsIfoutoctec}
                          median={this.state.mediaIfoutoctec}
                          variance={this.state.varIfoutoctec}
                          std={this.state.devIfoutoctec}
                          amount={this.state.amtIfoutoctec}
                        />
                      </Col>
                    </Row>
                  </Card.Footer>
                </Card>
                <Card>
                  <Card.Body>
                    <Line
                      graph={this.graphic2}
                      title="ifInOctects Counter Graphs"
                      data={this.state.octectsIn}
                      name="ifInOctects"
                      yvalue="value [Pkts]"
                    />
                  </Card.Body>

                  <Card.Footer>
                    <RecordsGraphFooter
                      title='ifInOctects'
                      min={this.state.minInoct}
                      max={this.state.maxInoct}
                      avg={this.state.AvgsInoctec}
                      median={this.state.mediaInoctec}
                      variance={this.state.varInoctec}
                      std={this.state.devInoctec}
                      amount={this.state.amtInoctec}
                    />
                  </Card.Footer>
                </Card>
                <Card>
                  <Card.Body>
                    <Line
                      graph={this.graphic3}
                      title="ifOutOctects Counter Graphs"
                      data={this.state.octectsOut}
                      name="ifOutOctects"
                      yvalue="value [Pkts]"
                    />
                  </Card.Body>
                  <Card.Footer>
                    <RecordsGraphFooter
                      title='ifOutOctects'
                      min={this.state.minOutoct}
                      max={this.state.maxOutoct}
                      avg={this.state.AvgsOutoctec}
                      median={this.state.mediaOutoctec}
                      variance={this.state.varOutoctec}
                      std={this.state.devOutoctec}
                      amount={this.state.amtOutoctec}
                    />
                  </Card.Footer>
                </Card>
                <Card>
                  <Card.Body>
                    <Line2
                      graph={this.graphic4}
                      title="Optical power Graphs"
                      data={this.state.opticalRx}
                      name="Rx"
                      data2={this.state.opticalTx}
                      name2="Tx"
                      yvalue="value [dBm]"
                    />
                  </Card.Body>
                  <Card.Footer>
                    <Row>
                      <Col md="6">
                        <RecordsGraphFooter
                          title='Rx'
                          min={this.state.minvalOpticalRx}
                          max={this.state.maxvalOpticalRx}
                          avg={this.state.AvgsOpticalRx}
                          median={this.state.mediaOpticalRx}
                          variance={this.state.varOpticalRx}
                          std={this.state.devOpticalRx}
                          amount={this.state.amtOpticalRx}
                        />
                      </Col>
                      <Col md="6">
                        <RecordsGraphFooter
                          title='Tx'
                          min={this.state.minvalOpticalTx}
                          max={this.state.maxvalOpticalTx}
                          avg={this.state.AvgsOpticalTx}
                          median={this.state.mediaOpticalTx}
                          variance={this.state.varOpticalTx}
                          std={this.state.devOpticalTx}
                          amount={this.state.amtOpticalTx}
                        />
                      </Col>
                    </Row>
                  </Card.Footer>
                </Card>
                <Card>
                  <Card.Body>
                    <Line
                      graph={this.graphic5}
                      title="Bit Error Ratio Graph"
                      data={this.state.bitErrorRatio}
                      name="Bit Error Ratio"
                      yvalue="value"
                    />
                  </Card.Body>

                  <Card.Footer>
                    <RecordsGraphFooter
                      title='Bit Error Ratio'
                      min={this.state.minvalBitErrorRatio}
                      max={this.state.maxvalBitErrorRatio}
                      avg={this.state.AvgsBitErrorRatio}
                      median={this.state.mediaBitErrorRatio}
                      variance={this.state.varBitErrorRatio}
                      std={this.state.devBitErrorRatio}
                      amount={this.state.amtBitErrorRatio}
                    />
                  </Card.Footer>
                </Card>
                <Card>
                  <Card.Body>
                    <Line
                      graph={this.graphic6}
                      title="Bit Error Rate Graph"
                      data={this.state.bitErrorRate}
                      name="Bit Error Rate"
                      yvalue="valur"
                    />
                  </Card.Body>
                  <Card.Footer>
                    <RecordsGraphFooter
                      title='Bit Error Rate'
                      min={this.state.minvalBitErrorRate}
                      max={this.state.maxvalBitErrorRate}
                      avg={this.state.AvgsBitErrorRate}
                      median={this.state.mediaBitErrorRate}
                      variance={this.state.varBitErrorRate}
                      std={this.state.devBitErrorRate}
                      amount={this.state.amtBitErrorRate}
                    />
                  </Card.Footer>
                </Card>
                <Card>
                  <Card.Body>
                    <Line2
                      graph={this.graphic7}
                      title="Availability Graphs"
                      data={this.state.availabilityIn}
                      name="Availability for IN device"
                      data2={this.state.availabilityOut}
                      name2="Availability for OUT device"
                      yvalue="value [%]"
                    />
                  </Card.Body>
                  <Card.Footer>
                    <Row>
                      <Col md="6">
                        <RecordsGraphFooter
                          title='Availability for IN device'
                          min={this.state.minvalAvailabilityIn}
                          max={this.state.maxvalAvailabilityIn}
                          avg={this.state.AvgsAvailabilityIn}
                          median={this.state.mediaAvailabilityIn}
                          variance={this.state.varAvailabilityIn}
                          std={this.state.devAvailabilityIn}
                          amount={this.state.amtAvailabilityIn}
                        />
                      </Col>
                      <Col md="6">
                        <RecordsGraphFooter
                          title='Availability for OUT device'
                          min={this.state.minvalAvailabilityOut}
                          max={this.state.maxvalAvailabilityOut}
                          avg={this.state.AvgsAvailabilityOut}
                          median={this.state.mediaAvailabilityOut}
                          variance={this.state.varAvailabilityOut}
                          std={this.state.devAvailabilityOut}
                          amount={this.state.amtAvailabilityOut}
                        />
                      </Col>
                    </Row>
                  </Card.Footer>
                </Card>
                <Card>
                  <Card.Body>
                    <Line2
                      graph={this.graphic8}
                      title="Jitter Graphs"
                      data={this.state.jitterGraphsIn}
                      name="Jitter for IN device"
                      data2={this.state.jitterGraphsOut}
                      name2="Jitter for OUT device"
                      yvalue="value"
                    />
                  </Card.Body>
                  <Card.Footer>
                    <Row>
                      <Col md="6">
                        <RecordsGraphFooter
                          title='Jitter for IN device'
                          min={this.state.minvalJitterGraphsIn}
                          max={this.state.maxvalJitterGraphsIn}
                          avg={this.state.AvgsJitterGraphsIn}
                          median={this.state.mediaJitterGraphsIn}
                          variance={this.state.varJitterGraphsIn}
                          std={this.state.devJitterGraphsIn}
                          amount={this.state.amtJitterGraphsIn}
                        />
                      </Col>
                      <Col md="6">
                        <RecordsGraphFooter
                          title='Jitter for OUT device'
                          min={this.state.minvalJitterGraphsOut}
                          max={this.state.maxvalJitterGraphsOut}
                          avg={this.state.AvgsJitterGraphsOut}
                          median={this.state.mediaJitterGraphsOut}
                          variance={this.state.varJitterGraphsOut}
                          std={this.state.devJitterGraphsOut}
                          amount={this.state.amtJitterGraphsOut}
                        />
                      </Col>
                    </Row>
                  </Card.Footer>
                </Card>
                <Card>
                  <Card.Body>
                    <Warning
                      title="Warnings Graphs"
                      data={this.state.formInW}
                      data2={this.state.formOutW}
                      data3={this.state.ifInW}
                      data4={this.state.ifOutW}
                      data5={this.state.rxW}
                      data6={this.state.txW}
                      data7={this.state.tempW}
                      data8={this.state.cpuW}
                      data9={this.state.bitRateW}
                      data10={this.state.bitRatioW}
                      data11={this.state.jitterW}
                      data12={this.state.delayW}
                      yvalue="value"
                    />
                  </Card.Body>
                </Card>
              </Form.Group>
            </Col>
          </Row>
        </Aux >
      );
    }

    return (
      <Aux>
        <FadeLoader color="#1D3582" loading={spinLoading} css={override} size={150} />
        <div className="mb-3">
          <a style={{ fontSize: "30px" }}>History Records</a>
        </div>
        <Row>
          <Col md={12}>
            <Card>
              <Card.Body>
                <Form.Group controlId="exampleForm.ControlSelect1">
                  <Row>
                    <Col md={4}>
                      <label>Select record type:</label>
                      <Form.Control
                        as="select"
                        onChange={(value) => this.handleTypeChange(value)}
                      >
                        <option>By day</option>
                        <option>By hour</option>
                      </Form.Control>
                    </Col>
                    <Col md={4}>
                      <Row>
                        <Col>
                          <div style={{ "margin-left": "25%" }}>
                            <label>Date: </label>
                            <br></br>
                            <DatePicker
                              className="form-control custom-select"
                              as="input"
                              type="date"
                              selected={this.state.date}
                              onChange={(e) => this.setState({ date: e })}
                            ></DatePicker>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col>

                          {this.state.type === 'hour' && (
                            <>
                              <Row>
                                <Col>
                                  <br></br>
                                  <label>Time:</label>
                                  <Form.Control
                                    as="select"
                                    id="LayerTypeSelect"
                                    onChange={(e) => this.setState({ time: e.target.value })}
                                  >
                                    <option>Select hour</option>
                                    <option>01:00</option>
                                    <option>02:00</option>
                                    <option>03:00</option>
                                    <option>04:00</option>
                                    <option>05:00</option>
                                    <option>06:00</option>
                                    <option>07:00</option>
                                    <option>08:00</option>
                                    <option>09:00</option>
                                    <option>10:00</option>
                                    <option>11:00</option>
                                    <option>12:00</option>
                                    <option>13:00</option>
                                    <option>14:00</option>
                                    <option>15:00</option>
                                    <option>16:00</option>
                                    <option>17:00</option>
                                    <option>18:00</option>
                                    <option>19:00</option>
                                    <option>20:00</option>
                                    <option>21:00</option>
                                    <option>22:00</option>
                                    <option>23:00</option>
                                    <option>24:00</option>
                                  </Form.Control>
                                </Col>
                              </Row>
                            </>
                          )}
                        </Col>
                      </Row>
                    </Col>
                    <Col md={4}>
                      <label> Select layer name: </label>
                      <Form.Control as="select"
                        id="LayerTypeSelect"
                        onChange={(e) => this.handleLayerChange(e.target.value)}>
                        {layers.map((option) => (
                          <option key={option._id} value={option.name}>
                            {option.name}
                          </option>
                        ))}
                      </Form.Control>
                    </Col>
                  </Row>
                </Form.Group>
              </Card.Body>
            </Card>
          </Col>
          <Col md={12}>
            <Card>
              <Card.Body>
                <center className="mb-4">
                  <a style={{ fontSize: "20px" }}>{this.state.layerSelected.name}</a>
                </center>

                <Form.Group controlId="exampleForm.ControlSelect1">
                  <Row>
                    <Col md={10}>
                      <Row>
                        <Col md={2}>
                          <label className="ml-2">OC_Source: </label>
                        </Col>

                        <Col md={8}>
                          <Select
                            value={selectedOcSource}
                            options={ocSources}
                            onChange={this.handleOcSourceChange}
                          />
                        </Col>
                        <Col>
                          <Button className="align-middle clear" variant="secondary" onClick={(e) => this.cleanFilter('ocSource')}>

                            <span className="feather icon-x"></span>
                          </Button>
                        </Col>
                      </Row>
                      <Row>
                        <Col md={2}>
                          <label className="ml-2">OC_Sink: </label>
                        </Col>

                        <Col md={8}>
                          <Select options={ocSinks}
                            onChange={this.handleOCSinkChange} value={selectedOcSink} />
                        </Col>
                        <Col>
                          <Button className="align-middle clear" variant="secondary" onClick={(e) => this.cleanFilter('ocSink')}>

                            <span className="feather icon-x"></span>
                          </Button>
                        </Col>
                      </Row>
                      <Row>
                        <Col md={2}>
                          <label className="ml-2">Source_Ne_Name: </label>
                        </Col>
                        <Col md={8}>
                          <Select options={sourceNames} value={selectedSourceName} onChange={this.handleSourceChange} />
                        </Col>
                        <Col>
                          <Button className="align-middle clear" variant="secondary" onClick={(e) => this.cleanFilter('sourceName')}>

                            <span className="feather icon-x"></span>
                          </Button>
                        </Col>
                      </Row>
                      <Row>
                        <Col md={2}>
                          <label className="ml-2">Sink_Ne_Name: </label>
                        </Col>

                        <Col md={8}>
                          <Select options={sinkNames} value={selectedSinkName} onChange={this.handleSinkChange} />
                        </Col>
                        <Col>
                          <Button className="align-middle  clear" variant="secondary" onClick={(e) => this.cleanFilter('sinkName')}>

                            <span className="feather icon-x"></span>
                          </Button>
                        </Col>
                      </Row>
                      <Row>
                        <Col md={2}>
                          <label className="ml-2">Trail Name: </label>
                        </Col>

                        <Col md={8}>
                          <Select options={names} value={selectedName} onChange={this.handleNameChange} />
                        </Col>
                        <Col>
                          <Button className="align-middle clear" variant="secondary" onClick={(e) => this.cleanFilter('name')}>
                            <span className="feather icon-x"></span>
                          </Button>
                        </Col>
                      </Row>
                    </Col>

                  </Row>

                  <center className="botones abs-center">
                    <Button className="mt-3" variant="secondary" onClick={this.cleanAllFilters}>
                      Clear all
                      </Button>
                    <Button className="mt-3" variant="primary" onClick={this.findRecords}>
                      Search
                    </Button>

                  </center>


                </Form.Group>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Aux >
    );
  }
}

export default Records;
