import React from "react";

const SignUp = React.lazy(() =>
  import("./Views/SignUp")
);
const SignIn = React.lazy(() =>
  import("./Views/SignIn")
);
const Stream = React.lazy(() => import("./Views/stream"));
const Stream2 = React.lazy(() => import("./Views/stream2"));
const Stream3 = React.lazy(() => import("./Views/stream3"));

const route = [
  { path: "/auth/signup-1", exact: true, name: "Signup 1", component: SignUp },
  { path: "/auth/signin-1", exact: true, name: "Signin 1", component: SignIn },
  { path: "/stream", exact: true, name: "Stream", component: Stream },
  { path: "/stream2", exact: true, name: "Stream2", component: Stream2 },
  { path: "/stream3", exact: true, name: "Stream3", component: Stream3 },
];

export default route;
