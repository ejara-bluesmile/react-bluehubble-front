import React, { Component } from 'react';
import { Card } from "react-bootstrap";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";

import highchartsAccessibility from "highcharts/modules/accessibility";
import HC_exporting from "highcharts/modules/exporting";
import HC_exportingOffline from "highcharts/modules/offline-exporting";
import HC_exportingData from "highcharts/modules/export-data";
import HC_more from "highcharts/highcharts-more";
import Boost from "highcharts/modules/boost";

class CardGraphEvent2 extends Component {

  render() {
    return (
      <Card className="p-1 mb-1">
        <Card.Header>
          <Card.Title as="h5">Events</Card.Title>
        </Card.Header>
        {this.getGraphtEvent2()}
        <Card.Body className="p-1 m-0">

        </Card.Body>
      </Card>
    );
  }
  getGraphtEvent2() {
    if (this.props.tipo === "och") {
      return (
        <HighchartsReact
          hihcharts={Highcharts}
          options={{
            chart: { polar: true },

            title: {
              text: "",
            },

            xAxis: {
              categories: [
                "Optical Power",
                "Delay",
                "Jitter",
                "Bit Error Rate",
                "Bit Error Ratio",
              ],
              tickmarkPlacement: "on",
              lineWidth: 0,
            },

            yAxis: {
              gridLineInterpolation: "polygon",
              lineWidth: 0,
              min: 0,
            },

            series: [
              {
                name: "Last 24 hours",
                data: [
                  // this.props.bandwidthWarnCounts,
                  // this.props.temperatureWarnCounts,
                  this.props.opticalWarnsCounts,
                  // this.props.cpuWarnsCounts,
                  this.props.delayWarnsCounts,
                  this.props.jitterWarnsCounts,
                  this.props.bitErrorRateWarnsCounts,
                  this.props.bitErrorRatioWarnsCounts,
                ],
                pointPlacement: "on",
              },
            ],
          }}
          ref={"eventsChart"} />
      );
    }
    return (
      <HighchartsReact
        hihcharts={Highcharts}
        options={{
          chart: { polar: true },

          title: {
            text: "",
          },

          xAxis: {
            categories: [
              "Bandwidht",
              "Temperature",
              "Optical Power",
              "Cpu Usage",
              "Delay",
              "Jitter",
              "Bit Error Rate",
              "Bit Error Ratio",
            ],
            tickmarkPlacement: "on",
            lineWidth: 0,
          },

          yAxis: {
            gridLineInterpolation: "polygon",
            lineWidth: 0,
            min: 0,
          },

          series: [
            {
              name: "Last 24 hours",
              data: [
                this.props.bandwidthWarnCounts,
                this.props.temperatureWarnCounts,
                this.props.opticalWarnsCounts,
                this.props.cpuWarnsCounts,
                this.props.delayWarnsCounts,
                this.props.jitterWarnsCounts,
                this.props.bitErrorRateWarnsCounts,
                this.props.bitErrorRatioWarnsCounts,
              ],
              pointPlacement: "on",
            },
          ],
        }}
        ref={"eventsChart"}
      ></HighchartsReact>
    );

  }
  componentDidMount() {
    highchartsAccessibility(Highcharts);
    HC_exporting(Highcharts);
    HC_exportingOffline(Highcharts);
    HC_exportingData(Highcharts);
    HC_more(Highcharts);
    Boost(Highcharts);
  }
}

export default CardGraphEvent2;

