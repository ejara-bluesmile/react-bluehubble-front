import React, { Component, useEffect, useState } from 'react';
import { Card } from "react-bootstrap";

import EasyTimer from "easytimer";


const CurrenTime = () => {


    const [timeValues, setTimevalue] = useState("")


    useEffect(() => {
        const startedTime = new Date();
        let timer = new EasyTimer()
        timer.start({
            startValues: [
                0,
                startedTime.getSeconds(),
                startedTime.getMinutes(),
                startedTime.getHours(),
                0,
            ],
        });

        timer.addEventListener("secondsUpdated", (e) =>{
            setTimevalue(
                timer.getTimeValues().toString(),
            )}, { passive: true }
        );
    }, [])


    return (
        <Card className="p-1 mb-1" style={{ height: "100%" }}>
            <Card.Body className="p-1 m-0">
                <h5>Current Time:</h5>
                <h5> {timeValues}</h5>
            </Card.Body>
        </Card>);
}

export default CurrenTime
