import React, { Component } from 'react';
import { Row, Col, Card } from "react-bootstrap";
import MetrixCard from './metrixCard';

import redButton from "../../assets/images/redbutton.png";
import greenButton from "../../assets/images/greenbutton.png";
import yellowButton from "../../assets/images/yellowbutton.png";
import EasyTimer from "easytimer";
import socketClusterClient from "socketcluster-client";
import axios from "axios";

import highchartsAccessibility from "highcharts/modules/accessibility";
import HC_exporting from "highcharts/modules/exporting";
import HC_exportingOffline from "highcharts/modules/offline-exporting";
import HC_exportingData from "highcharts/modules/export-data";
import HC_more from "highcharts/highcharts-more";
import Boost from "highcharts/modules/boost";


import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts";
import CurrenTime from './currenTime';



const informationTable = React.memo(function (props) {
    return (
        <React.Fragment>
            <Row className="m-0">
                <Col md={1} xl={1} className="p-1">
                    <Card className="p-1 mb-1" style={{ height: "100%" }}>
                        <Card.Body className="p-1 m-0">
                            <h5 className="m-0 status">Status</h5>
                            <h5>
                                {" "}
                                <span>
                                    <img
                                        src={props.status}
                                        alt=""
                                        style={{ width: "20 px", "marginLeft": "10px" }}
                                    />
                                </span>
                            </h5>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={3} xs={12} className="p-1">
                    <Card className="p-1 mb-1" style={{ height: "100%" }}>
                        <Card.Body className="p-1 m-0">
                            <h5>Time Online:</h5>
                            <h5>
                                <span>{props.onlineTimeDays} Days </span>
                                <span>{props.onlineTimeHours} Hours </span>
                                <span>{props.onlineTimeMinutes} Minutes </span>
                                <span>{props.onlineTimeSeconds} Seconds</span>
                            </h5>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={2} className="p-1">
                    <CurrenTime />
                </Col>
                <Col
                    // xl={1} xs={6}
                    className="p-1">

                    <MetrixCard
                        name="Queries"
                        value={props.queries}
                    />
                </Col>
                <Col
                    // xl={1} xs={6}
                    className="p-1"
                >

                    <MetrixCard
                        name="Timeouts"
                        value={props.timeouts}
                    />
                </Col>
                <Col
                    // xl={1} xs={6}
                    className="p-1"
                >

                    <MetrixCard
                        name="Packets"
                        value={props.packets}
                    />
                </Col>
                <Col
                    // xl={1} xs={6}
                    className="p-1"
                >

                    <MetrixCard
                        name="Warnings"
                        value={props.warninigs}
                    />
                </Col>
            </Row>
        </React.Fragment>
    );
});




export default informationTable;