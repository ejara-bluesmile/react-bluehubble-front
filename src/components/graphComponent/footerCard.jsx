import React, { Component } from 'react';
import { Card } from "react-bootstrap";

class FooterCard extends React.PureComponent {


    render() {
        return (<Card.Footer>
            {this.getFailuresFooter()}
            <h6>
                Min Value: <span>{this.props.min}</span>
            </h6>
            <h6>
                Max Value: <span>{this.props.max}</span>
            </h6>
            <h6>
                Average Value: <span>{this.props.avg}</span>
            </h6>
            <h6>
                n=quantities of date: <span>{this.props.dataQuantity}</span>
            </h6>
            <h6>
                Standard Deviation: <span>{this.props.std}</span>
            </h6>
        </Card.Footer>);
    }

    getFailuresFooter() {
        let tipo = this.props.tipo


        if (tipo === "largo") {
            return (<React.Fragment>
                <h6>Time Last Event: <span>{this.props.lastDate}</span></h6>

                <h6>Last Value Event: <span>{this.props.lastValue}</span></h6>
            </React.Fragment>);



        }
        return ""


    }


}

export default FooterCard;