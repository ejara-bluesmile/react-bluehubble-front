import React, { Component } from 'react';
import { Card } from "react-bootstrap";

class MetrixCard extends React.PureComponent {
    classMetod(){
        if(this.props.clase){
            return "p-1 mb-1 "+this.props.clase
        }
        return  "p-1 mb-1" 
    }

    render() {
         
        return (

            <Card className={this.classMetod()} style={{ height: "100%" }}>
                <Card.Body className="p-1">
                    <h6 className="m-1">{this.props.name}</h6>
                    <h6 className="f-w-300 f-30 align-items-center m-b-0">
                        <i className="feather icon-arrow-up text-c-green f-30 m-r-5" />
                        {this.props.value}
                    </h6>
                </Card.Body>
            </Card>);
    }
}

export default MetrixCard;