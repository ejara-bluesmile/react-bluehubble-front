import React, { Component } from 'react';
import { Card } from "react-bootstrap";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";

import highchartsAccessibility from "highcharts/modules/accessibility";
import HC_exporting from "highcharts/modules/exporting";
import HC_exportingOffline from "highcharts/modules/offline-exporting";
import HC_exportingData from "highcharts/modules/export-data";
import HC_more from "highcharts/highcharts-more";
import Boost from "highcharts/modules/boost";
import swal from 'sweetalert2';

class CardgraphErrorRateEvent extends Component {

    openWarnSegment(array, dataName) {
        if (array.length > 0) {
            localStorage.setItem("chartWarningData", JSON.stringify({ name: dataName, data: array }))
            window.open(`../chartWarnings?layer=OCH`, "")
        } else {
            swal.fire('Warning', 'No links or data found for the selected segment.', 'warning',
            )
        }
    }

   handleClick2(e) {
        this.openWarnSegment(this.props.devicesWithOutBitErrorRateWarnings, "Bit Error Rate in range")





    }
   handleClick1(e) {

        this.openWarnSegment(this.props.devicesWithBitErrorRateWarnings, "Bit Error Rate out of range")

    }

    render() {
        return (
            <Card className="p-1 mb-1">
                <Card.Header>
                    <Card.Title as="h5">Bit Error Rate Events</Card.Title>
                </Card.Header>
                <Card.Body className="p-1 m-0">
                    <HighchartsReact
                        hihcharts={Highcharts}
                        options={{
                            chart: {
                                animation: false,
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: "pie",
                                style: {
                                    font:
                                        '18px Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
                                },
                            },
                            boost: {
                                useGPUTranslations: true,
                                usePreallocated: true,
                            },
                            title: {
                                text: "",
                            },
                            accessibility: {
                                point: {
                                    valueSuffix: "%",
                                },
                            },
                            series: [
                                {
                                    type: "pie",
                                    name: "Amount",
                                    innerSize: "50%",
                                    data: [
                                        {
                                            name: "Bit Error Rate in range",
                                            y: this.props.devicesWithOutBitErrorRateWarnings
                                                .length,
                                            events: {
                                                click: (e) => {
                                                    this.Hnadleclik2(e)
                                                }
                                            }
                                        },
                                        {
                                            name: "Bit Error Rate out of range",
                                            y: this.props.devicesWithBitErrorRateWarnings
                                                .length,
                                            events: {
                                                click: (e) => {
                                                    this.Hnadleclik1(e)
                                                }
                                            }
                                        },
                                    ],
                                },
                            ],
                            tooltip: {
                                hideDelay: 0,
                                pointFormat:
                                    "{series.name}<b>:{point.y:.1f}</b><br>Percentage:{point.percentage:1.f}%",
                            },
                            plotOptions: {
                                series: {
                                    options: {
                                        turboTreshold: 1,
                                    },
                                },
                                pie: {
                                    animation: false,
                                    allowPointSelect: true,
                                    cursor: "pointer",
                                    dataLabels: {
                                        enabled: false,
                                    },
                                    showInLegend: true,
                                    startAngle: -90,
                                    endAngle: 90,
                                    center: ["50%", "75%"],
                                },
                            },
                        }}
                        ref={"bitErrorRateEventsChart"}
                    ></HighchartsReact>
                </Card.Body>
            </Card>
        );
    }
    componentDidMount() {
        highchartsAccessibility(Highcharts);
        HC_exporting(Highcharts);
        HC_exportingOffline(Highcharts);
        HC_exportingData(Highcharts);
        HC_more(Highcharts);
        Boost(Highcharts);
    }
}

export default CardgraphErrorRateEvent;