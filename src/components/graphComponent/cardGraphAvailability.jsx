import React, { Component } from 'react';
import { Card } from "react-bootstrap";
import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts";

import highchartsAccessibility from "highcharts/modules/accessibility";
import HC_exporting from "highcharts/modules/exporting";
import HC_exportingOffline from "highcharts/modules/offline-exporting";
import HC_exportingData from "highcharts/modules/export-data";
import HC_more from "highcharts/highcharts-more";
import Boost from "highcharts/modules/boost";
import { Link } from 'react-router-dom';
import swal from 'sweetalert2';

class CardGraphAvalibility extends Component {

  componet

  render() {
    return (<Card className="p-1 mb-1">
      <Card.Header>
        <Card.Title as="h5">General Availability</Card.Title>
      </Card.Header>

      <Card.Body className="p-1 m-0">
        {this.graphtAvilibility()}
      </Card.Body>
    </Card>);
  }
  componentDidUpdate() {
    if (this.refs.availabilityChart) {
      this.refs.availabilityChart.chart.redraw();
    }

  }

  openSegment(array, dataName, index) {
   
    let data = array.filter(element => element.interval == index)
   
    if (data.length > 0) {
      localStorage.setItem("chartData", JSON.stringify({ name: dataName, data: array }))
      localStorage.setItem("chartInterval", index)
      window.open(`../chartData?layer=` + this.props.page, "")
    } else {
      swal.fire('Warning', 'No links or data found for the selected segment.', 'warning',
      )
    }
  }
 handleClick2(e) {
    let index = e.point.index
    this.openSegment(this.props.availabilities, "availability", index)



  }
 handleClick1(e) {
    let index = e.point.index
    this.openSegment(this.props.availabilities, "availability", index)


  }
  graphtAvilibility() {
    if (!this.props.OndatafindLastDayAnalitica) {
      return (
        <HighchartsReact
          updateArgs={[false, true, false]}
          highcharts={Highcharts}
          options={{
            chart: {
              animation: false,
              // plotBackgroundColor: null,
              // plotBorderWidth: null,
              // plotShadow: false,
              type: "pie",
              style: {
                font:
                  '18px Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
              },
            },
            boost: {
              useGPUTranslations: true,
              usePreallocated: true,
            },
            title: {
              text: "",
            },
            accessibility: {
              point: {
                valueSuffix: "%",
              },
            },
            series: [
              {
                name: "Amount",
                data: [
                  {
                    name: 1 + "-" + 10 + "%",
                    y: 0,
                  },
                  {
                    name: 11 + "-" + 20 + "%",
                    y: 0,
                  },
                  {
                    name: 21 + "-" + 30 + "%",
                    y: 0,
                  },
                  {
                    name: 31 + "-" + 40 + "%",
                    y: 0,
                  },
                  {
                    name: 41 + "-" + 50 + "%",
                    y: 0,
                  },
                  {
                    name: 51 + "-" + 60 + "%",
                    y: 0,
                  },
                  {
                    name: 61 + "-" + 70 + "%",
                    y: 0,
                  },
                  {
                    name: 71 + "-" + 80 + "%",
                    y: 0,
                  },
                  {
                    name: 81 + "-" + 90 + "%",
                    y: 0,
                  },
                  {
                    name: 91 + "-" + 99 + "%",
                    y: 0,
                  },
                  {
                    name: 100 + "%",
                    y: this.props.availabilities.length,
                  },
                ],
              },
            ],
            tooltip: {
              hideDelay: 0,
              pointFormat:
                "{series.name}<b>:{point.y:.1f}</b><br>Percentage:{point.percentage:1.f}%",
            },
            plotOptions: {
              series: {
                options: {
                  turboTreshold: 1,
                },
              },
              pie: {
                propss: {
                  hover: {
                    animation: {
                      duration: 0,
                    },
                  },
                },
                animation: false,
                allowPointSelect: false,
                cursor: "pointer",
                dataLabels: {
                  enabled: false,
                },
                showInLegend: true,
                events: {
                  click: (e) => {
                    this.Hnadleclik2(e)
                  }
                }


              },
            },
          }}
          ref={"availabilityChart"}
        />
      );
    } else {
      return (
        <HighchartsReact
          updateArgs={[false, true, false]}
          highcharts={Highcharts}
          options={{
            chart: {
              animation: false,
              // plotBackgroundColor: null,
              // plotBorderWidth: null,
              // plotShadow: false,
              type: "pie",
              style: {
                font:
                  '18px Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
              },
            },
            boost: {
              useGPUTranslations: true,
              usePreallocated: true,
            },
            title: {
              text: "",
            },
            accessibility: {
              point: {
                valueSuffix: "%",
              },
            },
            series: [
              {
                name: "Amount",
                data: [
                  {
                    name: 1 + "-" + 10 + "%",
                    y: this.props.availability.intervals[0]
                      .values.length,
                  },
                  {
                    name: 11 + "-" + 20 + "%",
                    y: this.props.availability.intervals[1]
                      .values.length,
                  },
                  {
                    name: 21 + "-" + 30 + "%",
                    y: this.props.availability.intervals[2]
                      .values.length,
                  },
                  {
                    name: 31 + "-" + 40 + "%",
                    y: this.props.availability.intervals[3]
                      .values.length,
                  },
                  {
                    name: 41 + "-" + 50 + "%",
                    y: this.props.availability.intervals[4]
                      .values.length,
                  },
                  {
                    name: 51 + "-" + 60 + "%",
                    y: this.props.availability.intervals[5]
                      .values.length,
                  },
                  {
                    name: 61 + "-" + 70 + "%",
                    y: this.props.availability.intervals[6]
                      .values.length,
                  },
                  {
                    name: 71 + "-" + 80 + "%",
                    y: this.props.availability.intervals[7]
                      .values.length,
                  },
                  {
                    name: 81 + "-" + 90 + "%",
                    y: this.props.availability.intervals[8]
                      .values.length,
                  },
                  {
                    name: 91 + "-" + 99 + "%",
                    y: this.props.availability.intervals[9]
                      .values.length,
                  },
                  {
                    name: 100 + "%",
                    y: this.props.availability.intervals[10]
                      .values.length,
                  },
                ],
              },
            ],
            tooltip: {
              hideDelay: 0,
              pointFormat:
                "{series.name}<b>:{point.y:.1f}</b><br>Percentage:{point.percentage:1.f}%",
            },
            plotOptions: {
              series: {
                options: {
                  turboTreshold: 1,
                },
              },
              pie: {
                propss: {
                  hover: {
                    animation: {
                      duration: 0,
                    },
                  },
                },
                animation: false,
                allowPointSelect: false,
                cursor: "pointer",
                dataLabels: {
                  enabled: false,
                },
                showInLegend: true,
                events: {
                  click: (e) => {

                    this.Hnadleclik1(e)
                  }
                }
              },
            },
          }}
          ref={"availabilityChart"}
        />
      );
    }

  }
  componentDidMount() {
    highchartsAccessibility(Highcharts);
    HC_exporting(Highcharts);
    HC_exportingOffline(Highcharts);
    HC_exportingData(Highcharts);
    HC_more(Highcharts);
    Boost(Highcharts);
  }
}


export default CardGraphAvalibility;