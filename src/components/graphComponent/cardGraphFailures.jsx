import React, { Component } from 'react';
import { Card } from "react-bootstrap";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import ErrorCardBoody from "./errorCardBody"
import FooterCard from './footerCard';

import highchartsAccessibility from "highcharts/modules/accessibility";
import HC_exporting from "highcharts/modules/exporting";
import HC_exportingOffline from "highcharts/modules/offline-exporting";
import HC_exportingData from "highcharts/modules/export-data";
import HC_more from "highcharts/highcharts-more";
import Boost from "highcharts/modules/boost";

class CardGraphFailures extends Component {

    render() {
        return (
            <Card className="p-1 mb-1">
                <Card.Header>
                    <Card.Title as="h5">Mean Time Between Failures</Card.Title>
                </Card.Header>
                <Card.Body className="p-1 m-0">

                    {this.graphFailures()}
                </Card.Body>

                <FooterCard
                    tipo={this.props.tipo}
                    min={this.props.minMtbf}
                    max={this.props.maxMtbf}
                    avg={this.props.avgMtbf}
                    dataQuantity={this.props.dataQuantity}
                    std={this.props.stdMtbf}
                />
            </Card>

        );
    }
    graphFailures() {
        if (!this.props.lastRecord.mtbf || this.props.lastRecord.mtbf.length == 0) {
            return (<ErrorCardBoody />)
        }
        return (
            <HighchartsReact
                updateArgs={[false, true, false]}
                highcharts={Highcharts}
                options={{
                    chart: {
                        animation: false,
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: "pie",
                        style: {
                            font:
                                '18px Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
                        },
                    },
                    boost: {
                        useGPUTranslations: true,
                        usePreallocated: true,
                    },
                    title: {
                        text: "",
                    },
                    accessibility: {
                        point: {
                            valueSuffix: "%",
                        },
                    },
                    series: [
                        {
                            name: "Amount",
                            data: [
                                {
                                    name:
                                        this.props.lastRecord.mtbf.intervals[0].min.toFixed(
                                            4
                                        ) +
                                        "-" +
                                        this.props.lastRecord.mtbf.intervals[0].max.toFixed(
                                            4
                                        ),
                                    y: this.props.lastRecord.mtbf.intervals[0].values
                                        .length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.mtbf.intervals[1].min.toFixed(
                                            4
                                        ) +
                                        "-" +
                                        this.props.lastRecord.mtbf.intervals[1].max.toFixed(
                                            4
                                        ),
                                    y: this.props.lastRecord.mtbf.intervals[1].values
                                        .length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.mtbf.intervals[2].min.toFixed(
                                            4
                                        ) +
                                        "-" +
                                        this.props.lastRecord.mtbf.intervals[2].max.toFixed(
                                            4
                                        ),
                                    y: this.props.lastRecord.mtbf.intervals[2].values
                                        .length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.mtbf.intervals[3].min.toFixed(
                                            4
                                        ) +
                                        "-" +
                                        this.props.lastRecord.mtbf.intervals[3].max.toFixed(
                                            4
                                        ),
                                    y: this.props.lastRecord.mtbf.intervals[3].values
                                        .length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.mtbf.intervals[4].min.toFixed(
                                            4
                                        ) +
                                        "-" +
                                        this.props.lastRecord.mtbf.intervals[4].max.toFixed(
                                            4
                                        ),
                                    y: this.props.lastRecord.mtbf.intervals[4].values
                                        .length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.mtbf.intervals[5].min.toFixed(
                                            4
                                        ) +
                                        "-" +
                                        this.props.lastRecord.mtbf.intervals[5].max.toFixed(
                                            4
                                        ),
                                    y: this.props.lastRecord.mtbf.intervals[5].values
                                        .length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.mtbf.intervals[6].min.toFixed(
                                            4
                                        ) +
                                        "-" +
                                        this.props.lastRecord.mtbf.intervals[6].max.toFixed(
                                            4
                                        ),
                                    y: this.props.lastRecord.mtbf.intervals[6].values
                                        .length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.mtbf.intervals[7].min.toFixed(
                                            4
                                        ) +
                                        "-" +
                                        this.props.lastRecord.mtbf.intervals[7].max.toFixed(
                                            4
                                        ),
                                    y: this.props.lastRecord.mtbf.intervals[7].values
                                        .length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.mtbf.intervals[8].min.toFixed(
                                            4
                                        ) +
                                        "-" +
                                        this.props.lastRecord.mtbf.intervals[8].max.toFixed(
                                            4
                                        ),
                                    y: this.props.lastRecord.mtbf.intervals[8].values
                                        .length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.mtbf.intervals[9].min.toFixed(
                                            4
                                        ) +
                                        "-" +
                                        this.props.lastRecord.mtbf.intervals[9].max.toFixed(
                                            4
                                        ),
                                    y: this.props.lastRecord.mtbf.intervals[9].values
                                        .length,
                                },
                                {
                                    name: "Without stops",
                                    y: this.props.lastRecord.mtbf.intervals[10].values
                                        .length,
                                },
                            ],
                        },
                    ],
                    tooltip: {
                        hideDelay: 0,
                        pointFormat:
                            "{series.name}<b>:{point.y:.1f}</b><br>Percentage:{point.percentage:1.f}%",
                    },
                    plotOptions: {
                        series: {
                            options: {
                                turboTreshold: 1,
                            },
                        },
                        pie: {
                            animation: false,
                            allowPointSelect: true,
                            cursor: "pointer",
                            dataLabels: {
                                enabled: false,
                            },
                            showInLegend: true,
                        },
                    },
                }}
                ref={"mtbfChart"}
            />
        );
    }
    componentDidMount() {
        highchartsAccessibility(Highcharts);
        HC_exporting(Highcharts);
        HC_exportingOffline(Highcharts);
        HC_exportingData(Highcharts);
        HC_more(Highcharts);
        Boost(Highcharts);
    }
}

export default CardGraphFailures;