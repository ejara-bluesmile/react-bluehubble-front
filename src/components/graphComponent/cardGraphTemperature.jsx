import React, { Component } from 'react';
import { Card } from "react-bootstrap";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import ErrorCardBoody from "./errorCardBody"

import highchartsAccessibility from "highcharts/modules/accessibility";
import HC_exporting from "highcharts/modules/exporting";
import HC_exportingOffline from "highcharts/modules/offline-exporting";
import HC_exportingData from "highcharts/modules/export-data";
import HC_more from "highcharts/highcharts-more";
import Boost from "highcharts/modules/boost";
import FooterCard from './footerCard';

class CardGraphTemperature extends Component {

    render() {
        return (
            <Card className="p-1 mb-1">
                <Card.Header>
                    <Card.Title as="h5">Temperature</Card.Title>
                </Card.Header>
                <Card.Body className="p-1 m-0">
                    {this.graphTemperatura()}
                </Card.Body>

                <FooterCard
                    tipo={this.props.tipo}
                    lastDate={this.props.lastDateTemperature}
                    lastValue={this.props.lastValueTemperature}
                    min={this.props.minTemperature}
                    max={this.props.maxTemperature}
                    avg={this.props.avgTemperature}
                    dataQuantity={this.props.dataQuantity}
                    std={this.props.stdTemperature}
                />

            </Card>
        );
    }
    graphTemperatura() {
        if (!this.props.lastRecord.temperature || this.props.lastRecord.temperature.length == 0) {
            return (<ErrorCardBoody />)
        }

        return (
            <HighchartsReact
                updateArgs={[false, true, false]}
                highcharts={Highcharts}
                options={{
                    chart: {
                        animation: false,
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: "pie",
                        style: {
                            font:
                                '18px Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
                        },
                    },
                    boost: {
                        useGPUTranslations: true,
                        usePreallocated: true,
                    },
                    title: {
                        text: "",
                    },
                    accessibility: {
                        point: {
                            valueSuffix: "%",
                        },
                    },
                    series: [
                        {
                            name: "Amount",
                            data: [
                                {
                                    name:
                                        this.props.lastRecord.temperature.intervals[0]
                                            .min +
                                        " to " +
                                        this.props.lastRecord.temperature.intervals[0]
                                            .max +
                                        " Celsius",
                                    y: this.props.lastRecord.temperature.intervals[0]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.temperature.intervals[1]
                                            .min +
                                        " to " +
                                        this.props.lastRecord.temperature.intervals[1]
                                            .max +
                                        " Celsius",
                                    y: this.props.lastRecord.temperature.intervals[1]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.temperature.intervals[2]
                                            .min +
                                        " to " +
                                        this.props.lastRecord.temperature.intervals[2]
                                            .max +
                                        " Celsius",
                                    y: this.props.lastRecord.temperature.intervals[2]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.temperature.intervals[3]
                                            .min +
                                        " to " +
                                        this.props.lastRecord.temperature.intervals[3]
                                            .max +
                                        " Celsius",
                                    y: this.props.lastRecord.temperature.intervals[3]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.temperature.intervals[4]
                                            .min +
                                        " to " +
                                        this.props.lastRecord.temperature.intervals[4]
                                            .max +
                                        " Celsius",
                                    y: this.props.lastRecord.temperature.intervals[4]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.temperature.intervals[5]
                                            .min +
                                        " to " +
                                        this.props.lastRecord.temperature.intervals[5]
                                            .max +
                                        " Celsius",
                                    y: this.props.lastRecord.temperature.intervals[5]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.temperature.intervals[6]
                                            .min +
                                        " to " +
                                        this.props.lastRecord.temperature.intervals[6]
                                            .max +
                                        " Celsius",
                                    y: this.props.lastRecord.temperature.intervals[6]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.temperature.intervals[7]
                                            .min +
                                        " to " +
                                        this.props.lastRecord.temperature.intervals[7]
                                            .max +
                                        " Celsius",
                                    y: this.props.lastRecord.temperature.intervals[7]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.temperature.intervals[8]
                                            .min +
                                        " to " +
                                        this.props.lastRecord.temperature.intervals[8]
                                            .max +
                                        " Celsius",
                                    y: this.props.lastRecord.temperature.intervals[8]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.lastRecord.temperature.intervals[9]
                                            .min +
                                        " to " +
                                        this.props.lastRecord.temperature.intervals[9]
                                            .max +
                                        " Celsius",
                                    y: this.props.lastRecord.temperature.intervals[9]
                                        .values.length,
                                },
                                {
                                    name:
                                        "more than " +
                                        this.props.lastRecord.temperature.intervals[9]
                                            .max +
                                        " Celsius",
                                    y: this.props.lastRecord.temperature.intervals[10]
                                        .values.length,
                                },
                            ],
                        },
                    ],
                    tooltip: {
                        hideDelay: 0,
                        pointFormat:
                            "{series.name}<b>:{point.y:.1f}</b><br>Percentage:{point.percentage:1.f}%",
                    },
                    plotOptions: {
                        series: {
                            options: {
                                turboTreshold: 1,
                            },
                        },
                        pie: {
                            animation: false,
                            allowPointSelect: true,
                            cursor: "pointer",
                            dataLabels: {
                                enabled: false,
                            },
                            showInLegend: true,
                        },
                    },
                }}
                ref={"temperatureChart"}
            />
        );
    }
    componentDidMount() {
        highchartsAccessibility(Highcharts);
        HC_exporting(Highcharts);
        HC_exportingOffline(Highcharts);
        HC_exportingData(Highcharts);
        HC_more(Highcharts);
        Boost(Highcharts);
    }
}

export default CardGraphTemperature;