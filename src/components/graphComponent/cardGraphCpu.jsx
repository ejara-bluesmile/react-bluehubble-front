import React, { Component } from 'react';
import { Card } from "react-bootstrap";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import ErrorCardBoody from "./errorCardBody"
import FooterCard from './footerCard';

import highchartsAccessibility from "highcharts/modules/accessibility";
import HC_exporting from "highcharts/modules/exporting";
import HC_exportingOffline from "highcharts/modules/offline-exporting";
import HC_exportingData from "highcharts/modules/export-data";
import HC_more from "highcharts/highcharts-more";
import Boost from "highcharts/modules/boost";

class CardGraphCpu extends Component {

    render() {
        return (<Card className="p-1 mb-1">
            <Card.Header>
                <Card.Title as="h5">Cpu</Card.Title>
            </Card.Header>
            <Card.Body className="p-1 m-0">
                {this.graphCpu()}
            </Card.Body>

            <FooterCard
                tipo={this.props.tipo}
                lastDate={this.props.lastDateCpu}
                lastValue={this.props.lastValueCpu}
                min={this.props.minCpu}
                max={this.props.maxCpu}
                avg={this.props.avgCpu}
                dataQuantity={this.props.dataQuantity}
                std={this.props.stdCpu}
            />
        </Card>);
    }
    graphCpu() {
        if (!this.props.lastRecord.cpuUsage || this.props.lastRecord.cpuUsage.length == 0) {
            return (<ErrorCardBoody />)
        }
        return (<HighchartsReact
            updateArgs={[false, true, false]}
            highcharts={Highcharts}
            options={{
                chart: {
                    animation: false,
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: "pie",
                    style: {
                        font:
                            '18px Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
                    },
                },
                boost: {
                    useGPUTranslations: true,
                    usePreallocated: true,
                },
                title: {
                    text: "",
                },
                accessibility: {
                    point: {
                        valueSuffix: "%",
                    },
                },
                series: [
                    {
                        name: "Amount",
                        data: [
                            {
                                name: 1 + "-" + 10 + "%",
                                y: this.props.lastRecord.cpuUsage.intervals[0]
                                    .values.length,
                            },
                            {
                                name: 11 + "-" + 20 + "%",
                                y: this.props.lastRecord.cpuUsage.intervals[1]
                                    .values.length,
                            },
                            {
                                name: 21 + "-" + 30 + "%",
                                y: this.props.lastRecord.cpuUsage.intervals[2]
                                    .values.length,
                            },
                            {
                                name: 31 + "-" + 40 + "%",
                                y: this.props.lastRecord.cpuUsage.intervals[3]
                                    .values.length,
                            },
                            {
                                name: 41 + "-" + 50 + "%",
                                y: this.props.lastRecord.cpuUsage.intervals[4]
                                    .values.length,
                            },
                            {
                                name: 51 + "-" + 60 + "%",
                                y: this.props.lastRecord.cpuUsage.intervals[5]
                                    .values.length,
                            },
                            {
                                name: 61 + "-" + 70 + "%",
                                y: this.props.lastRecord.cpuUsage.intervals[6]
                                    .values.length,
                            },
                            {
                                name: 71 + "-" + 80 + "%",
                                y: this.props.lastRecord.cpuUsage.intervals[7]
                                    .values.length,
                            },
                            {
                                name: 81 + "-" + 90 + "%",
                                y: this.props.lastRecord.cpuUsage.intervals[8]
                                    .values.length,
                            },
                            {
                                name: 91 + "-" + 100 + "%",
                                y: this.props.lastRecord.cpuUsage.intervals[9]
                                    .values.length,
                            },
                        ],
                    },
                ],
                tooltip: {
                    hideDelay: 0,
                    pointFormat:
                        "{series.name}<b>:{point.y:.1f}</b><br>Percentage:{point.percentage:1.f}%",
                },
                plotOptions: {
                    series: {
                        options: {
                            turboTreshold: 1,
                        },
                    },
                    pie: {
                        animation: false,
                        allowPointSelect: true,
                        cursor: "pointer",
                        dataLabels: {
                            enabled: false,
                        },
                        showInLegend: true,
                    },
                },
            }}
            ref={"cpuChart"}
        />);
    }
    componentDidMount() {
        highchartsAccessibility(Highcharts);
        HC_exporting(Highcharts);
        HC_exportingOffline(Highcharts);
        HC_exportingData(Highcharts);
        HC_more(Highcharts);
        Boost(Highcharts);
    }
}

export default CardGraphCpu;