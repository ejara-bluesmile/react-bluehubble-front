import React, { Component } from 'react';
import { Card } from "react-bootstrap";
import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts";
import ErrorCardBoody from "./errorCardBody"
import FooterCard from './footerCard';

import highchartsAccessibility from "highcharts/modules/accessibility";
import HC_exporting from "highcharts/modules/exporting";
import HC_exportingOffline from "highcharts/modules/offline-exporting";
import HC_exportingData from "highcharts/modules/export-data";
import HC_more from "highcharts/highcharts-more";
import Boost from "highcharts/modules/boost";
import swal from 'sweetalert2';


class CardGraphOpticalPower extends Component {
   handleClick1(e) {
        let index = e.point.index


        if (this.props.opticals.length > 0) {
            this.openSegment(this.props.opticals, "Optical Power", index)
        } else {

            this.openSegment(this.props.opticalPower[index].intervals.values, "Optical Power", index)
        }


    }
    openSegment(array, dataName, index) {

        let data = array.filter(element => element.interval == index)
        //console.log("data filter", data)
        if (data.length > 0) {
            localStorage.setItem("chartData", JSON.stringify({ name: dataName, data: array }))
            localStorage.setItem("chartInterval", index)
            window.open(`../chartData?layer=` + this.props.page, "")
        } else {
            swal.fire('Warning', 'No links or data found for the selected segment.', 'warning',
            )
        }
    }

    render() {
        return (
            <Card className="p-1 mb-1">
                <Card.Header>
                    <Card.Title as="h5">Optical Power</Card.Title>
                </Card.Header>
                <Card.Body className="p-1 m-0">
                    {this.graphOpticalPawer()}
                </Card.Body>


                <FooterCard
                    tipo={this.props.tipo}
                    lastDate={this.props.lastDateOptical}
                    lastValue={this.props.lastValueOptical}
                    min={this.props.minOptical}
                    max={this.props.maxOptical}
                    avg={this.props.avgOptical}
                    dataQuantity={this.props.dataQuantity}
                    std={this.props.stdOptical}
                />
            </Card>
        );
    }
    graphOpticalPawer() {
        if (!this.props.opticalPower || this.props.opticalPower.length == 0) {
            return (<ErrorCardBoody />);
        }
        return (
            <HighchartsReact
                updateArgs={[false, true, false]}
                highcharts={Highcharts}
                options={{
                    chart: {
                        animation: false,
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: "pie",
                        style: {
                            font:
                                '18px Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
                        },
                    },
                    boost: {
                        useGPUTranslations: true,
                        usePreallocated: true,
                    },
                    title: {
                        text: "",
                    },
                    accessibility: {
                        point: {
                            valueSuffix: "%",
                        },
                    },
                    series: [
                        {
                            name: "Amount",
                            data: [
                                {
                                    name:
                                        this.props.opticalPower.intervals[0]
                                            .min +
                                        " to " +
                                        this.props.opticalPower.intervals[0]
                                            .max +
                                        " dBm",
                                    y: this.props.opticalPower.intervals[0]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.opticalPower.intervals[1]
                                            .min +
                                        " to " +
                                        this.props.opticalPower.intervals[1]
                                            .max +
                                        " dBm",
                                    y: this.props.opticalPower.intervals[1]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.opticalPower.intervals[2]
                                            .min +
                                        " to " +
                                        this.props.opticalPower.intervals[2]
                                            .max +
                                        " dBm",
                                    y: this.props.opticalPower.intervals[2]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.opticalPower.intervals[3]
                                            .min +
                                        " to " +
                                        this.props.opticalPower.intervals[3]
                                            .max +
                                        " dBm",
                                    y: this.props.opticalPower.intervals[3]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.opticalPower.intervals[4]
                                            .min +
                                        " to " +
                                        this.props.opticalPower.intervals[4]
                                            .max +
                                        " dBm",
                                    y: this.props.opticalPower.intervals[4]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.opticalPower.intervals[5]
                                            .min +
                                        " to " +
                                        this.props.opticalPower.intervals[5]
                                            .max +
                                        " dBm",
                                    y: this.props.opticalPower.intervals[5]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.opticalPower.intervals[6]
                                            .min +
                                        " to " +
                                        this.props.opticalPower.intervals[6]
                                            .max +
                                        " dBm",
                                    y: this.props.opticalPower.intervals[6]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.opticalPower.intervals[7]
                                            .min +
                                        " to " +
                                        this.props.opticalPower.intervals[7]
                                            .max +
                                        " dBm",
                                    y: this.props.opticalPower.intervals[7]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.opticalPower.intervals[8]
                                            .min +
                                        " to " +
                                        this.props.opticalPower.intervals[8]
                                            .max +
                                        " dBm",
                                    y: this.props.opticalPower.intervals[8]
                                        .values.length,
                                },
                                {
                                    name:
                                        this.props.opticalPower.intervals[9]
                                            .min +
                                        " to " +
                                        this.props.opticalPower.intervals[9]
                                            .max +
                                        " dBm",
                                    y: this.props.opticalPower.intervals[9]
                                        .values.length,
                                },
                                {
                                    name:
                                        "more than " +
                                        this.props.opticalPower.intervals[9]
                                            .max +
                                        " dBm",
                                    y: this.props.opticalPower.intervals[10]
                                        .values.length,
                                },
                            ],
                        },
                    ],
                    tooltip: {
                        hideDelay: 0,
                        pointFormat:
                            "{series.name}<b>:{point.y:.1f}</b><br>Percentage:{point.percentage:1.f}%",
                    },
                    plotOptions: {
                        series: {
                            options: {
                                turboTreshold: 1,
                            },
                        },
                        pie: {
                            animation: false,
                            allowPointSelect: true,
                            cursor: "pointer",
                            dataLabels: {
                                enabled: false,
                            },
                            showInLegend: true,
                            events: {
                                click: (e) => {
                                    this.Hnadleclik1(e)
                                }
                            }
                        },
                    },
                }}
                ref={"opticalPowerChart"}
            />
        )
    }
    componentDidUpdate() {
        if (this.refs.opticalPowerChart) {
            this.refs.opticalPowerChart.chart.redraw();
        }

    }
    componentDidMount() {
        highchartsAccessibility(Highcharts);
        HC_exporting(Highcharts);
        HC_exportingOffline(Highcharts);
        HC_exportingData(Highcharts);
        HC_more(Highcharts);
        Boost(Highcharts);
    }
}

export default CardGraphOpticalPower;




