import React, { useEffect, useState } from 'react';
import { Card } from "react-bootstrap";
import 'jquery/dist/jquery.min.js';
import $ from 'jquery';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min.js';

import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"






const TableWarning = ({ dataNew, loadin }) => {




    useEffect(() => {
        if (loadin) {
            $(document).ready(function () {
                $('#table').DataTable();
            });


        };
    }, [loadin])


    if (!dataNew || !dataNew) {
        return ("loading")
    }



    return (
        <Card>
            <Card.Header>
                Data
           </Card.Header>
            <Card.Body>
                <table id="table" className=" table-striped table-bordered table-responsive  table-dark">

                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Trail Name</th>
                            <th>Source_Ne_Name </th>
                            <th>Sink_Ne_Name</th>
                            <th>OC_Source</th>
                            <th>OC_Sink</th>
                            <th>Stream</th>
                        </tr>
                    </thead>


                    <tbody>


                        {
                            dataNew.map((item) =>
                            (< tr key={item.name}>
                                <td>{item.name}</td>
                                <td>{item.trailName}</td>
                                <td>{item.sourceName}</td>
                                <td>{item.sinkName}</td>
                                <td >{item.sourceOc}</td>
                                <td >{item.sinkOC}</td>

                                <td><a className="dataTable-StreamLink" target="_blank" href="../../Views/Map.js">Stream</a></td>
                            </tr>)




                            )

                        }
                    </tbody>

                </table >

            </Card.Body>
        </Card>



    );




}
export default TableWarning

