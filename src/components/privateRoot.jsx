import React from 'react'
import { Route, Switch, Redirect } from "react-router-dom";
//import routes from "../../../routes";
import { useHistory } from "react-router-dom";

const PrivateRoot = (props, { route }) => {

   
    if (localStorage.getItem('login')) {
        if(props.externo){
            return <a href={this.props.url} target="_blank"></a>
        }
      
        return (
            <Route

                path={props.path}
                exact={props.exact}
                name={props.name}
                render={props.render}
            />
        );
    } else {
       
        return <Redirect to="/signin"></Redirect>
    }



}
export default PrivateRoot
