import React, { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import "datatables.net-dt/css/jquery.dataTables.min.css"
// import $ from "jquery";  

export default (props) => {
    // useEffect(() => {
    //     if (props.rows)
    //         $(`#example${props.number}`).DataTable();
    // }, [props.rows]);

    if (props.rows) {
        const row = props.rows;
        return (
            <div>

                <table id={`example${props.number}`} className="dataTable display">
                    <thead>
                        <tr>
                            <th>SOURCE IP</th>
                            <th>SINK IP</th>
                            <th>OUT BANDWIDTH (%)</th>
                            <th>OUT BANDWIDTH (Kbps)</th>
                            <th>IN BANDWIDTH (%)</th>
                            <th>IN BANDWIDTH (Kbps)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{props.source}</td>
                            <td>{props.sink}</td>
                            <td>{row.avgBandwidthOut}</td>
                            <td>{row.avgBandwidthValueOut}</td>
                            <td>{row.avgBandwidthIn}</td>
                            <td>{row.avgBandwidthValueIn}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
    return <div className="App">Loading...</div>;
}