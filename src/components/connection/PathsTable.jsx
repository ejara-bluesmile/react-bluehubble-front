import React, { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
import $ from "jquery";


export default (props) => {
    useEffect(() => {
        if (props.rows)
            $(`#example${props.number}`).DataTable();
    }, [props.rows]);

    if (props.rows) {
        return (
            <div>

                <table id={`example${props.number}`} class="display">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>SOURCE IP</th>
                            <th>SINK IP</th>
                            <th>OUT BANDWIDTH (%)</th>
                            <th>OUT BANDWIDTH (Kbps)</th>
                            <th>IN BANDWIDTH (%)</th>
                            <th>IN BANDWIDTH (Kbps)</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.rows.map(row => (
                            <tr>
                                <td>{row.number}</td>
                                <td>{row.source_ip}</td>
                                <td>{row.sink_ip}</td>
                                <td>{row.bandwidthOutPercentage}</td>
                                <td>{row.bandwidthOutValue}</td>
                                <td>{row.bandwidthInPercentage}</td>
                                <td>{row.bandwidthInValue}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
    }
    return <div className="App">Loading...</div>;
}