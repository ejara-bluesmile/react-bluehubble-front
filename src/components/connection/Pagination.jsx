import React, { useEffect, useState } from 'react';
import PathsContainer from './PathsContainer';
import Pagination from "react-js-pagination";
import '../../style/pagination.css';

export default (props) => {
    const [pageCount, setPageCount] = useState(0);
    const [contents, setContents] = useState([]);
    const [pageContent, setPageContent] = useState([]);
    const [selectedPage, setSelectedPage] = useState(1);
    const [totalItems, setTotalItems] = useState(0);

    const perPage = 3;
    useEffect(() => console.log(contents), [contents]);
    useEffect(() => { mapPaths() }, [props.paths]);

    const mapPaths = async () => {
        const size = props.paths.length;
        setPageCount(Math.ceil(size / perPage));

        let newContents = [];
        for (let i = 0; i < size; i++) {
            if (i % perPage === 0) newContents.push([])
            let rows = [];
            let avgBandwidthIn = 0
            let avgBandwidthValueIn = 0
            let avgBandwidthOut = 0
            let avgBandwidthValueOut = 0
            let nIn = 0;
            let nOut = 0;
            for (let j = 0; j < props.paths[i].length; j++) {
                const element = props.paths[i][j];
                if (!isNaN(element.bandwidthInPercentage)) {
                    avgBandwidthIn += element.bandwidthInPercentage;
                    avgBandwidthValueIn += element.bandwidthInValue;
                    nIn++;
                }
                if (!isNaN(element.bandwidthOutPercentage)) {
                    avgBandwidthOut += element.bandwidthOutPercentage;
                    avgBandwidthValueOut += element.bandwidthOutValue;
                    nOut++;
                }
                rows.push({
                    number: j + 1,
                    source_ip: element.source_ip,
                    sink_ip: element.sink_ip,
                    bandwidthOutPercentage: element.bandwidthOutPercentage,
                    bandwidthOutValue: element.bandwidthOutValue,
                    bandwidthInPercentage: element.bandwidthInPercentage,
                    bandwidthInValue: element.bandwidthInValue,
                })
            }
            const avgs = {

                avgBandwidthIn: avgBandwidthIn /= nIn,
                avgBandwidthOut: avgBandwidthOut /= nOut,
                avgBandwidthValueIn: avgBandwidthValueIn /= nIn,
                avgBandwidthValueOut: avgBandwidthValueOut /= nOut,
            }
            const content = { number: i + 1, rows, avgs };
            newContents[newContents.length - 1].push(content);
        }
        setContents(newContents);
        setTotalItems(props.paths.length);
        let contentPaths = [];
        for (let i = 0; i < newContents[0].length; i++) {
            contentPaths.push(newContents[0][i]);
        }
        setPageContent(contentPaths);
    }


    const handlePageChange = (selected) => {
        setSelectedPage(selected);
        let contentPaths = [];
        for (let i = 0; i < contents[selected - 1].length; i++) {
            contentPaths.push(contents[selected - 1][i]);
        }
        setPageContent(contentPaths);
    }

    return (
        <div className="commentBox">
            <PathsContainer data={pageContent} source={props.source} sink={props.sink} />
            <Pagination
                activePage={selectedPage}
                itemsCountPerPage={perPage}
                totalItemsCount={totalItems}
                pageRangeDisplayed={5}
                itemClass="page-item"
                linkClass="page-link"
                onChange={handlePageChange}
            />
        </div>
    );
}