import React, { Component } from 'react';
import PathsCollapse from './PathsCollapse';

export default (props) => {
  
    const { data } = props;
    return (
        <div>
            { data.map((element, i) =>
                <>
                    <PathsCollapse number={element.number} content={element} source={props.source} sink={props.sink} />
                    <br></br>
                </>
            )}
        </div>
    )
}