import React, { useState } from 'react';
import {
  Button,
  Collapse
} from "react-bootstrap";
import PathsTable from './PathsTable';
import AvgsTable from './AvgsTable';

export default (props) => {
  const [open, setOpen] = useState(false);
  return (
    <>
      <Button
        className="path-collapse-button"
        onClick={() => setOpen(!open)}
        aria-controls="collapse-content"
        aria-expanded={open}
      >
        Path {props.number} - {props.content.rows.length} Jumps
      </Button>
      <Collapse in={open}>
        <div id="collapse-content">
          <PathsTable rows={props.content.rows} number={props.number} />
          <br></br>
          <center>
            <AvgsTable
              rows={props.content.avgs} number={props.number}
              source={props.source} sink={props.sink} />
          </center>
        </div>
      </Collapse>
    </>
  );
}