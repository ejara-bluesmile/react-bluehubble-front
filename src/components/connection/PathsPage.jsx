import React from 'react';
import PathsCollapse from './PathsCollapse';

export default (props) => {

    return (
        <div>
            {props.data.map((path, i) => (
                    <PathsCollapse number={i} content={path} />
            ))}
        </div>
    )
}