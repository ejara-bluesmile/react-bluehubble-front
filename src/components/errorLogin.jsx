import React from 'react'
import { Card, Row } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import "../assets/scss/errorLogin.scss"


const SignIn = () => {


    let history = useHistory();

    const navigate = useHistory();




    return (
        <Card className="p-1 mb-1">
            <Card.Header>
                <Card.Title as="h5">permission denied</Card.Title>
            </Card.Header>

            <Card.Body className="p-1 m-0">


                <span className="feather icon-alert-octagon iconErrorLogin"></span>


            </Card.Body>
            <Card.Footer>
                <Row>
                    <h6 className="texerror">Sorry, to access this page a login is required</h6>
                    <button className="ButonLogin" onClick={() => history.push("/signin")} >go to logIn</button>
                </Row>

            </Card.Footer>
        </Card>
    )
}


export default SignIn
