import React, { useRef, useEffect } from 'react'
import { Card } from "react-bootstrap";
import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts";
const Graft = ({ ids, values }) => {

    const grafRef = useRef(null)
    useEffect(() => {
        let graf = grafRef.current
        graf.chart.redraw();
    })
    return (
        <Card>
            <Card.Header>
                <Card.Title as="h5">Graphic</Card.Title>
            </Card.Header>
            <Card.Body>
                <HighchartsReact
                    updateArgs={[false, true, false]}
                    highcharts={Highcharts}
                    options={{
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Data'
                        },
                        xAxis: {
                            categories: ids,
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Value'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [{ name: "Devices Avg", data: values, color: "#1D3582" }]
                    }}
                    ref={grafRef}
                />
            </Card.Body>
        </Card>
    )
}

export default Graft
