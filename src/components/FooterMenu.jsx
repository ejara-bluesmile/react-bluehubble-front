import React from 'react'
import logoBlue from  "../assets/images/LOGOS/LOGOS/logoBlueBlanco2.webp"
import "../assets/scss/FooterMenu.scss"
const FooterMenu = () => {
    return (
        <footer class="bg-light text-center text-lg-start footer-bule">
  
        <div class="text-center p-3">
         
        designed by: <br></br>
          <a class="text-dark" href="https://bluesmile.cl/" target="_blank"  >
               <img className="logoblueSmile" src={logoBlue}></img> </a><br></br>
               © 2021 
        </div>
      
      </footer>
    )
}

export default FooterMenu
