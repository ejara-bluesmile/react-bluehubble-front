import React, { useState, useEffect } from 'react';
import 'jquery/dist/jquery.min.js';
import $ from 'jquery';

import 'bootstrap/dist/css/bootstrap.min.css';

import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"

export default (props) => {
    // const [warnings, setWarnings] = useState([]);
    useEffect(() => {
        if (props.warnings)
            $('#warningTable').DataTable();
    }, [props.warnings]);

    return (
        <>
            <div style={{"overflow-x":"auto"}}>
                <table id="warningTable" >
                    <thead>
                        <tr>
                            <th>Date</th>
                            {/* <th style="width: 240px">Service Name</th> */}
                            <th>Service Name</th>
                            <th>Problem</th>
                            <th>Link</th>
                        </tr>
                    </thead>
                    <tbody id="body-port">
                        {props.warnings.map(warning => {
                            console.log(warning)
                            const dateString = warning.time.toString();
                            const date = dateString.split("T")[0];
                            const time = dateString.split("T")[1];
                            const timeFinal = time.slice(0, -1);
                            return (
                                <tr>
                                    <td>{`${date}  ${timeFinal.substring(0, 8)}`}</td>
                                    <td>{warning.name}</td>
                                    <td>{warning.message}</td>
                                    <td><a>Real time streaming</a></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </>
    )
}