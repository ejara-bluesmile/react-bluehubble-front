import React, { useEffect, useState } from 'react';
import { Table } from "react-bootstrap";
import 'jquery/dist/jquery.min.js';
import $ from 'jquery';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min.js';

import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"


import swal from 'sweetalert2';



export default (props) => {
    const [isShow, setSHow] = useState(false);



    useEffect(() => {
        if (props.load == true) {
            $(document).ready(function () {
                $('#table').DataTable();
            });


        };
    }, [props.load])


    if (props.load == false) {
        return ("loading")
    }

    const htmpPort = (ports) => {
        $(document).ready(function () {
            $('#portTable').DataTable();
        });
      
        var popupCountedPort;
        popupCountedPort = `<table id="portTable" class=" table-striped table-bordered table-responsive  table-dark" >
        <thead>
            <tr>
                <th>ip-port</th>
                <th>ifAlias</th>
                <th>ifDuplex</th>
                <th>ifHighSpeed</th>
                <th >ifName</th>
                <th >ifSpeed</th>
                <th >ifType</th>
                <th >portNames</th>
            </tr>
        </thead>
        <tbody id="body-port">`

        for (const port of ports) {

            popupCountedPort += `<tr>
            <td> ${port.ipPort == null ? "not assigned" : port.ipPort}</td>
            <td> ${port.ifAlias == null || port.ifAlias == "None" || port.ifAlias == "Null0" ? "not configured" : port.ifAlias}</td>
            <td> ${port.ifDuplex == "None" || port.ifDuplex == null ? "unknown" : port.ifDuplex}</td>
            <td> ${port.ifHighSpeed == null ? "unknown" : port.ifHighSpeed}</td>
            <td>${port.ifName == null ? "not configured" : port.ifName}</td>
            <td> ${port.ifSpeed}</td>
            <td> ${port.ifType == null || port.ifType == "None" ? "unknown" : port.ifType}</td>
            <td> ${port.portNames == null ? "not configured" : port.portNames}</td>
            </tr>`
        }
        popupCountedPort += ` </tbody>
        </table>`
        
        return popupCountedPort
    }
    const mode = (hostname) => {
        var deviceTrans = props.itemsTable1.find(element => element.hostname == hostname);
        let pro = htmpPort(deviceTrans.ports)
        swal.fire({
            title: ` <strong>${hostname}</strong>`,
            html: "<h3>Ports</h3>" + pro,
            showCloseButton: true,
            showCancelButton: false,
            focusConfirm: false,
            backdrop: "test",

            width: 1212
        })
    }

    return (
        <table id="table" className=" table-striped table-bordered table-responsive  table-dark">

            <thead >
                <tr>
                    <th>hostname</th>
                    <th>Vendor</th>
                    <th>type</th>
                    <th className="description">description</th>
                    <th>ip</th>
                    <th>OS</th>
                    <th>port</th>
                </tr>
            </thead>


            <tbody>


                {
                    props.itemsTable1.map((item) =>
                    (< tr key={item.hostname}>
                        <td>{item.sysName}</td>
                        <td>{item.vendor}</td>
                        <td>{item.type}</td>
                        <td >{item.sysDescr}</td>
                        <td>{item.hostname}</td>
                        <td > {item.os}</td>

                        <td><div><button type="button" className="btn portBtn" onClick={() => mode(item.hostname)}>Ports</button>

                        </div>
                          
                        </td>
                    </tr>)




                    )

                }
            </tbody>

        </table >




    );




}

