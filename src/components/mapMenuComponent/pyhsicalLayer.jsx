
import React, { useState } from 'react';
import { Row, Col, Card } from "react-bootstrap";
import PysicalLayerCol from './pysicalLayerCol';


const PyhsicalLayer = React.memo(function () {
    const [isChecked, setIsChecked] = useState(true);
    const [isChecked2, setIsChecked2] = useState(true);
    const [isChecked3, setIsChecked3] = useState(true);
    const [isChecked4, setIsChecked4] = useState(true);

    const toggleChange = () => {
        setIsChecked(currentIschecked => !isChecked);
    };
    const toggleChange2 = () => {
        setIsChecked2(currentIschecked => !isChecked2);
    };
    const toggleChange3 = () => {
        setIsChecked3(currentIschecked => !isChecked3);
    };
    const toggleChange4 = () => {
        setIsChecked4(currentIschecked => !isChecked4);
    };
    return (
        <Card
            id="contentPhysical"
            className=" m-0"
            style={{ height: "100%" }}
        >
            <Card.Body className="p-1 ">
                <h6>Physical Layer</h6>
                <Col>
                    <PysicalLayerCol
                        name="IP_ELEMENT"
                        checked={isChecked}
                        onChange={toggleChange}



                        name2="IP_INTERFACE"
                        checked2={isChecked2}
                        onChange2={toggleChange2}

                    />

                    <PysicalLayerCol
                        name="OCH_ELEMENT"
                        checked={isChecked3}
                        onChange={toggleChange3}

                        name2="OCH"
                        checked2={isChecked4}
                        onChange2={toggleChange4}

                    />
                </Col>
            </Card.Body>
        </Card>
    );



}
)
export default PyhsicalLayer;