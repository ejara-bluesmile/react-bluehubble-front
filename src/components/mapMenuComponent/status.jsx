import React from 'react';
import { Card } from "react-bootstrap";
import redButton from "../../assets/images/redbutton.png";
import greenButton from '../../assets/images/greenbutton.png';
import InjectionMenu from './../trafficInjection/InjectionMenu';


const status = React.memo(function (props) {
    const getState = (status) => {
        if (status === redButton) {
            return "offline"
        }
        if (status === greenButton) {
            return "online"
        }

        return "warning"
    }

    return (

        <Card className="p-1 mb-1" style={{ height: "100%" }}>
            <Card.Body className="p-1 m-0">
                <h6 className="m-1">
                    Status{" "}
                    <a href="">
                        <img src={props.status} alt="" style={{ width: "15px" }} />
                    </a>
                </h6>
                <hr className="m-0 p-0" />
                <p className="m-1" style={{ fontSize: "15px" }}>
                    Live writing: {getState(props.status)}
                </p>
                <InjectionMenu/>
            </Card.Body>
        </Card>
    );
})



export default status
