import React, { useState } from 'react';
import {
    Row,
    Col,
    Card,
    Form,
    Button
} from "react-bootstrap";
import '../../assets/scss/warningFilter.scss';
import WarningTable from '../table/WarningTable';
import 'jquery/dist/jquery.min.js';
import $ from 'jquery';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const MySwal = withReactContent(Swal);

const WarningFilter = (props) => {
    const [isChecked, setIsChecked] = useState(true);
    const [isChecked2, setIsChecked2] = useState(false);
    const [isChecked3, setIsChecked3] = useState(true);

    const toggleChange = () => {
        setIsChecked(currentIschecked => !isChecked);
    };
    const toggleChange2 = () => {
        setIsChecked2(!isChecked2);
        setIsChecked3(!isChecked3);
        props.onLayerChange('IP_INTERFACE');
    };
    const toggleChange3 = () => {
        setIsChecked3(!isChecked3);
        setIsChecked2(!isChecked2);
        props.onLayerChange('OCH');
    };
    
    const openModal = () => {
        $.ajax({
            type: 'post',
            url: 'http://3.129.97.184:3003/findWarningRecords',
            data: {},
            success: function (response) {
                let alertsArray = response.result.data;
                // console.log(markersArray[0])
                for (let index = 0; index < alertsArray.length; index++) {
                    
                    const idAux =  alertsArray[index].idDevice.replace('out', '');
                    const idAux2 = idAux.replace('in', '');
                    let marker = props.markers[3].features.find(e => e.properties.ID === idAux2);
                    alertsArray[index].name = marker.name;
                    alertsArray[index].id = idAux;
                    
                }
                MySwal.fire({
                    title: <strong>Latest Warnings</strong>,
                    html: <>
                        <WarningTable warnings={alertsArray} />
                    </>,
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                    backdrop: "test",
                    width: 1128
                })
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //error
                // $(loading).css("display", "none");
            }
        });
    }

    return (
        <React.Fragment>
           
            <Col md={2} xl={2} sm={2} className="p-1">
                <Card className="p-1 mb-1" style={{ height: "100%" }}>
                    <Card.Body className="p-1 ">
                        <Form.Group style={{ "margin-bottom": "0rem" }}>
                            <Form.Check
                                inline
                                label="IP"
                                name="formHorizontalRadios"
                                type={"radio"}
                                checked={isChecked2} id="showIPNotifications"
                                onChange={toggleChange2}
                                style={{ fontSize: "12px" }}
                            />
                            <Form.Check
                                inline
                                label="MMOO"
                                name="formHorizontalRadios"
                                type={"radio"}
                                checked={isChecked3}
                                id="showOCHNotifications"
                                onChange={toggleChange3}
                                style={{ fontSize: "12px" }}
                            />
                            <Form.Check
                                custom
                                type="checkbox"
                                checked={isChecked}
                                id="checkbox3"
                                label="Show Live Warnings"
                                style={{ fontSize: "12px" }}
                                className="m-1"
                                onChange={toggleChange}
                            />
                            <Button 
                                className="show-warnings-btn"
                                onClick={openModal}>
                                Show Latest Warnings
                            </Button>
                        </Form.Group>
                    </Card.Body>
                </Card>
            </Col>
        </React.Fragment>
    );

}
export default WarningFilter 