import React, { Component } from 'react';
import { Col } from "react-bootstrap";
import '../../assets/scss/warningFilter.scss';

const SearchMap = React.memo(function () {
    return (
        <React.Fragment>
            <Col md="3" xl="2" className="p-1">
                <div id="searchLayer" style={{ width: "100%" }}>
                    <div> Search per Line</div>
                    <div id="IP_INTERFACEsearch" className="layer-search"></div>
                    <div id="OCHsearch" className="layer-search"></div>
                </div>
            </Col>
            <Col md="3" xl="2" className="p-1">
                <div id="searchSite">
                    <div> Search per Site</div>
                </div>
            </Col>
        </React.Fragment>);

})
export default SearchMap;
