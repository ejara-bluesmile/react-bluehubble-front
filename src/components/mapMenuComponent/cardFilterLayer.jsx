import React, { Component } from 'react';
import { Card } from "react-bootstrap";

const CardFileterLayer = React.memo(function () {
    {

        return (
            <Card
                id="contentFilter"
                className=" m-0"
                style={{ height: "100%" }}
            >
                <Card.Body className="p-1 ">
                    <h6>Filter per Layer</h6>
                    <div id="perLayerIP"></div>
                    <div id="perLayerOCH"></div>
                </Card.Body>
            </Card>
        );
    }
})
export default CardFileterLayer
