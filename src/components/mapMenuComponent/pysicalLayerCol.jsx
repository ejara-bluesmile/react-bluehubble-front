import React, { Component } from 'react';
import { Col } from "react-bootstrap";
const PysicalLayerCol = React.memo(function (props) {

    return (
        <React.Fragment>
        
            <div className="form-check1">
                <label className="form-check-label1 check-label" htmlFor="">
                    {props.name.replace('ELEMENT','DEVICE').replace('OCH','MMOO')}
                </label>
                <input
                    checked={props.checked}
                    onChange={props.onChange}
                    type="checkbox"
                    className="form-check-input1"
                    id={props.name}

                />
            </div>
            <div className="form-check1">
                <label className="form-check-label1 check-label" htmlFor="">
                    {props.name2.replace('OCH','MMOO')}
                </label>
                <input
                    checked={props.checked2}
                    onChange={props.onChange2}
                    type="checkbox"
                    className="form-check-input1"
                    id={props.name2}

                />
            </div>
            
            </React.Fragment>
        );
})

export default PysicalLayerCol

