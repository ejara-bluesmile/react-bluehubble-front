import React from 'react';

export default (props) => {

    return (
        <div style={{ "text-align": "center" }}>
            <h5>{props.title}</h5>
            <h6 className="countenido">
                Minimum value: <span>{props.min}</span>
            </h6>
            <h6 className="countenido">
                Maximum value: <span>{props.max}</span>
            </h6 >
            <h6 className="countenido">
                Average Value:
        <span>{props.avg.toFixed(16)}</span>
            </h6>
            <h6 className="countenido">
                Median Value:
        <span>{props.median.toFixed(16)}</span>
            </h6 >
            <h6 className="countenido">
                Variance Value:
        <span>{props.variance.toFixed(16)}</span>
            </h6>
            <h6 className="countenido">
                Standar deviation:
        <span>{props.std.toFixed(16)}</span>
            </h6>
            <h6 className="countenido">
                Amount of data: <span>{props.amount}</span>
            </h6>
        </div>
    );
}