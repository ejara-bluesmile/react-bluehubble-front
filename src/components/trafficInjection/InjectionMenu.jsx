import React, { useState, useEffect } from 'react';
import { DropdownButton, Dropdown } from 'react-bootstrap';
import { socket2 as socket } from "../../App/components/map-utils/MarkerCluster";
import '../../assets/scss/injection.scss';

export default (props) => {

    const injectTraffic = () => {
        socket.transmit('trafficInjected', 'traffic injected');
    }
    const solveTraffic = () => {
        socket.transmit('trafficSolved', 'traffic solved');
    }
    const disinjectTraffic = () => {
        socket.transmit('trafficDisInjected', 'traffic disinjected');
    }

    const resetTraffic = () => {
        socket.transmit('trafficReset', 'traffic reset');
    }
    useEffect(() => resetTraffic(), []);

    return (
        <>
            <DropdownButton
             id="dropdown-basic-button" title="Traffic Injection">
                <Dropdown.Item as="button" onClick={injectTraffic}>ON</Dropdown.Item>
                <Dropdown.Item as="button" onClick={disinjectTraffic}>OFF</Dropdown.Item>
                <Dropdown.Item as="button" onClick={resetTraffic}>RESET</Dropdown.Item>
            </DropdownButton>
        </>
    )
}
