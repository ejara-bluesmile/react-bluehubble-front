import React, { Component } from "react";
import Highcharts from "highcharts";
import highchartsAccessibility from "highcharts/modules/accessibility";
import HighchartsReact from "highcharts-react-official";
import Boost from "highcharts/modules/boost";

export default class chart extends Component {
  state = {
    option: {
      chart: { polar: true },

      title: {
        text: "",
      },

      xAxis: {
        categories: [
          "Bandwidht",
          "Temperature",
          "Optical Power",
          "Cpu Usage",
          "Delay",
          "Jitter",
          "Bit Error Rate",
          "Bit Error Ratio",
        ],
        tickmarkPlacement: "on",
        lineWidth: 0,
      },

      yAxis: {
        gridLineInterpolation: "polygon",
        lineWidth: 0,
        min: 0,
      },
      tooltip: {
        pointFormat: "{point.name}: <b>{point.percentage:.1f}%</b>",
      },

      series: [
        {
          name: "Allocated Budget",
          data: [43000, 19000, 60000, 35000, 17000, 10000],
          pointPlacement: "on",
        },
        {
          name: "Actual Spending",
          data: [50000, 39000, 42000, 31000, 26000, 14000],
          pointPlacement: "on",
        },
      ],
    },
  };
  render() {
    return (
      <div>
        <HighchartsReact
          // updateArgs={[false, true, true]}
          highcharts={Highcharts}
          options={this.state.option}
          ref={this.props.ref}
          oneToOne={true}
          allowChartUpdate={true}
        ></HighchartsReact>
      </div>
    );
  }
}
