import React, { Component } from "react";
import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts";

export default class warning extends Component {
  state = {
    option: {
      chart: {
        type: "scatter",
        zoomType: "y",
      },
      title: {
        text: "Warnings Graphs",
      },
      xAxis: {
        type: "datetime",
      },
      yAxis: {
        title: {
          text: "value",
        },
      },
      legend: {
        enabled: true,
      },
      series: [
        {
          name: "BandWidth IN Warnings",
          data: this.props.data,
        },
        {
          name: "BandWidth OUT Warnings",
          data: this.props.data2,
        },
        {
          name: "IN Octects Warnings",
          data: this.props.data3,
        },
        {
          name: "OUT Octects Warnings",
          data: this.props.data4,
        },
        {
          name: "Rx Warnings",
          data: this.props.data5,
        },
        {
          name: "Tx Warnings",
          data: this.props.data6,
        },
        {
          name: "Temperature Warnings",
          data: this.props.data7,
        },
        {
          name: "Cpu Warnings",
          data: this.props.data8,
        },
        {
          name: "Bit Error Rate Warnings",
          data: this.props.data9,
        },
        {
          name: "Bit Error Ratio Warnings",
          data: this.props.data10,
        },
        {
          name: "Delay Warnings",
          data: this.props.data11,
        },
        {
          name: "Jitter Warnings",
          data: this.props.data12,
        },
      ],
    },
  };
  render() {
    return (
      <div>
        <HighchartsReact
          hihcharts={Highcharts}
          options={this.state.option}
        ></HighchartsReact>
      </div>
    );
  }
}
