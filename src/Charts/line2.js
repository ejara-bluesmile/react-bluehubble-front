import React, { Component } from "react";
import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts";

export default class line extends Component {
  state = {
    option: {
      chart: {
        type: "area",
        zoomType: "x",
      },
      title: {
        text: this.props.title,
      },

      xAxis: {
        type: "datetime",
      },
      yAxis: {
        title: {
          text: this.props.yvalue,
        },
      },
      legend: {
        enabled: true,
      },
      series: [
        {
          fillColor: {
            linearGradient: [0, 0, 0, 300],
            stops: [
              [0, Highcharts.getOptions().colors[0]],
              [
                1,
                Highcharts.color(Highcharts.getOptions().colors[0])
                  .setOpacity(0)
                  .get("rgba"),
              ],
            ],
          },
          type: "area",
          color: "#0066cc",
          name: this.props.name,
          data: this.props.data,
        },
        {
          fillColor: {
            linearGradient: [0, 0, 0, 300],
            stops: [
              [0, Highcharts.getOptions().colors[0]],
              [
                1,
                Highcharts.color(Highcharts.getOptions().colors[3])
                  .setOpacity(0)
                  .get("rgba"),
              ],
            ],
          },
          type: "area",
          color: "#FF0000",
          name: this.props.name2,
          data: this.props.data2,
        },
      ],
    },
  };
  graphic= React.createRef();
  
  render() {
    return (
      <div>
        {" "}
        <HighchartsReact
          highcharts={Highcharts}
          options={this.state.option}
          ref={this.graphic}
        ></HighchartsReact>
      </div>
    );
  }
}
