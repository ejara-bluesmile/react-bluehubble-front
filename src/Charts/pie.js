import React, { Component } from "react";
import Highcharts from "highcharts";
import highchartsAccessibility from "highcharts/modules/accessibility";
import HighchartsReact from "highcharts-react-official";
import Boost from "highcharts/modules/boost";

export default class chart extends Component {
  state = {
    option: {
      chart: {
        animation: false,
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: this.props.type,
        style: {
          font: '18px Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"'
        }
      },
      boost: {
        useGPUTranslations: true,
        usePreallocated: true
      },
      title: {
        text: "",
      },
      tooltip: {
        pointFormat: "{point.name}: <b>{point.percentage:.1f}%</b>",
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        },
        legend: {
          enabled: true,
          // floating: true,
          // verticalAlign: "xbottom",
          // align: "bottom",
          // layout: "vertical",
          y: 10,
          labelFormatter: function () {
            var total = 0,
              percentage;
            percentage = ((this.y / total) * 100).toFixed(2);
            return (
              this.name +
              this.y +
              '(<span style="color:' +
              this.color +
              '">' +
              percentage +
              "%)"
            );
          },
        },
        series: [
          {
            name: 'Amount',
            data: this.props.data,
          },
        ],
        tooltip: {
          pointFormat: '{series.name}<b>:{point.y:.1f}</b><br>Percentage:{point.percentage:1.f}%'
        },
        plotOptions: {
          series: {
            options: {
              turboTreshold: 1
            }
          },

          pie: {
            allowPointSelect: true,
            cursor: "pointer",
            dataLabels: {
              enabled: false,
            },
            showInLegend: true,
          },
        },
      },
    }
  }
  render() {
    return (
      <div>
        <HighchartsReact

          // updateArgs={[false, true, true]}
          highcharts={Highcharts}
          options={this.state.option}
          ref={this.props.ref}
          oneToOne={true}
          allowChartUpdate={true}
        ></HighchartsReact>
      </div>
    );
  }
}
