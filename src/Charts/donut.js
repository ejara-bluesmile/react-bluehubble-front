import React, { Component } from "react";
import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts";

export default class semicircle extends Component {
  state = {
    option: {
      chart: { type: "pie" },
      title: {
        text: "",
      },
      legend: {
        enabled: true,
        // floating: true,
        // verticalAlign: "xbottom",
        // align: "bottom",
        // layout: "vertical",
        y: 10,
        labelFormatter: function () {
          var total = 0,
            percentage;
          percentage = ((this.y / total) * 100).toFixed(2);
          return (
            this.name +
            this.y +
            '(<span style="color:' +
            this.color +
            '">' +
            percentage +
            "%)"
          );
        },
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
      },
      series: [
        {
          name: "Amount",
          data: this.props.data,
          size: "60%",
          innerSize: "40%",
          showInLegend: true,
          dataLabels: {
            enabled: false,
          },
        },
      ],
      plotOptions: {
        series: {
          options: {
            turboTreshold: 1
          }
        },

        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: false,
          },
          showInLegend: true,
        },
      },
    },
  };
  render() {
    return (
      <div>
        {" "}
        <HighchartsReact
          hihcharts={Highcharts}
          options={this.state.option}
        ></HighchartsReact>
      </div>
    );
  }
}
