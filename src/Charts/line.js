import React, { Component } from "react";
import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts";

export default class line extends Component {
  state = {
    option: {
      chart: {
        type: "area",

        zoomType: "x",
      },
      title: {
        text: this.props.title,
      },

      xAxis: {
        type: "datetime",
      },
      yAxis: {
        title: {
          text: this.props.yvalue,
        },
      },
      legend: {
        enabled: true,
      },
      plotOptions: {
        series: {
          // fillOpacity: 0.7,
          turboThreshold: 50 
        },
        area: {
          fillColor: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1
          },
          stops: [
            [0, Highcharts.getOptions().colors[0]],
            [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
        ]
          },
        },
      },
      series: [
        {
          type: "area",
          color: "#0066cc",
          name: this.props.name,
          data: this.props.data,
        },
      ],
    },
  };

  render() {
    return (
      <div>
        {" "}
        <HighchartsReact
          highcharts={Highcharts}
          options={this.state.option}
          ref={this.props.graph}
        ></HighchartsReact>
      </div>
    );
  }
}
