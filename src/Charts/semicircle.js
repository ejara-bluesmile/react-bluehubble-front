import React, { Component } from "react";
import HighchartsReact from "highcharts-react-official";
import Highcharts from "highcharts";

export default class semicircle extends Component {
  state = {
    option: {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
      },
      title: {
        text: "",
      },
      legend: {
        enabled: true,
        // floating: true,
        // verticalAlign: "xbottom",
        // align: "bottom",
        // layout: "vertical",
        // y: 10,
        labelFormatter: function () {
          var total = 0,
            percentage;
          percentage = ((this.y / total) * 100).toFixed(2);
          return (
            this.name +
            this.y +
            '(<span style="color:' +
            this.color +
            '">' +
            percentage +
            "%)"
          );
        },
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
      },
      series: [
        {
          type: "pie",
          name: "Browser share",
          innerSize: "50%",
          data: this.props.data,
          // (["Firefox", 10.38],
          // ["IE", 56.33],
          // ["Chrome", 24.03],
          // ["Safari", 4.77],
          // ["Opera", 0.91]),
          // {
          //   name: "Proprietary or Undetectable",
          //   y: 0.2,
          //   dataLabels: {
          //     enabled: false,
          //   },
          // },
        },
      ],
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: "bold",
              color: "white",
              textShadow: "0px 1px 2px black",
            },
          },
          startAngle: -90,
          endAngle: 90,
          center: ["50%", "75%"],
          showInLegend: true,
        },
      },
    },
  };
  render() {
    return (
      <div>
        {" "}
        <HighchartsReact
          hihcharts={Highcharts}
          options={this.state.option}
        ></HighchartsReact>
      </div>
    );
  }
}
