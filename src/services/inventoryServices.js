import axios from "axios";


async function getInventory() {
    try {
        let devices
        const apiUrl = "http://13.58.191.209:40000/devices"
        const info = await axios.get(apiUrl);
        devices = info.data
        export default devices
        console.log("metodo get", devices)

    } catch (e) {
        console.log(e)
    }

};

export default {
    getInventory,

}