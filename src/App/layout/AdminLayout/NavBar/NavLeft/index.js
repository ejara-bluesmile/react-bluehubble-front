import React, { Component } from "react";
import { connect } from "react-redux";
import windowSize from "react-window-size";

import NavSearch from "./NavSearch";
import Aux from "../../../../../hoc/_Aux";
import DEMO from "../../../../../store/constant";
import * as actionTypes from "../../../../../store/actions";
import {
  Row,
  Col,
  Card,
  Form,
  Button,
  InputGroup,
  FormControl,
  FormGroup,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";

class NavLeft extends Component {
  render() {
    let iconFullScreen = ["feather"];
    iconFullScreen = this.props.isFullScreen
      ? [...iconFullScreen, "icon-minimize"]
      : [...iconFullScreen, "icon-maximize"];

    let navItemClass = ["nav-item"];
    if (this.props.windowWidth <= 575) {
      navItemClass = [...navItemClass, "d-none"];
    }
    let dropdownRightAlign = false;
    if (this.props.rtlLayout) {
      dropdownRightAlign = true;
    }
    let title = "";
    document.title = title + " Blue Hubble";
    return (
      <Aux>
        <ul className="navbar-nav mr-auto">
          <li>
            <a
              href={DEMO.BLANK_LINK}
              className="full-screen"
              onClick={this.props.onFullScreen}
            >
              <i className={iconFullScreen.join(" ")} />
            </a>
          </li>
          <li>
            <a href="" style={{ fontSize: "20px" }}></a>
          </li>
          {/* <li className={navItemClass.join(" ")}>
            <Dropdown alignRight={dropdownRightAlign}>
              <Dropdown.Toggle variant={"link"} id="dropdown-basic">
                Search
              </Dropdown.Toggle>
              <ul>
                <Dropdown.Menu>
                  <li>
                    <a className="dropdown-item" href={DEMO.BLANK_LINK}>
                      {"buscador 1"}
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href={DEMO.BLANK_LINK}>
                      {"buscador 2"}
                    </a>
                  </li>
                </Dropdown.Menu>
              </ul>
            </Dropdown>
          </li>
          <li className="nav-item">
            <NavSearch placeholder="Search per site" />
          </li> */}
          {/* <li className="nav-item">
            <NavSearch placeholder="Search per link" />
          </li>
          <li className={navItemClass.join(" ")}>
            <Dropdown alignRight={dropdownRightAlign}>
              <Dropdown.Toggle variant={"link"} id="dropdown-basic">
                Filter per Layer
              </Dropdown.Toggle>
              <ul>
                <Dropdown.Menu onLoad={this.cargarfiltro}>
                  <li>
                    <Form.Check
                      custom
                      type="checkbox"
                      id="OCH"
                      label="OCH"
                      style={{ fontSize: "12px" }}
                      className="m-1"
                    />
                  </li>
                  <li>
                    <Form.Check
                      custom
                      type="checkbox"
                      id="IP_INTERFACE"
                      label="IP_INTERFACE"
                      style={{ fontSize: "12px" }}
                      className="m-1"
                    />
                  </li>
                </Dropdown.Menu>
              </ul>
            </Dropdown>
          </li>
          <li className={navItemClass.join(" ")}>
            <Dropdown alignRight={dropdownRightAlign}>
              <Dropdown.Toggle variant={"link"} id="dropdown-basic">
                Physical Layer
              </Dropdown.Toggle>
              <ul>
                <Dropdown.Menu>
                  <li>
                    <Form.Check
                      custom
                      type="checkbox"
                      id="checkbox1"
                      label="IP_ELEMENT"
                      style={{ fontSize: "12px" }}
                      className="m-1"
                    />
                  </li>
                  <li>
                    <Form.Check
                      custom
                      type="checkbox"
                      id="checkbox2"
                      label="IP_INTERFACE"
                      style={{ fontSize: "12px" }}
                      className="m-1"
                    />
                  </li>
                  <li>
                    <Form.Check
                      custom
                      type="checkbox"
                      id="checkbox3"
                      label="OCH_ELEMENT"
                      style={{ fontSize: "12px" }}
                      className="m-1"
                    />
                  </li>
                  <li>
                    <Form.Check
                      custom
                      type="checkbox"
                      id="checkbox4"
                      label="OCH"
                      style={{ fontSize: "12px" }}
                      className="m-1"
                    />
                  </li>
                </Dropdown.Menu>
              </ul>
            </Dropdown>
          </li> */}
        </ul>
      </Aux>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isFullScreen: state.isFullScreen,
    rtlLayout: state.rtlLayout,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFullScreen: () => dispatch({ type: actionTypes.FULL_SCREEN }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(windowSize(NavLeft));
