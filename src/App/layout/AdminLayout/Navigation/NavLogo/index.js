import React from "react";
import DEMO from "./../../../../../store/constant";
import Aux from "../../../../../hoc/_Aux";
import logo from "../../../../../assets/images/LOGOS/LOGOS/logoHubbleGreyscaleSinBy.webp"
import  "../../../../../assets/scss/logo.scss"

const navLogo = (props) => {
  let toggleClass = ["mobile-menu"];
  if (props.collapseMenu) {
    toggleClass = [...toggleClass, "on"];
  }

  return (
    <Aux >

      <div className="navbar-brand header-logo appbar">
        <a href={DEMO.BLANK_LINK} className="b-brand">
          <img src={logo} className="logomenu"></img>
          <span className="b-title">Blue Hubble</span>
        </a>
        <a
          href={DEMO.BLANK_LINK}
          className={toggleClass.join(" ")}
          id="mobile-collapse"
          onClick={props.onToggleNavigation}
        >
          <span />
        </a>
     
      </div>
  

    </Aux>
  );
};

export default navLogo;
