import React, { Component } from "react";
import { Row, Col, Button, Card, Collapse } from "react-bootstrap";

import Aux from "../../../../../hoc/_Aux";
import DEMO from "../../../../../store/constant";

export default class filters extends Component {
  state = {
    isBasic: false,
    isMultiTarget: [],
    accordionKey: 1,
  };
  targetHandler = (target) => {
    if (this.state.isMultiTarget.some((item) => item === target)) {
      this.setState((prevState) => {
        return {
          isMultiTarget: prevState.isMultiTarget.filter(
            (item) => item !== target
          ),
        };
      });
    } else {
      this.setState((prevState) => {
        return {
          isMultiTarget: [...prevState.isMultiTarget, target],
        };
      });
    }
  };
  multiTargetHandler = () => {
    const allTarget = ["target1", "target2"];
    allTarget.map((target) => {
      this.targetHandler(target);
      return false;
    });
  };
  render() {
    const { isBasic, isMultiTarget, accordionKey } = this.state;
    return <div></div>;
  }
}
