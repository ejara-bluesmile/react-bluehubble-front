import React, { Component, Suspense } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Fullscreen from "react-full-screen";
import windowSize from "react-window-size";

import Navigation from "./Navigation";
import NavBar from "./NavBar";
import Breadcrumb from "./Breadcrumb";
import Loader from "../Loader";
import routes from "../../../routes";
import Aux from "../../../hoc/_Aux";
import * as actionTypes from "../../../store/actions";
//import Login from "../Demo/Authentication/SignIn/SignIn"
import Login from "../../../Views/SignIn"
import "./app.scss";
import PrivateRoot from './../../../components/privateRoot';

class AdminLayout extends Component {
  state = { auth: false }
  componentDidMount() {
    var auth = this.state.auth;

    auth = localStorage.getItem('login')

    this.setState({ auth })

  }

  fullScreenExitHandler = () => {
    if (
      !document.fullscreenElement &&
      !document.webkitIsFullScreen &&
      !document.mozFullScreen &&
      !document.msFullscreenElement
    ) {
      this.props.onFullScreenExit();
    }
  };

  componentWillMount() {
    if (
      this.props.windowWidth > 992 &&
      this.props.windowWidth <= 1024 &&
      this.props.layout !== "horizontal"
    ) {
      this.props.onComponentWillMount();
    }
  }

  mobileOutClickHandler() {
    if (this.props.windowWidth < 992 && this.props.collapseMenu) {
      this.props.onComponentWillMount();
    }
  }

  render() {
    /* full screen exit call */
    document.addEventListener("fullscreenchange", this.fullScreenExitHandler, { passive: true });
    document.addEventListener(
      "webkitfullscreenchange",
      this.fullScreenExitHandler,
      { passive: true }
    );
    document.addEventListener(
      "mozfullscreenchange",
      this.fullScreenExitHandler,
      { passive: true }
    );
    document.addEventListener("MSFullscreenChange", this.fullScreenExitHandler, { passive: true });



    const menu = routes.map((route, index) => {

      return route.component ? (

        <PrivateRoot
          tipo={route.tipo}
          key={index}
          path={route.path}
          exact={route.exact}
          name={route.name}
          auth={this.state.auth}
          ruete={route}
          render={(props) => <route.component {...props} />}
        />
      ) : null;
    });

    return (
      <Aux>

        <Fullscreen enabled={this.props.isFullScreen}>
          <Navigation />
          <NavBar />
          <div
            className="pcoded-main-container"
            onClick={() => this.mobileOutClickHandler}
          >
            <div className="pcoded-wrapper">
              <div className="pcoded-content p-3">
                <div className="pcoded-inner-content">
                  <Breadcrumb />
                  <div className="main-body">
                    <div className="page-wrapper">
                      <Suspense fallback={<Loader />}>
                        <Switch>
                          <Route
                            exact
                            path="/signin"
                            render={() => {
                              return <Login />;
                            }}
                          />
                          {menu}
                          <Redirect from="/" to={this.props.defaultPath} />
                        </Switch>
                      </Suspense>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Fullscreen>
      </Aux>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    defaultPath: state.defaultPath,
    isFullScreen: state.isFullScreen,
    collapseMenu: state.collapseMenu,
    configBlock: state.configBlock,
    layout: state.layout,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFullScreenExit: () => dispatch({ type: actionTypes.FULL_SCREEN_EXIT }),
    onComponentWillMount: () => dispatch({ type: actionTypes.COLLAPSE_MENU }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(windowSize(AdminLayout));
