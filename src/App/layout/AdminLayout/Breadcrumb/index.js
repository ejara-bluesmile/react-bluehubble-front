import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Row,
  Col,
  Card,
  Form,
  Button,
  InputGroup,
  FormControl,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import config from "../../../../config";
import navigation from "../../../../menu-items";
import DEMO from "../../../../store/constant";
import Aux from "../../../../hoc/_Aux";
import status from "../../../../assets/images/redbutton.png";
import NavSearch from "../NavBar/NavLeft/NavSearch";
import Control from "../../../../Views/MapMenu";
import NavLeft from "../NavBar/NavLeft";

var writing = "online";
class Breadcrumb extends Component {
  state = {
    main: [],
    item: [],
  };

  componentDidMount() {
    navigation.items.map((item, index) => {
      if (item.type && item.type === "group") {
        this.getCollapse(item, index);
      }
      return false;
    });
  }

  componentWillReceiveProps = () => {
    navigation.items.map((item, index) => {
      if (item.type && item.type === "group") {
        this.getCollapse(item);
      }
      return false;
    });
  };

  getCollapse = (item) => {
    if (item.children) {
      item.children.filter((collapse) => {
        if (collapse.type && collapse.type === "collapse") {
          this.getCollapse(collapse);
        } else if (collapse.type && collapse.type === "item") {
          if (document.location.pathname === config.basename + collapse.url) {
            this.setState({ item: collapse, main: item });
          }
        }
        return false;
      });
    }
  };

  render() {
    let main, item;
    let breadcrumb = "";
    let title = "";
    if (this.state.main && this.state.main.type === "collapse") {
      main = (
        <li className="breadcrumb-item">
          <a href={DEMO.BLANK_LINK}>{this.state.main.title}</a>
        </li>
      );
    }
    if (this.state.item && this.state.item.type === "check") {
      title = this.state.item.title;

      item = (
        <li className="breadcrumb-item">
          <a href={DEMO.BLANK_LINK}>{title}</a>
          <Form.Check custom type="checkbox" id="" label="" />
        </li>
      );
    }

    if (this.state.item && this.state.item.type === "item") {
      title = this.state.item.title;
      item = (
        <li className="breadcrumb-item">
          <a href={DEMO.BLANK_LINK}>{title}</a>
        </li>
      );

      if (this.state.item.breadcrumbs !== false) {
        breadcrumb = <div className="page-header"></div>;
      }
    }

    document.title = title + " Blue Hubble";

    return <Aux>{breadcrumb}</Aux>;
  }
}

export default Breadcrumb;
