import React, { Component, Suspense, useState } from 'react';
import { Switch, Route } from 'react-router-dom';
import Loadable from 'react-loadable';

import '../../node_modules/font-awesome/scss/font-awesome.scss';

import Loader from './layout/Loader'
import Aux from "../hoc/_Aux";
import ScrollToTop from './layout/ScrollToTop';
import routes from "../route";
import Login from "../Views/SignIn"

const AdminLayout = Loadable({
    loader: () => import('./layout/AdminLayout'),
    loading: Loader
});

class App extends Component {
    state = {
        auth: false
    }

    componentDidMount() {
        var auth = this.state.auth;
        auth = localStorage.getItem('login')
       
        this.setState({ auth })
    }
    render() {

    
        const menu = routes.map((route, index) => {
            return (route.component) ? (

                <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={props => (
                        <route.component {...props} />
                    )} />
            ) : (null);
        });

        return (
            <Aux>
                <ScrollToTop>
                    <Suspense fallback={<Loader />}>
                        <Switch>
                            <Route
                                exact
                                path="/signin"
                                render={() => {
                                    return <Login />;
                                }}
                            />
                            {menu}
                            <Route path="/" component={AdminLayout} />
                        </Switch>
                    </Suspense>
                </ScrollToTop>
            </Aux>
        );
    }
}

export default App;
