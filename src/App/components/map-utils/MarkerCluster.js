/* eslint-disable no-multi-str */
/* eslint-disable no-eval */
/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable no-undef */
import React, { useEffect } from "react";
import L from "leaflet";
import { useLeaflet } from "react-leaflet";
import Autolinker from "../../../lib/Autolinker.min";
import "leaflet.markercluster/dist/leaflet.markercluster";
import "leaflet.markercluster/dist/MarkerCluster.css";
import "leaflet.markercluster/dist/MarkerCluster.Default.css";
import "leaflet-search";
import "leaflet-ant-path";
import "leaflet.control.layers.tree";
import socketClusterClient from "socketcluster-client";
import * as icons from "./icons";
import GeoJSONFilterControl from "./GeoJSONFilterControl";
import { getBySource_Wavelenght, getByRate } from "./layerStyle";
import WarningTable from "../../../components/table/WarningTable";
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const MySwal = withReactContent(Swal);
let OCH;
let IP_INTERFACE;

var ochTree = {
  label: "MMOO",
  layer: OCH,
  // name: "OCH",
};
var ip_interfaceTree = {
  label: "IP_INTERFACE",
  layer: IP_INTERFACE,
};
var ctl2 = L.control.layers.tree(ochTree, null, {
  collapsed: false,
  namedToggle: false,
  name: "OCH",
  label: "MMOO",
  closedSymbol: "",
  openedSymbol: "",
});

var ctl7 = L.control.layers.tree(ip_interfaceTree, null, {
  collapsed: false,
  namedToggle: false,
  name: "IP_INTERFACE",
  closedSymbol: "",
  openedSymbol: "",
});

const socket = socketClusterClient.create({
  hostname: "3.129.97.184",
  secure: false,
  port: 3000,
});
const socket2 = socketClusterClient.create({
  hostname: "3.129.97.184",
  secure: false,
  port: 3005,
});
let markersArray = [];
var alertsArray = [];
let clusterMarkers = {};
let map;
let openStream = false;
let isBdDown = false;
let isBdDown2 = false;
let loginOnce = true;
let showLicense = true;
let modalIsOpen = true;
let mainModalIsOpen = true;
let layerCounter = 0;
let errorMessage = false;
let showWarningsFlag = true;
let warningsCounter = 0;
let warningsCounter2 = 0;
let packagesCounter = 0;
let isVpnDown = false;
let openedMarker;
var openedMainAlert = "";
var receiveAlerts = false;
let content;

const MarkerCluster = ({ layer, name, ...props }) => {
  map = useLeaflet().map;

  // useEffect(() => showWarningsFlag = props.showWarnings, [props.showWarnings]);
  const openAlertStream = (id, msg) => {
    var idAux;
    var idAux2;
    idAux = id.replace("out", "");
    idAux2 = idAux.replace("in", "");
    let markerFound = markersArray.find(
      (e) => e.feature.properties.ID === idAux2
    );
    if (!markerFound) return;
    var actualContent = markerFound.getPopup().getContent();
    content = actualContent;
    markerFound
      .getPopup()
      .setContent(
        `<center><h3 style="margin:0; text-align: center;
        color:red !important;  margin-left: -37px;
        ">` +
          msg +
          `</h3></center><br>` +
          markerFound.getPopup().getContent()
      );
    if (idAux2 === "3125" || idAux2 === "3124") {
      map.options.closePopupOnClick = false;
      markerFound
        .getPopup()
        .setContent(
          markerFound
            .getPopup()
            .getContent()
            .replace('" width="750"', '&traffic=true" width="750"')
        );
      console.log(markerFound.getPopup().getContent());
    }
    $("#loadSpinGray").css("display", "inline-block");
    // map.setZoom(10)
    openedMarker = markerFound;
    markerFound.openPopup();
    // map.setView([markerFound.getPopup()._latlng.lat + 0.06, markerFound.getPopup()._latlng.lng]);
    if (idAux2 === "3125" || idAux2 === "3124") {
      openedMainAlert = idAux2;
    } else {
      openedMainAlert = "";
    }
    markerFound.getPopup().on("remove", function () {
      markerFound.getPopup().setContent(actualContent);
      modalIsOpen = true;
      map.options.closePopupOnClick = true;
      markerFound.path.setStyle({
        fillColor: "gray",
        color: "gray",
      });
      if (idAux2 === "3125" || idAux2 === "3124") mainModalIsOpen = true;
    });
  };

  const getPopupContent = (feature) => {
    let popupContent = '<embed  class="embmap-not "src="./';
    switch (feature.properties.IDstreaming) {
      case "TX":
        popupContent += "stream";
        break;
      case "OCH":
        popupContent += "stream3";
        break;
      default:
        break;
    }
    popupContent +=
      "?idDevice=" +
      feature.properties.ID +
      "&name=" +
      feature.properties.Trail_name +
      "&ip1=" +
      feature.properties.NE_IP_Address_Source +
      "&ip2=" +
      feature.properties.NE_IP_Address_Sink +
      "&oid1=" +
      feature.properties.OIDIn.OID1 +
      "/" +
      feature.properties.OIDIn.OID2 +
      "/" +
      feature.properties.OIDIn.OID3 +
      "/" +
      feature.properties.OIDIn.OID4 +
      "/" +
      feature.properties.OIDIn.OID5 +
      "&oid2=" +
      feature.properties.OIDOut.OID1 +
      "/" +
      feature.properties.OIDOut.OID2 +
      "/" +
      feature.properties.OIDOut.OID3 +
      "/" +
      feature.properties.OIDOut.OID4 +
      "/" +
      feature.properties.OIDOut.OID5 +
      "/&name1=" +
      feature.properties.OC_Source + "-" + feature.properties.NE_ID_Source +
      "&name2=" +
      feature.properties.OC_Sink + "-" + feature.properties.NE_ID_Sink +
      "&sourceModel=" +
      feature.properties.Source_Model +
      "&sinkModel=" +
      feature.properties.Sink_Model +
      '" width="750" height="500"></embed>' 
      // "Service Name: " +
      // feature.properties.Trail_name; //(version notebook)
    //let popupContent = '<embed src="./stream" width="1000" height="1750"></embed>' + "Service Name: "+feature.properties.Trail_name //(version 4k)
    return popupContent;
  };

  useEffect(() => {
    // (async () => {
    //   let channel = socket2.subscribe("trafficReset");
    //   for await (let warning of channel) {
    //     if (layerCounter === 4) {
    //       isVpnDown = false
    //       let idAux = warning.idDevice.replace('out', '')
    //       let idAux2 = idAux.replace('in', '')
    //       let markerFound = markersArray.find(e => e.feature.properties.ID === idAux2);
    //       markerFound.path.setStyle({
    //         fillColor: '#e03',
    //         color: '#e03',
    //         "opacity": 0.6
    //       });
    //       markerFound.path.addTo(map)
    //       if (!modalIsOpen || !showWarningsFlag) {
    //         $('#notification-circle2').css("display", "flex")
    //         $('#notification-circle-label2').text(++warningsCounter + "")
    //       }
    //       if (layerCounter === LAYERS_AMOUNT && modalIsOpen && showWarningsFlag) {
    //         if (idAux2 === '3125' || idAux2 === '3124') {
    //           if (receiveAlerts) {
    //             errorMessage = false
    //             $('#isLiveButton2').css("background-image", "url('../images/greenbutton.png')")
    //             modalIsOpen = false;
    //             openAlertStream(warning.idDevice, warning.msg);
    //           } else {
    //             $('#notification-circle2').css("display", "flex")
    //             $('#notification-circle-label2').text(++warningsCounter + "")

    //           }
    //         } else {
    //           errorMessage = false
    //           $('#isLiveButton2').css("background-image", "url('../images/greenbutton.png')")
    //           modalIsOpen = false;
    //           openAlertStream(warning.idDevice, warning.msg);

    //         }
    //       }
    //     }
    //   }

    // })();

    (async () => {
      let channel = socket2.subscribe("clientModal");
      for await (let warning of channel) {
        // if (layerCounter == 4) {
        isVpnDown = false;
        let idAux = warning.idDevice.replace("out", "");
        let idAux2 = idAux.replace("in", "");
        let markerFound = markersArray.find(
          (e) => e.feature.properties.ID == idAux2
        );
        markerFound.path.setStyle({
          fillColor: "#e03",
          color: "#e03",
          opacity: 0.6,
        });
        markerFound.path.addTo(map);
        if (!modalIsOpen || !showWarningsFlag) {
          $("#notification-circle2").css("display", "flex");
          $("#notification-circle-label2").text(++warningsCounter + "");
        }
        if (showWarningsFlag) {
          if (idAux2 === "3125" || idAux2 === "3124") {
            if (receiveAlerts && mainModalIsOpen) {
              errorMessage = false;
              $("#isLiveButton2").css(
                "background-image",
                "url('../images/greenbutton.png')"
              );
              mainModalIsOpen = false;
              openAlertStream(warning.idDevice, warning.msg);
            } else {
              $("#notification-circle2").css("display", "flex");
              $("#notification-circle-label2").text(++warningsCounter + "");
            }
          } else if (modalIsOpen) {
            errorMessage = false;
            $("#isLiveButton2").css(
              "background-image",
              "url('../images/greenbutton.png')"
            );
            modalIsOpen = false;
            openAlertStream(warning.idDevice, warning.msg);
          }
        }
        // }
      }
    })();
    (async () => {
      let channel = socket2.subscribe("clientModalNew");
      for await (let warning of channel) {
        // console.log(warning)
        // if (layerCounter == 4) {
        isVpnDown = false;
        let idAux = warning.idDevice.replace("out", "");
        let idAux2 = idAux.replace("in", "");
        let markerFound = markersArray.find(
          (e) => e.feature.properties.ID == idAux2
        );
        // console.log(markerFound)
        markerFound.path.setStyle({
          fillColor: "#e03",
          color: "#e03",
          opacity: 0.6,
        });
        markerFound.path.addTo(map);
        if (
          (warning.idDevice === "3124in" ||
            warning.idDevice === "3124out" ||
            warning.idDevice === "3125in" ||
            warning.idDevice === "3125out") &&
          mainModalIsOpen
        ) {
          errorMessage = false;
          $("#isLiveButton2").css(
            "background-image",
            "url('../images/greenbutton.png')"
          );
          mainModalIsOpen = false;
          openAlertStream(warning.idDevice, warning.msg);
        }
        // }
      }
    })();
    (async () => {
      let channel = socket2.subscribe("injectTraffic");
      for await (let warning of channel) {
        console.log(warning);
        setTimeout(() => {
          receiveAlerts = true;
        }, 500);
      }
    })();
    (async () => {
      let channel = socket2.subscribe("solveTraffic");
      for await (let warning of channel) {
        markersArray.map((mark) => {
          mark.path.setStyle({
            fillColor: "gray",
            color: "gray",
          });
          map.removeLayer(mark.path);
        });
        openedMarker.getPopup().setContent(content);
        Swal.fire("Problem Solved", "Bandwidth capacity expanded", "success");
      }
    })();
    (async () => {
      let channel = socket2.subscribe("disInjectTraffic");
      for await (let warning of channel) {
        receiveAlerts = false;
        markersArray.map((mark) => {
          mark.path.setStyle({
            fillColor: "gray",
            color: "gray",
          });
          map.removeLayer(mark.path);
        });
      }
    })();
    (async () => {
      let channel = socket2.subscribe("resetTraffic");
      for await (let warning of channel) {
        receiveAlerts = false;
        markersArray.map((mark) => {
          mark.path.setStyle({
            fillColor: "gray",
            color: "gray",
          });
          map.removeLayer(mark.path);
        });
      }
    })();
  }, []);

  useEffect(() => {
    let layerCounter = 0;
    let leaf_layer;
    let markers = L.markerClusterGroup();
    markers.clearLayers();

    if (
      layer.type === "FeatureCollection" &&
      (layer.name === "OMS" ||
        layer.name === "OCH" ||
        layer.name === "OTU" ||
        layer.name === "CLIENT" ||
        layer.name === "IP_INTERFACE" ||
        layer.name === "ODU")
    ) {
      markers.on("click", function (event) {
        $("#loadSpinGray").css("display", "inline-block");
      });

      leaf_layer = L.geoJson(layer, {
        style: function (feature) {
          return (
            getBySource_Wavelenght(feature.properties.Source_Wavelenght) ||
            getByRate(feature.properties.Rate)
          );
        },
        onEachFeature: function (feature, layer) {
          layer.color = layer.options.color;
          const popupContent = getPopupContent(feature);
          let customOptions = {
            maxWidth: "4000",
            className: "custom",
          };

          layer.bindPopup(popupContent, customOptions);
          markersArray.push(layer);

          //layer.bindPopup(chart, {maxHeight: 2000}); para ser usado con chart d3.
          /**
           *Line Animation (ant-path plugin) implementation
           */
          let path = L.polyline.antPath(
            [
              [layer._latlngs[0].lat, layer._latlngs[0].lng],
              [layer._latlngs[1].lat, layer._latlngs[1].lng],
              [layer._latlngs[2].lat, layer._latlngs[2].lng],
              [layer._latlngs[3].lat, layer._latlngs[3].lng],
            ],
            {
              delay: 500,
              dashArray: [30, 40],
              // "opacity": 0,
              weight: 8,
              color: "gray",
              pulseColor: "#FFFFFF",
              paused: false,
              reverse: false,
              hardwareAccelerated: true,
              stroke: true,
            }
          );
          //TO SET PATH DELAY:
          //   path.setStyle({
          //     "delay": 0
          //   })
          //Adding the layer features to the path
          path.bindPopup(popupContent, customOptions);
          path.bindTooltip(
            "UID: " +
              feature.properties.ID +
              " Service Name: " +
              feature.properties.Trail_name +
              " from: " +
              feature.properties.Source_Ne_Name +
              "  To: " +
              feature.properties.Sink_Ne_Name,
            {
              sticky: true,
            }
          );
          path.on("click", function (event) {});
          path.getPopup().on("remove", function () {
            map.removeLayer(layer.path);
            modalIsOpen = true;
            path.setStyle({
              fillColor: "gray",
              color: "gray",
            });
          });
          path.getPopup().on("add", function () {
            showWarningsFlag = false;
          });

          layer.path = path;
          feature.path = path;
          layer.getPopup().on("remove", function () {
            $("#loadSpinGray").css("display", "none");
            modalIsOpen = true;
            let layer2 = markersArray.find(
              (e) =>
                e.feature.properties.IDanime === feature.properties.IDanime &&
                e.feature.properties.ID !== feature.properties.ID
            );
            map.removeLayer(layer.path);
            if (layer2.path.options.color !== "#e03")
              map.removeLayer(layer2.path);
          });
          layer.getPopup().on("add", function () {
            modalIsOpen = false;
            let layer2 = markersArray.find(
              (e) =>
                e.feature.properties.IDanime === feature.properties.IDanime &&
                e.feature.properties.ID !== feature.properties.ID
            );
            layer.path.addTo(map);
            layer2.path.addTo(map);
          });
          layer.bindTooltip(
            "UID: " +
              feature.properties.ID +
              "<br>Service Name: " +
              feature.properties.Trail_name +
              "<br>From: " +
              feature.properties.Source_Ne_Name +
              "  To: " +
              feature.properties.Sink_Ne_Name +
              "<br> Source IP: " +
              feature.properties.NE_IP_Address_Source,
            {
              sticky: true,
            }
          );
          // if (globalIdDevice === feature.properties.ID)
          //     openStream = true
        },
      });
      let txt;
      layer.name === "OCH" ? (txt = "MMOO") : (txt = layer.name);
      let find2 = new L.Control.Search({
        layer: markers,
        zoom: 13,
        textPlaceholder: txt,
        delayType: 100, //defecto 100
        initial: true, //defecto false
        autoCollapseTime: 4200, //defecto 4200
        hideMarkerOnCollapse: false, //defecto false
        marker: false,
        propertyName: "Trail_name",
        container: name + "search",
      });
      let color, found;
      find2
        .on("search:locationfound", function (e) {
          color = e.layer.color;
          found = e.layer;
          e.layer.path.addTo(map);
        })
        .on("search:collapsed", function (e) {
          if (found) {
            found.setStyle({
              color: color,
            });
            map.removeLayer(found.path);
            found = undefined;
          }
        })
        .on("search:cancel", function (e) {
          if (found) {
            found.setStyle({
              color: color,
            });
            map.removeLayer(found.path);
            found = undefined;
          }
        });
      find2.addTo(map);

      // document.getElementById("searchLayer").appendChild(find2.getContainer());
      layerCounter += 1;
    } else if (
      layer.type === "FeatureCollection" &&
      layer.name === "Metro_Phy_Fiber_Route"
    ) {
      markers.on("click", function (event) {
        $("#loadSpinGray").css("display", "inline-block");
      });
      leaf_layer = L.geoJson(layer, {
        style: function (feature) {
          switch (feature.geometry.type) {
            case "MultiLineString":
              return {
                color: "#66FF00",
                weight: 3,
                opacity: 1,
              };
            //case 'LineString': return {color: "#66FF00", weight: 6, opacity: 1};
            default:
              break;
          }
        },
        onEachFeature: function (feature, layer) {
          layer.bindTooltip("Ruta: " + feature.properties.Name, {
            sticky: true,
          });
        },
      });
    } else if (
      layer.type === "FeatureCollection" &&
      layer.name === "PHY_Fibers_LD"
    ) {
      markers.on("click", function (event) {
        $("#loadSpinGray").css("display", "inline-block");
      });
      leaf_layer = L.geoJson(layer, {
        style: function (feature) {
          switch (feature.geometry.type) {
            case "MultiLineString":
              return {
                color: "#66FF00",
                weight: 3,
                opacity: 1,
              };
            case "LineString":
              return {
                color: "#66FF00",
                weight: 3,
                opacity: 1,
              };
            default:
              break;
          }
        },
        onEachFeature: function (feature, layer) {
          layer.bindTooltip("Ruta: " + feature.properties.Name, {
            sticky: true,
          });
        },
      });
    } else {
      if (
        layer.type === "FeatureCollection" &&
        (layer.name === "IP_ELEMENT" || layer.name === "OCH_ELEMENT")
      ) {
        leaf_layer = new L.geoJson(layer, {
          pointToLayer: function (feature, latlng) {
            let icon;
            switch (feature.properties.NE_Type) {
              case "1830 PSS-16":
                icon = icons.microwave_blue;
                break;
              case "1830 PSS-32":
                icon = icons.microwave_blue;
                break;
              case "1830 PSS-4":
                icon = icons.microwave_blue;
                break;
              case "1830PSS":
                icon = icons.microwave_blue;
                break;
              case "ASR9K":
                icon = icons.asr9k;
                break;
              case "C76XX":
                icon = icons.netflow_router;
                break;
              case "MX2020":
                icon = icons.optical_router;
                break;
              case "MX960":
                icon = icons.optical_router;
                break;
              case "NE40E-X":
                icon = icons.router_2;
                break;
              case "NE40E-X8/X16A":
                icon = icons.router_1;
                break;
              case "OptiX BWS 1600G":
                icon = icons.ng_wdm;
                break;
              case "OptiX Metro 6100V1":
                icon = icons.ng_wdm;
                break;
              case "OptiX Metro 6100V1E":
                icon = icons.ng_wdm;
                break;
              case "OptiX OSN 1800":
                icon = icons.optical_transport;
                break;
              case "OptiX OSN 1800 V":
                icon = icons.optical_transport;
                break;
              case "OptiX OSN 6800":
                icon = icons.ng_wdm_blue;
                break;
              case "OptiX OSN 8800":
                icon = icons.microwave_blue;//microwave_blue_blue
                break;
              case "OptiX OSN 8800 T16":
                icon = icons.microwave_blue;
                break;
              case "OptiX OSN 1800 T32":
                icon = icons.microwave_blue;
                break;
              case "OptiX OSN 9800":
                icon = icons.microwave_blue;
                break;
              case "OptiX OSN 9800 U16":
                icon = icons.microwave_blue;
                break;
              case "OptiX OSN 9800 U32":
                icon = icons.microwave_blue;
                break;
              case "S93XX":
                icon = icons.wavelenght_router;
                break;
              case "WDM Virtual NE":
                icon = icons.microwave_blue;
                break;
              case "NE40E-X2-M8":
                icon = icons.asr9k;
                break;
              case "ATN950B ":
                icon = icons.optical_router;
                break;
              default:
                break;
            }
            let marker = new L.marker(latlng, {
              icon: icon,
            });
            marker.bindTooltip("IP: " + feature.properties.Ne_IP);
            marker.bindPopup(
              '<table>\
                            <tr>\
                                <th scope="row">NE_Name:</th>\
                                <td>' +
                (["NE_Name"] !== null
                  ? Autolinker.link(String([feature.properties.NE_Name]))
                  : "") +
                '</td>\
                            </tr>\
                            <tr>\
                                <th scope="row">Model:</th>\
                                <td>' +
                (["Model"] !== null || ["NE_Type"] !== null
                  ? (Autolinker.link(String([feature.properties.Model])) || Autolinker.link(String([feature.properties.NE_Type])))
                  : "") +
                '</td>\
                            </tr>\
                            <tr>\
                                <th scope="row">OC_Name:</th>\
                                <td>' +
                (["OC_Name"] !== null
                  ? Autolinker.link(String([feature.properties.OC]))
                  : "") +
                '</td>\
                            </tr>\
                            <tr>\
                                <th scope="row">NE_IP_Address:</th>\
                                <td>' +
                (["NE_IP_Address"] !== null
                  ? Autolinker.link(String([feature.properties.Ne_IP]))
                  : "") +
                "</td>\
                            </tr>\
                            </table>",
              {
                maxHeight: 800,
                'className' : 'devicePopup'
              }
            );
            marker.on("click", function (event) {
              $("#loadSpinGray").css("display", "none");
            });
            return marker;
          },
        });

        let find = new L.Control.Search({
          layer: markers,
          zoom: 15,
          textPlaceholder: "Search per Site",
          delayType: 100,
          initial: false,
          autoCollapseTime: 4200,
          hideMarkerOnCollapse: false,
          propertyName: "OC",
        }).addTo(map);
        document.getElementById("searchSite").appendChild(find.getContainer());
      }
    }
    /*Part 5 rendering to the map and filtering*/
    markers.addLayer(leaf_layer);

    map.addLayer(markers);

    map.fitBounds(leaf_layer.getBounds());
    if (layer.name === "OCH") {
      OCH = new GeoJSONFilterControl(leaf_layer, layer, {
        Network: "Network",
        Rate: "Rate",
        Source_Wavelenght: "Source_Wavelenght",
        Subnet_Source: "Subnet_Source",
        Level: "Level",
        OC_Source: "OC_Source",
        OC_Sink: "OC_Sink",
      }).addTo(map);
      layerCounter += 1;
      if (layerCounter === 6) $(loading).css("display", "none");
    }
    if (layer.name === "IP_INTERFACE") {
      IP_INTERFACE = new GeoJSONFilterControl(leaf_layer, layer, {
        Network: "Network",
        Rate: "Rate",
        Source_Wavelenght: "Source_Wavelenght",
        Subnet_Source: "Subnet_Source",
        Level: "Level",
        "Transmission(TX)": "OC_Source",
        "Reception(RX)": "OC_Sink",
      }).addTo(map);
      layerCounter += 1;
    }

    ctl2.setOverlayTree(OCH).collapseTree(true).expandSelected(true).addTo(map);
    ctl7.setOverlayTree(IP_INTERFACE).collapseTree(true).addTo(map);
    document.getElementById("perLayerOCH").appendChild(ctl2.getContainer());
    document.getElementById("perLayerIP").appendChild(ctl7.getContainer());

    let ctl7isActive = true;
    let ctl2isActive = true;
    clusterMarkers[markers._leaflet_id] = true;

    $(ctl7.getContainer())
      .children()
      .eq(1)
      .children()
      .eq(2)
      .click(function (e) {
        if (!ctl7isActive) {
          ctl7._overlaysTree.layer.eachLayer(function (layer) {
            if (clusterMarkers[markers._leaflet_id]) map.addLayer(layer);
          });
          ctl7isActive = true;
        } else {
          ctl7isActive = false;
        }
        if (!clusterMarkers[markers._leaflet_id]) map.removeLayer(markers);
      });

    $(ctl2.getContainer())
      .children()
      .eq(1)
      .children()
      .eq(2)
      .click(function (e) {
        if (!ctl2isActive) {
          ctl2._overlaysTree.layer.eachLayer(function (layer) {
            if (clusterMarkers[markers._leaflet_id]) map.addLayer(layer);
          });
          ctl2isActive = true;
        } else {
          ctl2isActive = false;
        }
        if (!clusterMarkers[markers._leaflet_id]) map.removeLayer(markers);
      });

    map.scrollWheelZoom.disable();
    $("#" + name).change(function (e) {
      if (map.hasLayer(markers)) {
        clusterMarkers[markers._leaflet_id] = false;
        $.each(markers._nonPointGroup._layers, function (i, layer) {
          map.removeLayer(layer.path);
        });
        map.removeLayer(markers);
      } else {
        clusterMarkers[markers._leaflet_id] = true;
        $.each(markers._nonPointGroup._layers, function (i, layer) {
          if (layer.path.options.color === "#e03") layer.path.addTo(map);
        });
        map.addLayer(markers);
      }
    });

    // if (openStream) {
    //     openMarkerStream(globalIdDevice)
    //     window.history.pushState("", "", "map");
    // }
  }, []);

  return null;
};

export { MarkerCluster, socket, socket2 };
