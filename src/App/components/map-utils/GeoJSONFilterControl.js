import L from "leaflet";

export default  L.Control.extend({
    initialize: function (geoJsonLayer, data, fields, options = {}) {
      this.layer = geoJsonLayer;
      this.data = data;
      this.fields = fields;
      this.filteredOut = {};
      L.Util.setOptions(this, options);
    },
    options: {
      position: "topright",
      cursor: "pointer",
    },
    render: function () {
      this.layer.getLayers().forEach((marker) => {
        const mustFilter = Object.entries(this.filteredOut)
          .map(([group, val]) => val.has(marker.feature.properties[group]))
          .some((x) => x);
  
        marker.setStyle({
          opacity: mustFilter ? 0 : 1,
          //fill: !mustFilter
        });
      });
    },
    onAdd: function (map) {
      this.holder = L.DomUtil.create(
        "div",
        "button clear all select all geojson-filter leaflet-bar leaflet-control leaflet-control-custom"
      );
      if (!map.hasLayer(this.layer)) {
        // Hide the filter box if the layer is not yet visible
        this.holder.style.display = "none";
      }
      /* We don't want click events to interact with the map */
      L.DomEvent.disableClickPropagation(this.holder);
  
      Object.entries(this.fields).forEach(([title, fieldId]) => {
        this.filteredOut[fieldId] = new Set();
        /* Define the callback to filter all data for the given field */
        let callback = (e) => {
          let changedField = e.target.value;
          let changedTo = e.target.checked;
          if (!changedTo) {
            this.filteredOut[fieldId].add(changedField);
          } else {
            this.filteredOut[fieldId].delete(changedField);
          }
          this.render();
        };
        /* Create a fieldset with a legend for this filter group */
        let fieldGroup = L.DomUtil.create(
          "fieldset",
          "checkboxgroup",
          this.holder
        );
        let legend = L.DomUtil.create("legend", "", fieldGroup);
        legend.innerHTML = title;
  
        // setup clear/all buttons
        let buttonHolder = L.DomUtil.create("div", "", fieldGroup);
        let clearAll = L.DomUtil.create("button", "", buttonHolder);
        clearAll.textContent = "clear all";
        let selectAll = L.DomUtil.create("button", "", buttonHolder);
        selectAll.textContent = "select all";
  
        let valuesSet = this.data.features.reduce(
          (set, f) => set.add(f.properties[fieldId]),
          new Set()
        );
  
        clearAll.onclick = () => {
          fieldGroup
            .querySelectorAll('input[type="checkbox"]')
            .forEach((el) => (el.checked = false));
          this.filteredOut[fieldId] = valuesSet;
          valuesSet = this.data.features.reduce(
            (set, f) => set.add(f.properties[fieldId]),
            new Set()
          );
          for (let feature of this.data.features) {
            // feature.path.setStyle({
            //     "opacity": 0
            // });
            if (feature.path.options.color != "#e03")
              map.removeLayer(feature.path);
          }
          this.render();
        };
        selectAll.onclick = () => {
          fieldGroup
            .querySelectorAll('input[type="checkbox"]')
            .forEach((el) => (el.checked = true));
          this.filteredOut[fieldId] = new Set();
          for (let feature of this.data.features) {
            if (feature.path.options.color == "#e03") {
              feature.path.setStyle({
                // "stroke": true,
                opacity: 0.6,
              });
              feature.path.addTo(map);
            }
          }
          this.render();
        };
        valuesSet.forEach((val) => {
          let cb = L.DomUtil.create("input", "", fieldGroup);
          cb.type = "checkbox";
          cb.id = fieldId + "_" + val;
          cb.checked = "checked";
          cb.value = val;
          cb.onchange = callback;
  
          let cbLabel = L.DomUtil.create("label", "", fieldGroup);
          cbLabel.setAttribute("for", cb.id);
          cbLabel.textContent = val;
          L.DomUtil.create("br", "", fieldGroup);
        });
      });
      map.on({
        layeradd: (e) => {
          if (e.layer === this.layer) {
            this.holder.style.display = "";
          }
        },
        layerremove: (e) => {
          if (e.layer === this.layer) {
            this.holder.style.display = "none";
          }
        },
      });
      return this.holder;
    },
    show: function () {
      this.holder.style.display = "";
    },
    hide: function () {
      this.holder.style.display = "none";
    },
  });