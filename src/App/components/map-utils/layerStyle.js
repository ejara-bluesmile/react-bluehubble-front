export const getBySource_Wavelenght = (source_Wavelenght) => {
    switch (source_Wavelenght) {
        case "192.1":
            return {
                color: "#66FF00",
                weight: 6,
                opacity: 1,
            };
        case "192.15":
            return {
                color: "#af00000",
                weight: 6,
                opacity: 1,
            };
        case "192.2":
            return {
                color: "#CCCCFF",
                weight: 6,
                opacity: 1,
            };
        case "192.3":
            return {
                color: "#FFCCCC",
                weight: 6,
                opacity: 1,
            };
        case "192.35":
            return {
                color: "#af0000",
                weight: 6,
                opacity: 1,
            };
        case "192.4":
            return {
                color: "#66FFCC",
                weight: 6,
                opacity: 1,
            };
        case "192.45":
            return {
                color: "#66FFCC",
                weight: 6,
                opacity: 1,
            };
        case "192.5":
            return {
                color: "#99CCFF",
                weight: 6,
                opacity: 1,
            };
        case "192.55":
            return {
                color: "#99CCFF",
                weight: 6,
                opacity: 1,
            };
        case "192.6":
            return {
                color: "#99FF99",
                weight: 6,
                opacity: 1,
            };
        case "192.65":
            return {
                color: "#99FF99",
                weight: 6,
                opacity: 1,
            };
        case "192.7":
            return {
                color: "#33CCCC",
                weight: 6,
                opacity: 1,
            };
        case "192.75":
            return {
                color: "#33CCCC",
                weight: 6,
                opacity: 1,
            };
        case "192.8":
            return {
                color: "#99FF33",
                weight: 6,
                opacity: 1,
            };
        case "192.85":
            return {
                color: "#99FF33",
                weight: 6,
                opacity: 1,
            };
        case "192.9":
            return {
                color: "#CCCC99",
                weight: 6,
                opacity: 1,
            };
        case "192.95":
            return {
                color: "#CCCC99",
                weight: 6,
                opacity: 1,
            };
        case "193.00":
            return {
                color: "#CC66CC",
                weight: 6,
                opacity: 1,
            };
        case "193.05":
            return {
                color: "#CC66CC",
                weight: 6,
                opacity: 1,
            };
        case "193.1":
            return {
                color: "#00CCFF",
                weight: 6,
                opacity: 1,
            };
        case "193.15":
            return {
                color: "#00CCFF",
                weight: 6,
                opacity: 1,
            };
        case "193.2":
            return {
                color: "#FF9966",
                weight: 6,
                opacity: 1,
            };
        case "193.25":
            return {
                color: "#FF9966",
                weight: 6,
                opacity: 1,
            };
        case "193.3":
            return {
                color: "#FFCC00",
                weight: 6,
                opacity: 1,
            };
        case "193.35":
            return {
                color: "#FFCC00",
                weight: 6,
                opacity: 1,
            };
        case "193.4":
            return {
                color: "#00FF99",
                weight: 6,
                opacity: 1,
            };
        case "193.45":
            return {
                color: "#00FF99",
                weight: 6,
                opacity: 1,
            };
        case "193.5":
            return {
                color: "#6699FF",
                weight: 6,
                opacity: 1,
            };
        case "193.55":
            return {
                color: "#6699FF",
                weight: 6,
                opacity: 1,
            };
        case "193.6":
            return {
                color: "#FF66CC",
                weight: 6,
                opacity: 1,
            };
        case "193.65":
            return {
                color: "#FF66CC",
                weight: 6,
                opacity: 1,
            };
        case "193.7":
            return {
                color: "#FF9966",
                weight: 6,
                opacity: 1,
            };
        case "193.75":
            return {
                color: "#FF9966",
                weight: 6,
                opacity: 1,
            };
        case "193.8":
            return {
                color: "#9966CC",
                weight: 6,
                opacity: 1,
            };
        case "193.85":
            return {
                color: "#9966CC",
                weight: 6,
                opacity: 1,
            };
        case "193.9":
            return {
                color: "#CCFFFF",
                weight: 6,
                opacity: 1,
            };
        case "193.95":
            return {
                color: "#CCFFFF",
                weight: 6,
                opacity: 1,
            };
        case "194.00":
            return {
                color: "#3399FF",
                weight: 6,
                opacity: 1,
            };
        case "194.05":
            return {
                color: "#3399FF",
                weight: 6,
                opacity: 1,
            };
        case "194.1":
            return {
                color: "#FF6666",
                weight: 6,
                opacity: 1,
            };
        case "194.15":
            return {
                color: "#FF6666",
                weight: 6,
                opacity: 1,
            };
        case "194.2":
            return {
                color: "#669999",
                weight: 6,
                opacity: 1,
            };
        case "194.25":
            return {
                color: "#669999",
                weight: 6,
                opacity: 1,
            };
        case "194.3":
            return {
                color: "#CC9933",
                weight: 6,
                opacity: 1,
            };
        case "194.35":
            return {
                color: "#CC9933",
                weight: 6,
                opacity: 1,
            };
        case "194.4":
            return {
                color: "#FF9900",
                weight: 6,
                opacity: 1,
            };
        case "194.45":
            return {
                color: "#FF9900",
                weight: 6,
                opacity: 1,
            };
        case "194.5":
            return {
                color: "#669999",
                weight: 6,
                opacity: 1,
            };
        case "194.55":
            return {
                color: "#669999",
                weight: 6,
                opacity: 1,
            };
        case "194.6":
            return {
                color: "#FF6633",
                weight: 6,
                opacity: 1,
            };
        case "194.65":
            return {
                color: "#FF6633",
                weight: 6,
                opacity: 1,
            };
        case "194.7":
            return {
                color: "#FF9900",
                weight: 6,
                opacity: 1,
            };
        case "194.75":
            return {
                color: "#FF9900",
                weight: 6,
                opacity: 1,
            };
        case "194.8":
            return {
                color: "#33CC33",
                weight: 6,
                opacity: 1,
            };
        case "194.85":
            return {
                color: "#33CC33",
                weight: 6,
                opacity: 1,
            };
        case "194.9":
            return {
                color: "#00FF00",
                weight: 6,
                opacity: 1,
            };
        case "194.95":
            return {
                color: "#00FF00",
                weight: 6,
                opacity: 1,
            };
        case "195.00":
            return {
                color: "#CC6666",
                weight: 6,
                opacity: 1,
            };
        case "195.05":
            return {
                color: "#CC6666",
                weight: 6,
                opacity: 1,
            };
        case "195.1":
            return {
                color: "#E67E22",
                weight: 6,
                opacity: 1,
            };
        case "195.15":
            return {
                color: "#E67E22",
                weight: 6,
                opacity: 1,
            };
        case "195.2":
            return {
                color: "#FF9900",
                weight: 6,
                opacity: 1,
            };
        case "195.25":
            return {
                color: "#FF9900",
                weight: 6,
                opacity: 1,
            };
        case "195.3":
            return {
                color: "#CC9900",
                weight: 6,
                opacity: 1,
            };
        case "195.35":
            return {
                color: "#CC9900",
                weight: 6,
                opacity: 1,
            };
        case "195.4":
            return {
                color: "#CC6633",
                weight: 6,
                opacity: 1,
            };
        case "195.45":
            return {
                color: "#CC6633",
                weight: 6,
                opacity: 1,
            };
        case "195.5":
            return {
                color: "#009999",
                weight: 6,
                opacity: 1,
            };
        case "195.55":
            return {
                color: "#009999",
                weight: 6,
                opacity: 1,
            };
        case "195.6":
            return {
                color: "#669933",
                weight: 6,
                opacity: 1,
            };
        case "195.65":
            return {
                color: "#669933",
                weight: 6,
                opacity: 1,
            };
        case "195.7":
            return {
                color: "#009999",
                weight: 6,
                opacity: 1,
            };
        case "195.75":
            return {
                color: "#009999",
                weight: 6,
                opacity: 1,
            };
        case "195.8":
            return {
                color: "#CC0066",
                weight: 6,
                opacity: 1,
            };
        case "195.85":
            return {
                color: "#CC0066",
                weight: 6,
                opacity: 1,
            };
        case "195.9":
            return {
                color: "#E67E22",
                weight: 6,
                opacity: 1,
            };
        case "195.95":
            return {
                color: "#E67E22",
                weight: 6,
                opacity: 1,
            };
        case "196.00":
            return {
                color: "#333399",
                weight: 6,
                opacity: 1,
            };
        case "-":
            return {
                color: "#BDBDBD",
                weight: 6,
                opacity: 1,
            };
        case "1310":
            return {
                color: "#CC0066",
                weight: 6,
                opacity: 1,
            };
        //case '': return {color: "#333399", weight: 6, opacity: 1};
        //case '10G': return {color: "#A9F5F2", weight: 6, opacity: 1};
        //case '100G': return {color: "#ff0000", weight: 6, opacity: 1};
        default:
            return null;
    }
}

export const getByRate = (rate) => {
    switch (rate) {
        case "STM-16":
            return {
                color: "#2ECCFA",
                weight: 6,
                opacity: 1,
            };
        case "STM-64":
            return {
                color: "#BDBDBD",
                weight: 6,
                opacity: 1,
            };
        case "10GE":
            return {
                color: "#af00000",
                weight: 6,
                opacity: 1,
            };
        case "10GE LAN":
            return {
                color: "#CCCCFF",
                weight: 6,
                opacity: 1,
            };
        case "GE(GFP-T)":
            return {
                color: "#FFCCCC",
                weight: 6,
                opacity: 1,
            };
        case "GE":
            return {
                color: "#af0000",
                weight: 6,
                opacity: 1,
            };
        case "ANY":
            return {
                color: "#66FFCC",
                weight: 6,
                opacity: 1,
            };
        case "100GE":
            return {
                color: "#66FFCC",
                weight: 6,
                opacity: 1,
            };
        case "STM-4":
            return {
                color: "#99CCFF",
                weight: 6,
                opacity: 1,
            };
        case "HDSDIRBR":
            return {
                color: "#99CCFF",
                weight: 6,
                opacity: 1,
            };
        case "FC400":
            return {
                color: "#99FF99",
                weight: 6,
                opacity: 1,
            };
        case "SDI":
            return {
                color: "#99FF99",
                weight: 6,
                opacity: 1,
            };
        case "FICON4G":
            return {
                color: "#33CCCC",
                weight: 6,
                opacity: 1,
            };
        case "HDSDI":
            return {
                color: "#33CCCC",
                weight: 6,
                opacity: 1,
            };
        case "GE(TTT-GMP)":
            return {
                color: "#99FF33",
                weight: 6,
                opacity: 1,
            };
        case "FC800":
            return {
                color: "#99FF33",
                weight: 6,
                opacity: 1,
            };
        case "InfiniBand 5G":
            return {
                color: "#CCCC99",
                weight: 6,
                opacity: 1,
            };
        case "10GBE":
            return {
                color: "#CCCC99",
                weight: 6,
                opacity: 1,
            };
        case "10GbE":
            return {
                color: "#CCCC99",
                weight: 6,
                opacity: 1,
            };
        case "1GBE":
            return {
                color: "#FA58D0",
                weight: 6,
                opacity: 1,
            };
        case "1GbE":
            return {
                color: "#FA58D0",
                weight: 6,
                opacity: 1,
            };
        case "DSR":
            return {
                color: "#CC66CC",
                weight: 6,
                opacity: 1,
            };
        //case 'ODU2': return {color: "#00CCFF", weight: 6, opacity: 1};
        case "100GBE":
            return {
                color: "#00CCFF",
                weight: 6,
                opacity: 1,
            };
        case "100GbE":
            return {
                color: "#00CCFF",
                weight: 6,
                opacity: 1,
            };
        case "OTU2":
            return {
                color: "#FF9966",
                weight: 6,
                opacity: 1,
            };
        case "":
            return {
                color: "#A9F5F2",
                weight: 6,
                opacity: 1,
            };
        case "-":
            return {
                color: "#A9F5F2",
                weight: 6,
                opacity: 1,
            };
        //IP
        case "10G":
            return {
                color: "#FF33E9",
                weight: 6,
                opacity: 1,
            };
        case "100G":
            return {
                color: "#FE0000",
                weight: 6,
                opacity: 1,
            };
        //ODU
        case "ODU0":
            return {
                color: "#24B7CB",
                weight: 6,
                opacity: 1,
            };
        case "ODU1":
            return {
                color: "#2ECCFA",
                weight: 6,
                opacity: 1,
            };
        case "ODU2":
            return {
                color: "#FF33E9",
                weight: 6,
                opacity: 1,
            };
        case "ODU3":
            return {
                color: "#af00000",
                weight: 6,
                opacity: 1,
            };
        case "ODU4":
            return {
                color: "#FE0000",
                weight: 6,
                opacity: 1,
            };
        case "ODU5G":
            return {
                color: "#FFCCCC",
                weight: 6,
                opacity: 1,
            };
        case "INFINIBAND 5G":
            return {
                color: "#CCCC99",
                weight: 6,
                opacity: 1,
            };
        case "ODUCN":
            return {
                color: "#af0000",
                weight: 6,
                opacity: 1,
            };
        default:
            return null;
    }
}
