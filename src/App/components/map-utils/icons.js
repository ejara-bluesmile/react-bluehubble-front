import L from "leaflet";
import asr9kImg from "../../../assets/images/icons/asr9k.png";
import optical_routerImg from "../../../assets/images/icons/optical_router.png";
import virtual_wdmImg from "../../../assets/images/icons/virtual_wdm.png";
import wavelenght_routerImg from "../../../assets/images/icons/wavelenght_router.png";
import roadmImg from "../../../assets/images/icons/roadm.png";
import roadm_blueImg from "../../../assets/images/icons/roadm_blue.png";
import optical_transportImg from "../../../assets/images/icons/optical_transport.png";
import router_1Img from "../../../assets/images/icons/router_1.png";
import router_2Img from "../../../assets/images/icons/router_2.png";
import ng_wdm_blueImg from "../../../assets/images/icons/ng_wdm_blue.png";
import ng_wdmImg from "../../../assets/images/icons/ng_wdm.png";
import microwave_blueImg from "../../../assets/images/icons/microwave-blue.png";


export const virtual_wdm = new L.icon({
    iconUrl: virtual_wdmImg,
    iconSize: [60, 50],
});
export const wavelenght_router = new L.icon({
    iconUrl: wavelenght_routerImg,
    iconSize: [60, 50],
});
export const roadm = new L.icon({
    iconUrl: roadmImg,
    iconSize: [60, 50],
});
export const roadm_blue = new L.icon({
    iconUrl: roadm_blueImg,
    iconSize: [60, 50],
});
export const optical_transport = new L.icon({
    iconUrl: optical_transportImg,
    iconSize: [60, 50],
});
export const optical_router = new L.icon({
    iconUrl: optical_routerImg,
    iconSize: [60, 50],
});
export const netflow_router = new L.icon({
    iconUrl: roadmImg,
    iconSize: [60, 50],
});
export const asr9k = new L.icon({
    iconUrl: asr9kImg,
    iconSize: [60, 50],
});
export const router_1 = new L.icon({
    iconUrl: router_1Img,
    iconSize: [60, 50],
});
export const router_2 = new L.icon({
    iconUrl: router_2Img,
    iconSize: [60, 50],
});
export const ng_wdm_blue = new L.icon({
    iconUrl: ng_wdm_blueImg,
    iconSize: [60, 50],
});
export const ng_wdm = new L.icon({
    iconUrl: ng_wdmImg,
    iconSize: [60, 50],
});
export const microwave_blue = new L.icon({
    iconUrl: microwave_blueImg,
    iconSize: [60, 50],
});