import React from "react";
import $ from "jquery";


window.jQuery = $;
window.$ = $;
global.jQuery = $;


const Map = React.lazy(() => import("./Views/Map"));
const Records = React.lazy(() => import("./Views/Records"));
const Inventory = React.lazy(() => import("./Views/inventory.jsx"))
const CharData = React.lazy(() => import("./Views/chartData"))
const chartWarnings = React.lazy(() => import("./Views/ChartWarnings"))
const Connection = React.lazy(() => import("./Views/Connection"));
const login = React.lazy(() => import("./Views/SignIn"));
const UserProfileLite = React.lazy(() => import("./Views/UserProfileLite"));
const routes = [
 
  {
    path: "/profile",
    exact: true,
    name: "profile",
    component: UserProfileLite,
  },
  {
    path: "/",
    exact: true,
    name: "map",
    component: Map,
  },
  {
    path: "/chartWarnings",
    exact: true,
    name: "chartWarnings",
    component: chartWarnings,
  },
  {
    path: "/chartData",
    exact: true,
    name: "chartData",
    component: CharData,
  },
  {
    path: "/inventory",
    exact: true,
    name: "inventory",
    component: Inventory,
  },
  {
    path: "/records",
    exact: true,
    name: "Records",
    component: Records,
  },
  {
    path: "/connection",
    exact: true,
    name: "Connection",
    component: Connection,
  },
];

export default routes;
