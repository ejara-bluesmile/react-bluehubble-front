/* config-overrides.js */
const path= require('path');
const { whenProd } = require('@craco/craco');
const HtmlCriticalWebpackPlugin = require('html-critical-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const Critters = require('critters-webpack-plugin');
// const HtmlWebpackPlugin = require('html-webpack-plugin')
// const WebpackCritical = require('webpack-critical');



module.exports = function override(webpackConfig, env) {
  //do stuff with the webpack config...

  return {
    ...webpackConfig,
    plugins: [
      ...webpackConfig.plugins,
      // new Critters({
      //     preload: 'swap',
      //   }),
      ...whenProd(
          () => [
      // new HtmlWebpackPlugin(),
      // new WebpackCritical({
      //   context:  path.resolve(__dirname, 'build'),
      //   ignore: ['@font-face']
      // }),
      new HtmlCriticalWebpackPlugin({
        base: path.resolve(__dirname, 'build'),
        src: 'index.html',
        dest: 'index.html',
        inline: true,
        minify: true,
        extract: true,
        width: 320,
        height: 565,
        penthouse: {
          blockJSRequests: false,
        },
      }),
      ],
      []
      ),
    ],
    // module: {
    //     rules: [
    //       {
    //         test: /.s?css$/,
    //         use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
    //       },
    //     ],
    //   },>
    optimization: {
      minimize: true,
      minimizer: [
        // For webpack@5 you can use the `...` syntax to extend existing minimizers (i.e. `terser-webpack-plugin`), uncomment the next line
        //   `...`,
        new CssMinimizerPlugin(),
      ],
    },
  };
}