 FROM node:12.18.1
 WORKDIR /front
 ENV PORT 5000
 COPY package.json /front/
 RUN npm install
 COPY . ./
 EXPOSE 5000