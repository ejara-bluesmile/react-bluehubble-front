var express= require('express');
var app =express();
var path=require("path");
var compression=require("compression")


app.use(compression());

//static path
app.use(express.static(path.join(__dirname,'build')))
app.get('ping',(req,res)=>{
    res.send('pong');

});

//server the index page 
app.get('/*',(req,res)=>{
res.sendFile(path.join(__dirname,"build","index.html"));
});

app.listen(5000,()=>{
    console.log("server started at 5000")
})